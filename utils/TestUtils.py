import pytest
from functools import wraps


def Expects(items):
    def actual_decorator(function):
        @wraps(function)
        def returned_func(self, *args, **kwargs):
            # print self.data
            for str in items:
                if str not in self.data:
                    pytest.skip('%s not in %s' % (str, self.data))

            return function(self, *args, **kwargs)
        return returned_func
    return actual_decorator

from BasePage import BasePage

loc = {
    "buy_pass": "xpath=//a[text()='Buy a Pass']",
    "pass_heading": "xpath=//span[text()='Choose pass']",
    "pass_name": "xpath=//h3[text()='{}']",
    "add_to_basket":
        "xpath=//h3[text()='{}']/../../../..//span[text()='Add to Basket']",
    "quantity_input": "xpath=//*[text()='{}']/..//input["
                      "contains(@class, 'sidebar__quantity-input')]",
    "price_in_pass_manager":
        "xpath=//div[contains(@class, 'sidebar__total')]",
    "proceed_button": "xpath=//button[text()='Next']",
    "pending_order_heading": "xpath=//h2[contains(text(),'Pending Order')]",
    "add_to_basket3": "xpath=//button[contains(text(),'Add To Basket')]",
    "list_view_generic_pass": "xpath=//span[text()='{}']",
}

pass_prices = {
    'The Standard Pass': 1495
}


class M2020PassPage(BasePage):

    def __init__(self, driver):
        super(M2020PassPage, self).__init__(driver=driver)

    def add_single_pass(self, pass_type, quantity=1):

        self.press(loc["buy_pass"])
        self.wait_until_appear(loc["pass_heading"])
        assert self.is_element_visible(loc["pass_heading"])
        self.press(loc["pass_name"].format(pass_type))
        # self.wait_until_appear(loc["add_to_basket"])
        self.wait_in_seconds(2)
        self.press(loc["add_to_basket"].format(pass_type))
        self.wait_in_seconds(2)
        current_quantity = (self.find_by_locator(
            loc["quantity_input"].format(pass_type)).get_attribute('value'))
        if quantity > 1:
            self.clear_and_type(
                loc["quantity_input"].format(pass_type), quantity)
            updated_quantity = (self.find_by_locator(
                loc["quantity_input"].format(pass_type)).get_attribute('value'))
            assert int(current_quantity) != int(updated_quantity)
            assert quantity == int(updated_quantity)
        total_price = pass_prices[pass_type] * quantity
        price_on_page = int(((((self.find_by_locator(
            loc["price_in_pass_manager"])).text).split(
            '$'))[1]).replace(',', ''))
        assert total_price == price_on_page
        self.press(loc["proceed_button"])
        self.wait_until_appear(loc["pending_order_heading"])
        assert self.is_element_visible(loc["pending_order_heading"])
        return True, price_on_page

    # def add_pass_from_list_view(self, data, pass_type=None):
    #     if pass_type == 'press':
    #         self.get_web_page(data['url'] + special_passes_url['press'])
    #     elif pass_type == 'competitor':
    #         self.get_web_page(data['url'] + special_passes_url['competitors'])
    #         self.wait_in_seconds(3)
    #     elif pass_type == 'representative':
    #         self.get_web_page(
    #             data['url'] + special_passes_url['representative'])
    #     self.wait_until_appear(loc["add_to_basket3"])
    #     if pass_type == 'representative':
    #         self.wait_in_seconds(2)
    #         self.press(loc["list_view_generic_pass"].format(
    #             'Festival Representative'))
    #     self.press(loc["add_to_basket3"])
    #     self.wait_until_appear(loc["pending_order_heading"])
    #     assert "manager" in self.driver.current_url
    #     return True
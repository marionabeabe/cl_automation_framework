cred_card_visa = {
    "name_on_card": "Luke Skywalker",
    "credit_card_number": "4444333322221111",
    "expiry_month": "10",
    "expiry_year": "2020",
    "cvv": "737",
    "pin": "sty"
}

cred_card_mastercard = {
    "name_on_card": "Han Solo",
    "credit_card_number": "5555444433331111",
    "expiry_month": "10",
    "expiry_year": "2020",
    "cvv": "737",
    "pin": "sty"
}

cred_card_amex = {
    "name_on_card": "Qui-Gon Jinn",
    "credit_card_number": "370000000000002",
    "expiry_month": "10",
    "expiry_year": "2020",
    "cvv": "7373",
    "pin": "sty"
}

cred_card_diners = {
    "name_on_card": "John Wick",
    "credit_card_number": "36006666333344",
    "expiry_month": "10",
    "expiry_year": "2020",
    "cvv": "737",
    "pin": "sty"
}

cred_card_discover = {
    "name_on_card": "Robert De Niro",
    "credit_card_number": "6445644564456445",
    "expiry_month": "10",
    "expiry_year": "2020",
    "cvv": "737",
    "pin": "sty"
}

cred_card_jcb = {
    "name_on_card": "Sam L Jackson",
    "credit_card_number": "3569990010095841",
    "expiry_month": "10",
    "expiry_year": "2020",
    "cvv": "737",
    "pin": "sty"
}


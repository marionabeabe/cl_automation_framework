import datetime
import strgen
import pytest

random_string = str(strgen.StringGenerator("[\d\w]{6}").render())


now = datetime.datetime.now()
formatted_date = now.strftime("%Y-%m-%d-%H-%M")

test_env = pytest.config.getoption('LAB')
empty_credentials = {
    'url': 'http://money2020asia-{}.lionsfestivals.com/'.format(test_env),
    'username': ' ',
    'password': ' '
}

invalid_credentials = {
        'url': 'http://money2020asia-{}.lionsfestivals.com'.format(test_env),
        "username": "invalid_test_automation_2018-11-12-12-39@cl.com",
        "password": "Automation123invalid"
    }

create_account_data = {
    "url": 'http://money2020asia-{}.lionsfestivals.com/'.format(test_env),
    "title": "Mr",
    "first_name": "John",
    "last_name": "Hannibal",
    "email": "Automation_" + formatted_date + random_string + "@m2020.com",
    "password": "Automation123",
    "job_title": "Product Manager",
    "mobile": "+44327442878",
    "company_name": "The A Team",
    "address1": "A Team Road",
    "address2": "A Team Building",
    "city": "London",
    "postcode": "AT1 TM0",
    "country": "United Kingdom",
    "company_role": "Middle Manager",
    "job_role": "Middle Manager",
    "job_department": "Press and PR",
    "diet_requirements": "Vegetarian",
    "company_stage": "Start-up (established)",
    "company_sector": "Payments",
    "revenue_bracket": "$1bn+"
}

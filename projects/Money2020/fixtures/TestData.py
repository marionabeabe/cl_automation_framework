import os
import datetime
import pytest
import strgen
random_string = str(strgen.StringGenerator("[\d\w]{6}").render())
now = datetime.datetime.now()
formatted_date = now.strftime("%Y-%m-%d-%H-%M")
test_env = pytest.config.getoption('LAB')
#test_env = os.getenv('TEST_ENV', 'uat')
cust = 'customer_uat' if test_env == 'uat' \
    else 'customer_staging' if test_env == 'staging' \
    else 'customer_integration' if test_env == 'integration' \
    else 'customer_prod'

customer = os.getenv('customer', cust)
Environments = {
    "dev": "",
    "uat": "http://money2020asia-uat.lionsfestivals.com/",
    "staging": "http://money2020asia-staging.lionsfestivals.com/",
    "prod": "change",
    "integration": "http://money2020asia-integration.lionsfestivals.com/",
}


Accounts = {
    "admin_uat": {
        "username": "admin@test.com",
        "password": "Password007",
        "person_id": ""
    },
    "customer_uat": {
        "username":
            "M2020_Automation2019-07-12-11-30eFC8rX@m2020.com",
        "password": "Automation123",
        "person_id": "03930f29-90a4-e911-80e7-005056ad6a25"
    },
    "customer_staging": {
        "username":
            "M2020_Automation2019-07-16-10-117FUbun@m2020.com",
        "password": "Automation123",
        "person_id": ""
    },
    "customer_integration": {
        "username":
            "Digital_Pass_CL_Automation_2019-05-28-14-45Ahrr0w@cl.com",
        "password": "Automation123",
        "person_id": ""
    },
    "customer_prod": {
        "username":
            "change",
        "password": "Automation123",
        "person_id": ""
    }
    }

test_data = {
    "url": Environments[test_env],
    "username": Accounts[customer]['username'],
    "password": Accounts[customer]['password'],
    "person_id": Accounts[customer]['person_id'],
    "env": test_env,
    "title": "Mr",
    "comp_activity": "Production",
    "country": "UNITED KINGDOM",
    "job_role": "Technology",
    "company_type": "Photography",
}

delegate_data = {
    "url": Environments[test_env],
    "title": "Ms",
    "first_name": "Mary",
    "last_name": "Popkins",
    "contact": "Mary Popkins",
    "email": "M2020_Automation" + formatted_date + random_string + "@m2020.com",
    "password": "Automation123",
    "job_title": "Product Manager",
    "mobile": "+44327442878",
    "company_name": "Popkins Company",
    "address1": "Popkins Building",
    "address2": "Popkins Road",
    "city": "Edinburgh",
    "postcode": "EC14 1JK",
    "country": "United Kingdom",
    "company_role": "Middle Manager",
    "company_activity": "Production",
    "job_role": "Middle Manager",
    "company_type": "Photography",
    "new_first_name": "John",
    "new_last_name": "Kennedy",
    "new_email": "john.kennedy@abc.com",
    "job_department": "Marketing and Brand Management",
    "diet_requirements": "Vegetarian",
    "company_stage": "Start-up (early stage)",
    "company_sector": "Investors",
    "revenue_bracket": "$1m - $5m"

}

update_delegate_data = {
    "url": Environments[test_env],
    "title": "Prof",
    "first_name": "Vin",
    "last_name": "Diesel",
    'password': "Automation123",
    "job_title": "Fast and Furious Driver",
    "mobile": "+44327442878",
    "company_name": "Furious Company",
    "address1": "Fast Building",
    "address2": "Fast Road",
    "city": "Manchester",
    "postcode": "FAST11 1UHO",
    "country": "United Kingdom",
    "company_role": "Middle Manager",
    "company_activity": "Production",
    "company_type": "Photography",
    "new_password": "Automation124",
    "marketing": {"cl": "checked", "dl":"unchecked"}

}

upgrade_pass_data = {
    "url": Environments[test_env],
    "title": "Mr",
    "first_name": "Luke",
    "last_name": "Skywalker",
    "email": "test_automation_" + formatted_date + "@cl.com",
    "password": "Automation123",
    "job_title": "Product Manager",
    "mobile": "+44327442878",
    "company_name": "Awesome Company",
    "address1": "Awesome Building",
    "address2": "Awesome Road",
    "city": "London",
    "postcode": "WC23 1UHO",
    "country": "United Kingdom",
    "company_role": "Middle Manager",
    "company_activity": "Production",
    "company_type": "Photography"
}

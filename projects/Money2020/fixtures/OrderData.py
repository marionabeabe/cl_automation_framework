import strgen
import os

user = os.getenv('username')
random_string = str(strgen.StringGenerator("[\d\w]{6}").render())

company_data = {
    'add_new_company': 'Add new company',
    'company_name': 'Ascential Events',
    'address_1': '33 Kingsway',
    'address_2': 'Strand Underpass',
    'pobox': 'WC2B',
    'city': 'London',
    'postcode': 'WC2B 6UF',
    'country': 'United Kingdom',
    'email': 'abc@bbc.com'
}

company_data_2 = {
    'add_new_company': 'Add new company',
    'company_name': 'Dragon Events',
    'address_1': '33 Dragonway',
    'address_2': 'Dragon Underpass',
    'pobox': 'WC2B',
    'city': 'Jurong East',
    'postcode': 'SIN1 GP0',
    'country': 'Singapore',
    'email': 'dragon@dragonevents.com'
}

fpath = 'C://Users/{}/automation_framework\projects\CannesLions\\fixtures\C' \
       'apture1.PNG'.format(user)

someother_delegate = {
    'title': 'Ms',
    'first_name': 'Captain',
    'last_name': 'Marvel',
    'job_title': 'Senior Manager',
    'email': 'test_automation@13.com',
    'company': 'XMen Company',
    'address1': 'Hollywood Road',
    'address2': 'Hollywood Building',
    'city': 'London',
    'postcode': 'BH3 UH2',
    'country': 'United Kingdom',
    'comp_activity': 'Media Owner',
    'company_type': 'Entertainment',
    'mobile': '23456789',
    'dob': '12/10/1990',
    'path': fpath,
    'job_department': "Sales/BD",
    'diet_requirements': 'Kosher',
    'company_stage': 'Established business',
    'company_sector': 'Lending',
    'revenue_bracket': '$5m - $20m'
    }

contractor_data = {
    'title': 'Mr',
    'first_name': 'Master',
    'last_name': 'Yoda',
    'job_title': 'Senior Manager',
    'email': 'test_automation@13.com',
    'company': 'Test Awesome Company',
    'address1': '55 Star Wars Road',
    'address2': 'Boba Fett Building',
    'city': 'London',
    'postcode': 'UHO1 UH2',
    'country': 'United Kingdom',
    'comp_activity': 'Media Owner',
    'company_type': 'Entertainment',
    'mobile': '23456789',
    'dob': '05/08/1986',
    'nationality': 'American',
    'city_of_birth': 'Washington DC',
    'country_of_birth': 'United States',
}

senior_officer_data = {
    'officer_full_name': 'The Hulk',
    'officer_position': 'Destroyer'
}

billing_address_data = {
    'company_name': 'Paramount Tech',
    'address_1': 'Paramount Buildings',
    'address_2': 'Paramount Road',
    'pobox': 'EB10',
    'city': 'Old Trafford',
    'postcode': 'OL1 1HS',
    'country': 'UNITED KINGDOM'
}


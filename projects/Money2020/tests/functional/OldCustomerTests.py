from TestDriver import TestDriver
from utils.TestUtils import Expects
from projects.Money2020.fixtures.TestData import test_data
from projects.Money2020.fixtures.PaymentData import cred_card_visa, \
    cred_card_mastercard, cred_card_amex, cred_card_diners, \
    cred_card_discover, cred_card_jcb
from projects.Money2020.fixtures.OrderData import company_data, \
    someother_delegate, company_data_2
from projects.Money2020.pages.AccountPage import M2020AccountPage
from projects.CannesLions.pages.OrderPage import OrderPage
from projects.Money2020.pages.PassPage import M2020PassPage
from projects.CannesLions.pages.APICalls import APICalls


class OldCustomerTests(TestDriver):

    def test_o1_login_with_old_account(self):
        obj = M2020AccountPage(self.driver)
        res = obj.log_into_account(test_data)
        api = APICalls(self)
        assert api.api_remove_all_passes(
            festival='m2020', person_id=test_data["person_id"],
            env=test_data['env'])
        self.assert_and_log('old_account', res)

    @Expects(['old_account'])
    def test_o2_buy_standard_pass_for_some_one_with_visa_credit_card(self):
        obj = M2020PassPage(self.driver)
        (result, total) = obj.add_single_pass(pass_type='The Standard Pass')
        assert result
        obj1 = OrderPage(self.driver)
        assert obj1.process_order("someone", total, account="new",
                                  data=someother_delegate, festival='m2020',
                                  company=company_data)
        res = obj1.pay_for_order('credit_card', card='visa',
                                 data=cred_card_visa, festival='m2020')
        self.assert_and_log('pass_1', res)

    @Expects(['pass_1'])
    def test_o3_buy_standard_pass_for_some_one_with_master_card(self):
        obj = M2020PassPage(self.driver)
        (result, total) = obj.add_single_pass(pass_type='The Standard Pass')
        assert result
        obj1 = OrderPage(self.driver)
        assert obj1.process_order("someone", total, account="new",
                                  data=someother_delegate, festival='m2020',
                                  company=company_data_2)
        res = obj1.pay_for_order('credit_card', card='mc',
                                 data=cred_card_mastercard, festival='m2020')
        self.assert_and_log('pass_2', res)

    @Expects(['pass_2'])
    def test_o4_buy_standard_pass_for_some_one_with_amex(self):
        obj = M2020PassPage(self.driver)
        (result, total) = obj.add_single_pass(pass_type='The Standard Pass')
        assert result
        obj1 = OrderPage(self.driver)
        assert obj1.process_order("someone", total, account="new",
                                  data=someother_delegate, festival='m2020',
                                  company=company_data)
        res = obj1.pay_for_order('credit_card', card='amex',
                                 data=cred_card_amex, festival='m2020')
        self.assert_and_log('pass_3', res)

    @Expects(['pass_3'])
    def test_o5_buy_standard_pass_for_unnamed_with_discover(self):
        obj = M2020PassPage(self.driver)
        (result, total) = obj.add_single_pass(pass_type='The Standard Pass')
        assert result
        obj1 = OrderPage(self.driver)
        assert obj1.process_order("unnamed", total, account="new",
                                  data=someother_delegate, festival='m2020',
                                  company=company_data)
        res = obj1.pay_for_order('credit_card', card='discover',
                                 data=cred_card_discover, festival='m2020')
        self.assert_and_log('pass_4', res)

    @Expects(['pass_4'])
    def test_o6_buy_standard_pass_for_some_one_with_jcb(self):
        obj = M2020PassPage(self.driver)
        (result, total) = obj.add_single_pass(pass_type='The Standard Pass')
        assert result
        obj1 = OrderPage(self.driver)
        assert obj1.process_order("someone", total, account="new",
                                  data=someother_delegate, festival='m2020',
                                  company=company_data)
        res = obj1.pay_for_order('credit_card', card='jcb',
                                 data=cred_card_jcb, festival='m2020')
        self.assert_and_log('pass_5', res)

    @Expects(['pass_5'])
    def test_o7_buy_standard_pass_for_some_one_with_diners(self):
        obj = M2020PassPage(self.driver)
        (result, total) = obj.add_single_pass(pass_type='The Standard Pass')
        assert result
        obj1 = OrderPage(self.driver)
        assert obj1.process_order("someone", total, account="new",
                                  data=someother_delegate, festival='m2020',
                                  company=company_data)
        res = obj1.pay_for_order('credit_card', card='diners',
                                 data=cred_card_diners, festival='m2020')
        self.assert_and_log('pass_6', res)

    # @Expects(['pass_6'])
    # def test_o8_buy_standard_pass_for_unnamed_with_bank_transfer(self):
    #     obj = M2020PassPage(self.driver)
    #     (result, total) = obj.add_single_pass(pass_type='The Standard Pass')
    #     assert result
    #     obj1 = OrderPage(self.driver)
    #     assert obj1.process_order("unnamed", total, account="new",
    #                               data=someother_delegate, festival='m2020',
    #                               company=company_data)
    #     res = obj1.pay_for_order(pay_by='bank_transfer', festival='m2020')
    #     self.assert_and_log('pass_5', res)

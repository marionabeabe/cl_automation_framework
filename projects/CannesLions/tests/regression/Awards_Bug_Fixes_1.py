from TestDriver import TestDriver
from projects.CannesLions.fixtures.TestData import entries_data
from projects.CannesLions.fixtures.PaymentData import awards_cc_visa
from projects.CannesLions.fixtures.OrderData import entries_company_data, \
    senior_officer_data, billing_address_data
from projects.CannesLions.pages.AccountPage import AccountPage
from projects.CannesLions.pages.OrderPage import OrderPage
from utils.TestUtils import Expects
from projects.DubaiLynx.pages.CampaignPage import CampaignPage
from projects.DubaiLynx.fixtures.CampaignData import campaign_data,\
    categories_data
from projects.CannesLions.fixtures.OrderData import company_data
from projects.CannesLions.fixtures.BUGFixTestData import \
    awards_and_categories, bug_fix_campaign_data, new_campaign, ipl_1, ipl_2
from projects.CannesLions.pages.OrderPage import OrderPage
from projects.CannesLions.pages.PassPage import PassPage
from projects.DubaiLynx.pages.OrderPage import OrderPage as DLOrderPage

import platform

linux_path = '/var/lib/jenkins/workspace/files/File2.jpg'


class BUGRegression1(TestDriver):

    def test_cl_bug_1_create_cl_account_and_update_details(self):
        obj = AccountPage(self.driver)
        entries_data['email'] = "Awards_1_" + entries_data['email']
        res = obj.create_account(entries_data, site='cl_awards')
        self.assert_and_log('login', res)

    @Expects(['login'])
    def test_cl_bugs_2_create_campaign_through_three_dots(self):
        obj = CampaignPage(self.driver)
        (campaign, res) = obj.create_campaign(bug_fix_campaign_data, brand='cl')
        assert res
        res = obj.create_entry(
            campaign, awards_and_categories['award'],
            awards_and_categories['category'], entries=None, brand='cl')
        self.assert_and_log('campaign', res)

        assert obj.create_campaign_through_three_dots(
            awards_and_categories['award'], awards_and_categories['selector'],
            yes_no=True, campaign=True, campaign_data=new_campaign)

        assert obj.delete_campaign(campaign)

    @Expects(['login'])
    def test_cl_bugs_3_validate_campaign(self):
        obj = CampaignPage(self.driver)
        # create campaign with brand
        (campaign, res) = obj.create_campaign(ipl_1)
        assert res
        # change campaign to brand-less and change title
        n_campaign = obj.modify_campaign(
            campaign, ['title', 'client_less'], ipl_2)
        assert obj.delete_campaign(n_campaign)

        # create brand-less campaign
        (campaign, res) = obj.create_campaign(ipl_1, client_less=True)
        assert res
        # add a brand to campaign
        assert obj.modify_campaign(
            campaign, 'client_less', ipl_2, fill_brand=True)
        assert obj.delete_campaign(campaign)

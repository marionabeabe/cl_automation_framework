from TestDriver import TestDriver
from projects.CannesLions.fixtures.TestData import delegate_data
from projects.CannesLions.fixtures.PaymentData import awards_cc_visa
from projects.CannesLions.fixtures.OrderData import entries_company_data, \
    senior_officer_data, billing_address_data
from projects.CannesLions.pages.AccountPage import AccountPage
from projects.CannesLions.pages.OrderPage import OrderPage
from utils.TestUtils import Expects
from projects.DubaiLynx.pages.CampaignPage import CampaignPage
from projects.DubaiLynx.fixtures.CampaignData import campaign_data, \
    categories_data
from projects.CannesLions.fixtures.OrderData import company_data
from projects.CannesLions.fixtures.BUGFixTestData import \
    awards_and_categories, bug_fix_campaign_data, new_campaign
from projects.CannesLions.pages.OrderPage import OrderPage
from projects.CannesLions.pages.PassPage import PassPage
from projects.DubaiLynx.pages.OrderPage import OrderPage as DLOrderPage

import platform

linux_path = '/var/lib/jenkins/workspace/files/File2.jpg'


class DelegatesBugFixes1(TestDriver):

    def test_cl_bug_1_create_cl_account_and_update_details(self):
        obj = AccountPage(self.driver)
        delegate_data['email'] = "DEL_" + delegate_data['email']
        res = obj.create_account(delegate_data)
        self.assert_and_log('login', res)

    @Expects(['login'])
    def test_cl_bugs_2_change_booking_and_finance_data(self):
        obj = PassPage(self.driver)
        # obj1 = AccountPage(self.driver)
        # assert obj1.switch_system('delegates')
        (result, total) = obj.add_single_pass(pass_type='networking')
        assert result
        obj = OrderPage(self.driver)
        assert obj.process_order("myself", total)
        assert obj.modify_contact(
            contact='booking', data=delegate_data, modify=True)
        assert obj.modify_contact(
            contact='finance', data=delegate_data, modify=False)
        assert obj.add_invoice(company_data)
        res = obj.pay_for_order(pay_by='bank_transfer', festival='cl')
        self.assert_and_log('pass1', res)

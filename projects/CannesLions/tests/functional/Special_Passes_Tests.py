import strgen

from TestDriver import TestDriver
from projects.CannesLions.fixtures.TestData import test_data, delegate_data
from projects.CannesLions.fixtures.OrderData import contractor_data
from projects.CannesLions.fixtures.SpecialPassesData import media_data, \
    ylc_data, student_data
from projects.CannesLions.pages.LandingPage import LandingPage
from projects.CannesLions.pages.AccountPage import AccountPage
from projects.CannesLions.pages.OrderPage import OrderPage
from projects.CannesLions.pages.PassPage import PassPage
from projects.CannesLions.fixtures.PaymentData import  \
    cred_card_mastercard
from projects.CannesLions.fixtures.OrderData import company_data, \
    someother_delegate
import platform
from utils.TestUtils import Expects

linux_path = '/var/lib/jenkins/workspace/files/Capture1.PNG'


class SpecialPassesTests(TestDriver):

    def test_s1_create_account_login(self):
        obj = LandingPage(self.driver)
        assert obj.load_landing_page(test_data)
        obj = AccountPage(self.driver)
        delegate_data['email'] = "special_pass_tests_" + delegate_data['email']
        res = obj.create_account(delegate_data)
        self.assert_and_log('account', res)

    # Press pass has been closed for 2019
    # @Expects(['account'])
    # def test_s2_buy_press_pass(self):
    #     obj = PassPage(self.driver)
    #     obj.add_pass_from_list_view(delegate_data, pass_type='press')
    #     obj = OrderPage(self.driver)
    #     if platform.system() == 'Linux':
    #         media_data['id_path'] = linux_path
    #     res = obj.process_special_passes("myself", "press", data=media_data,
    #                                      extra_form=True)
    #     self.assert_and_log('press', res)
    #
    # @Expects(['press'])
    # def test_s3_check_pass_count_increase(self):
    #     obj = AccountPage(self.driver)
    #     assert obj.check_pass_count(pending_count=0, ready_count=1)

    @Expects(['account'])
    def test_s4_buy_contractor_pass(self):
        contractor_data['email'] = \
            str(strgen.StringGenerator("[\d\w]{6}").render()) + \
            contractor_data['email']
        obj = PassPage(self.driver)
        assert obj.add_pass_from_list_view(delegate_data,
                                           pass_type='contractor')
        obj = OrderPage(self.driver)
        res = obj.process_special_passes("myself", "contractor",
                                         data=contractor_data)
        self.assert_and_log('contractor', res)

    @Expects(['contractor'])
    def test_s5_check_pass_count_increase(self):
        obj = AccountPage(self.driver)
        assert obj.check_pass_count(pending_count=0, ready_count=1)

    @Expects(['account'])
    def test_s6_buy_festival_representative_pass(self):
        delegate_data['email'] = \
            str(strgen.StringGenerator("[\d\w]{6}").render()) + \
            delegate_data['email']
        obj = PassPage(self.driver)
        assert obj.add_pass_from_list_view(delegate_data,
                                           pass_type='festival_rep')
        obj = OrderPage(self.driver)
        res1 = obj.process_special_passes("someone", "festival_rep",
                                          account='old',
                                          data=someother_delegate)
        self.assert_and_log('festival_rep', res1)

    @Expects(['contractor', 'festival_rep'])
    def test_s7_check_pass_count_increase(self):
        obj = AccountPage(self.driver)
        assert obj.check_pass_count(pending_count=0, ready_count=2)

    # @Expects(['account'])
    # def test_s8_buy_roger_hatchuel_pass(self):
    #     obj = PassPage(self.driver)
    #     assert obj.add_pass_from_list_view(delegate_data,
    #                                        pass_type='roger_hatchuel')
    #     obj = OrderPage(self.driver)
    #     if platform.system() == 'Linux':
    #         student_data['path'] = linux_path
    #     res1 = obj.process_special_passes("someone", "roger_hatchuel",
    #                                       account='old', data=student_data)
    #     self.assert_and_log('hatchuel', res1)

    # @Expects(['press', 'contractor', 'festival_rep'])
    # def test_s9_check_pass_count_increase(self):
    #     obj = AccountPage(self.driver)
    #     assert obj.check_pass_count(pending_count=0, ready_count=3)

    @Expects(['account'])
    def test_ss10_buy_young_lion_competitior(self):
        obj = PassPage(self.driver)
        assert obj.add_pass_from_list_view(delegate_data,
                                           pass_type='young_lion_competitor')
        obj = OrderPage(self.driver)
        if platform.system() == 'Linux':
            ylc_data['path'] = linux_path
        obj.process_order("someone", 1425, account='old', data=ylc_data,
                          delegate='young_lion_competitor')
        assert obj.add_invoice(company_data)
        res1 = obj.pay_for_order('credit_card', card='mc',
                                 data=cred_card_mastercard)
        self.assert_and_log('young_lion', res1)

    @Expects(['contractor', 'festival_rep', 'young_lion'])
    def test_ss11_check_pass_count_increase(self):
        obj = AccountPage(self.driver)
        assert obj.check_pass_count(pending_count=0, ready_count=3)

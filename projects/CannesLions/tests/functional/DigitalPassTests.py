import strgen

from TestDriver import TestDriver
from projects.CannesLions.fixtures.TestData import test_data, delegate_data
from projects.CannesLions.fixtures.OrderData import someother_delegate
from projects.CannesLions.pages.LandingPage import LandingPage
from projects.CannesLions.pages.AccountPage import AccountPage
from projects.CannesLions.pages.OrderPage import OrderPage
from projects.CannesLions.pages.PassPage import PassPage
from projects.CannesLions.fixtures.PaymentData import \
    cred_card_mastercard
from projects.CannesLions.fixtures.DigitalPassTestData import *
from utils.TestUtils import Expects


class DigitalPassTests(TestDriver):

    def test_n1_create_account_login(self):
        obj = LandingPage(self.driver)
        assert obj.load_landing_page(test_data)
        obj = AccountPage(self.driver)
        delegate_data['email'] = "Digital_Pass_" + delegate_data['email']
        res = obj.create_account(delegate_data)
        self.assert_and_log('new_account', res)

    @Expects(['new_account'])
    def test_n2_buy_digital_pass_with_for_myself_with_bank_transfer(self):
        someother_delegate['email'] = \
            str(strgen.StringGenerator("[\d\w]{6}").render()) + \
            someother_delegate['email']
        obj1 = PassPage(self.driver)
        obj = OrderPage(self.driver)
        (result, total) = obj1.add_single_pass(pass_type='digital')
        assert result

        assert obj.process_order("myself", total, skip=True)

        # Digital only - UK with out VAT reg
        assert obj.add_invoice(gb_with_out_tin)
        assert obj.process_tax(total, gb_with_out_tin['country'],
                               digital=True)

        # Digital only - UK with VAT reg
        assert obj.change_invoice_company(gb_with_tin, vat_reg=True)
        assert obj.process_tax(total, gb_with_tin['country'],
                               digital=True)

        # Digital only - France with VAT reg
        assert obj.change_invoice_company(france_with_tin, vat_reg=True)
        assert obj.process_tax(total, france_with_tin['country'],
                               digital=True)

        # Digital only - France with out VAT reg
        assert obj.change_invoice_company(france_with_out_tin)
        assert obj.process_tax(total, france_with_out_tin['country'],
                               digital=True)

        # Digital only - Russia with out VAT reg
        assert obj.change_invoice_company(russia_with_out_tin)
        assert obj.process_tax(total, russia_with_out_tin['country'],
                               digital=True)

        # Digital only - United States with out VAT reg
        assert obj.change_invoice_company(us_with_out_tin)
        assert obj.process_tax(total, us_with_out_tin['country'],
                               digital=True)

        res = obj.pay_for_order('credit_card', card='mc',
                                data=cred_card_mastercard)
        self.assert_and_log('pass1', res)
        multiple_passes = ['networking', 'digital']
        (result, total) = obj1.add_multiple_passes(multiple_passes)
        obj.process_order("myself", total, data=someother_delegate,
                          skip=True, multiple=True,
                          passes=multiple_passes)
        assert obj.process_tax(total, gb_with_out_tin['country'], combined=True,
                               invoice='before')
        # Digital + Networking - UK with out VAT reg
        assert obj.add_invoice(gb_with_out_tin)
        assert obj.process_tax(total, gb_with_out_tin['country'], combined=True,
                               invoice='after')

        # Digital + Networking - UK with VAT reg
        assert obj.change_invoice_company(gb_with_tin, vat_reg=True)
        assert obj.process_tax(total, gb_with_tin['country'], combined=True,
                               invoice='after')

        # Digital + Networking - France with VAT reg
        assert obj.change_invoice_company(france_with_tin, vat_reg=True)
        assert obj.process_tax(total, france_with_tin['country'], combined=True,
                               invoice='after', non_digital_tax=True)

        # Digital + Networking - France with out VAT reg
        assert obj.change_invoice_company(france_with_out_tin)
        assert obj.process_tax(total, france_with_out_tin['country'],
                               combined=True, invoice='after')
        # Digital + Networking - Russia with out  VAT reg
        assert obj.change_invoice_company(russia_with_out_tin)
        assert obj.process_tax(total, russia_with_out_tin['country'],
                               combined=True, invoice='after')
        # Digital + Networking - United States with VAT reg
        assert obj.change_invoice_company(us_with_out_tin)
        assert obj.process_tax(total, us_with_out_tin['country'], combined=True,
                               invoice='after')

from TestDriver import TestDriver
from projects.CannesLions.fixtures.TestData import entries_data
from projects.CannesLions.pages.AccountPage import AccountPage
from projects.CannesLions.pages.OrderPage import OrderPage
from utils.TestUtils import Expects
from projects.DubaiLynx.pages.CampaignPage import CampaignPage
from projects.DubaiLynx.fixtures.CampaignData import campaign_data,\
    categories_data, linux_file_paths
from projects.CannesLions.fixtures.AwardsCategoriesData import awards_data
from projects.CannesLions.fixtures.VATData import country_company_data,\
    campaign_data
import platform


class CLVATTests(TestDriver):

    def test_cl_vat_1_create_cl_account_and_update_details(self):
        obj = AccountPage(self.driver)
        entries_data['email'] = "VAT_Tests" + entries_data['email']
        res = obj.create_account(entries_data, site='cl_awards')
        # self.assert_and_log('login', res)

    # @Expects(['login'])
    def test_cl_vat_check_vat_for_different_countries(self):
        if platform.system() == 'Linux':
            categories_data['media_upload'] = linux_file_paths
        obj = CampaignPage(self.driver)
        campaign_data['title'] = 'Different VAT Countries'
        (campaign, res) = obj.create_campaign(campaign_data, brand='cl')
        assert res
        assert obj.create_entry(campaign, awards_data['award'],
                                awards_data['category'], entries=None,
                                brand='cl')
        assert obj.fill_campaign_details(categories_data, campaign,
                                         award=awards_data['award'], brand='cl',
                                         selector=awards_data['selector'])

        obj = OrderPage(self.driver)
        obj.go_to_payment_page()
        # GB with out VAT
        assert obj.add_company_details('invoice', country_company_data)
        assert obj.is_vat_needed(country_company_data)

        # GB  with VAT
        country_company_data['vat_number'] = 'GB333289454'
        assert obj.change_country_and_vat(country_company_data, fill_vat=True)
        assert obj.is_vat_needed(country_company_data)

        # France with VAT
        country_company_data['vat_number'] = 'FR01353534506'
        country_company_data['country'] = 'FRANCE'
        assert obj.change_country_and_vat(country_company_data, fill_vat=True)
        assert not obj.is_vat_needed(country_company_data)
        country_company_data.pop('vat_number')

        # France with out VAT
        country_company_data['country'] = 'FRANCE'
        assert obj.change_country_and_vat(country_company_data)
        assert obj.is_vat_needed(country_company_data)

        # JAPAN - ROW - NO VAT
        country_company_data['country'] = 'JAPAN'
        assert obj.change_country_and_vat(country_company_data)
        assert not obj.is_vat_needed(country_company_data)

        # USA - ROW - NO VAT
        country_company_data['country'] = 'USA'
        assert obj.change_country_and_vat(country_company_data)
        assert not obj.is_vat_needed(country_company_data)

import strgen
import pytest

from TestDriver import TestDriver
from utils.TestUtils import Expects
from projects.CannesLions.fixtures.TestData import test_data
from projects.CannesLions.fixtures.PaymentData import cred_card_visa, \
    cred_card_mastercard, cred_card_amex
from projects.CannesLions.fixtures.OrderData import company_data, \
    someother_delegate
from projects.CannesLions.pages.LandingPage import LandingPage
from projects.CannesLions.pages.AccountPage import AccountPage
from projects.CannesLions.pages.OrderPage import OrderPage
from projects.CannesLions.pages.PassPage import PassPage
from projects.CannesLions.pages.APICalls import APICalls


class OldCustomerTest(TestDriver):

    def test_o1_login_with_old_account(self):
        obj = LandingPage(self.driver)
        assert obj.load_landing_page(test_data)
        obj = AccountPage(self.driver)
        res = obj.log_into_account(test_data)
        self.assert_and_log('new_account', res)
        api = APICalls(self)
        api.api_remove_all_passes(test_data["person_id"], env=test_data['env'])

    @Expects(['new_account'])
    def test_o3_buy_networking_pass_for_some_one_with_visa_credit_card(self):
        obj = PassPage(self.driver)
        (result, total) = obj.add_single_pass(pass_type='networking')
        assert result
        obj1 = OrderPage(self.driver)
        test_env = pytest.config.getoption('LAB')
        if test_env == 'uat':
            assert obj1.process_order("myself", total)
        else:
            assert obj1.process_order("someone", total, account="old",
                                      data=someother_delegate)
        assert obj1.add_invoice(company_data)
        res = obj1.pay_for_order('credit_card', card='visa',
                                 data=cred_card_visa)
        self.assert_and_log('pass_1', res)

    @Expects(['pass_1'])
    def test_o4_buy_complete_gold_pass_for_some_one_with_bank_transfer(self):
        someother_delegate['email'] = \
            str(strgen.StringGenerator("[\d\w]{6}").render()) + \
            someother_delegate['email']
        obj = PassPage(self.driver)
        (result, total) = obj.add_single_pass(pass_type='complete', quantity=1,
                                              upgrade='complete_gold')
        assert result
        obj1 = OrderPage(self.driver)
        assert obj1.process_order("someone", total,  account="old",
                                  data=someother_delegate)
        assert obj1.add_invoice(company_data)
        res = obj1.pay_for_order('bank_transfer', festival='cl')
        self.assert_and_log('pass_2', res)

    @Expects(['pass_2'])
    def test_o5_buy_complete_platinum_pass_for_someone_with_mastercard(self):
        someother_delegate['email'] = \
            str(strgen.StringGenerator("[\d\w]{6}").render()) + \
            someother_delegate['email']
        obj = PassPage(self.driver)
        (result, total) = obj.add_single_pass(pass_type='complete',
                                              quantity=1,
                                              upgrade='complete_gold')
        assert result
        obj1 = OrderPage(self.driver)
        assert obj1.process_order("someone", total, account="old",
                                  data=someother_delegate)
        assert obj1.add_invoice(company_data)
        res = obj1.pay_for_order('credit_card', card='mc',
                                 data=cred_card_mastercard)
        self.assert_and_log('pass_3', res)

    @Expects(['pass_3'])
    def test_o6_buy_lions_health_pass_for_someone_with_amex(self):
        someother_delegate['email'] = \
            str(strgen.StringGenerator("[\d\w]{6}").render()) + \
            someother_delegate['email']
        obj = PassPage(self.driver)
        (result, total) = obj.add_single_pass(pass_type='lions_health',
                                              quantity=1)
        obj1 = OrderPage(self.driver)
        assert obj1.process_order("someone", total, account="old",
                                  data=someother_delegate)
        assert obj1.add_invoice(company_data)
        res = obj1.pay_for_order('credit_card', card='amex',
                                 data=cred_card_amex)
        self.assert_and_log('pass_4', res)

    @Expects(['pass_4'])
    def test_o7_buy_complete_for_someone_with_banktransfer(self):
        someother_delegate['email'] = \
            str(strgen.StringGenerator("[\d\w]{6}").render()) + \
            someother_delegate['email']
        obj = PassPage(self.driver)
        (result, total) = obj.add_single_pass(pass_type='complete', quantity=1)
        assert result
        obj1 = OrderPage(self.driver)
        assert obj1.process_order("someone", total, account="old",
                                  data=someother_delegate)
        assert obj1.add_invoice(company_data)
        res = obj1.pay_for_order('bank_transfer', festival='cl')
        self.assert_and_log('pass_5', res)

    @Expects(['pass_5'])
    def test_o8_buy_complete_student_for_unnamed_using_mastercard(self):
        someother_delegate['email'] = \
            str(strgen.StringGenerator("[\d\w]{6}").render()) + \
            someother_delegate['email']
        obj = PassPage(self.driver)
        (result, total) = obj.add_single_pass(pass_type='complete', quantity=1)
        assert result
        obj1 = OrderPage(self.driver)
        assert obj1.process_order("someone", total, account="old",
                                  data=someother_delegate)
        assert obj1.add_invoice(company_data)
        res = obj1.pay_for_order('credit_card', card='mc',
                                 data=cred_card_mastercard)
        self.assert_and_log('pass_6', res)

    @Expects(['pass_6'])
    def test_o9_logout(self):
        obj = AccountPage(self.driver)
        assert obj.logout(test_data)

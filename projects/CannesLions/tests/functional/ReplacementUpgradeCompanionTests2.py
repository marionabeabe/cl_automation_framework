import  platform
from TestDriver import TestDriver
from projects.CannesLions.fixtures.TestData import test_data, delegate_data
from projects.CannesLions.fixtures.PaymentData import cred_card_visa, \
    cred_card_mastercard, cred_card_amex
from projects.CannesLions.fixtures.OrderData import company_data, \
    someother_delegate
from projects.CannesLions.fixtures.UpdateData import companion_details, \
    replace_delegate
from projects.CannesLions.pages.LandingPage import LandingPage
from projects.CannesLions.pages.PassManagerPage import PassManagerPage
from projects.CannesLions.pages.AccountPage import AccountPage
from projects.CannesLions.pages.OrderPage import OrderPage
from projects.CannesLions.pages.PassPage import PassPage
from utils.TestUtils import Expects

linux_path = '/var/lib/jenkins/workspace/files/Capture1.PNG'


class ReplacementUpgradeCompTests2(TestDriver):

    def test_r0_create_account_login(self):
        obj = LandingPage(self.driver)
        assert obj.load_landing_page(test_data)
        obj = AccountPage(self.driver)
        delegate_data['email'] = "RUC_" + delegate_data['email']
        res = obj.create_account(delegate_data)
        self.assert_and_log('new_account', res)

    @Expects(['new_account'])
    def test_rr11_buy_unnamed_pass_companion_upgrade(self):
        obj1 = PassPage(self.driver)
        obj2 = OrderPage(self.driver)
        obj3 = PassManagerPage(self.driver)

        # unnamed registration >> companion >> upgrade
        (result, total) = obj1.add_single_pass(pass_type='complete')
        assert result
        assert obj2.process_order(person="unnamed", total=total, account="old",
                                  data=someother_delegate,)
        assert obj2.add_invoice(company_data)
        assert obj2.pay_for_order(pay_by='bank_transfer', festival='cl')

        obj2.process_delegate_details(
            person="provide_more", account="old", data=someother_delegate,
            fill=True, pass_type='Complete')

        assert obj3.upgrade_booked_pass(
            pass_type='Complete', upgrade='companion',
            data=companion_details, person="someone", account="old")
        assert obj2.pay_for_order(pay_by='bank_transfer', festival='cl')

        assert obj3.upgrade_booked_pass(
            pass_type='Complete + Companion', upgrade='pass_type',
            data=companion_details, upgrade_pass="Complete Gold + Companion")
        res = obj2.pay_for_order(pay_by='bank_transfer', festival='cl')
        self.assert_and_log('pass11', res)

    @Expects(['pass11'])
    def test_rr12_buy_unnamed_pass_upgrade_companion(self):
        obj1 = PassPage(self.driver)
        obj2 = OrderPage(self.driver)
        obj3 = PassManagerPage(self.driver)

        # unnamed registration >> upgrade >> companion
        (result, total) = obj1.add_single_pass(pass_type='networking')
        assert result
        assert obj2.process_order(person="unnamed", total=total, account="old",
                                  data=someother_delegate,
                                  pass_type='Networking')
        assert obj2.add_invoice(company_data)
        assert obj2.pay_for_order(pay_by='bank_transfer', festival='cl')

        obj2.process_delegate_details(
            person="provide_more", account="old", data=someother_delegate,
            fill=True, pass_type='Networking')

        assert obj3.upgrade_booked_pass(
            pass_type='Networking', upgrade='pass_type',
            data=companion_details, upgrade_pass="Complete Gold")
        assert obj2.pay_for_order(pay_by='bank_transfer', festival='cl')

        assert obj3.upgrade_booked_pass(
            pass_type='Complete Gold', upgrade='companion',
            data=companion_details, person="someone", account="old")

        res = obj2.pay_for_order(pay_by='bank_transfer', festival='cl')
        self.assert_and_log('pass12', res)

    @Expects(['pass12'])
    def test_rr13_buy_unnamed_comp_upgrade_replace_upgrade(self):
        obj1 = PassPage(self.driver)
        obj2 = OrderPage(self.driver)
        obj3 = PassManagerPage(self.driver)
        # unnamed registration + companion >> upgrade >> replacement >> upgrade
        (result, total) = obj1.add_single_pass(pass_type='complete')
        assert result
        assert obj2.process_order(person="unnamed", total=total, account="old",
                                  data=someother_delegate, )
        assert obj2.add_invoice(company_data)
        assert obj2.pay_for_order(pay_by='bank_transfer', festival='cl')

        obj2.process_delegate_details(
            person="provide_more", account="old", data=someother_delegate,
            fill=True, pass_type='Complete')
        assert obj3.upgrade_booked_pass(
            pass_type='Complete', upgrade='companion',
            data=companion_details, person="someone", account="old")
        assert obj2.pay_for_order(pay_by='bank_transfer', festival='cl')

        assert obj3.upgrade_booked_pass(
            pass_type='Complete + Companion', upgrade='pass_type',
            data=companion_details, upgrade_pass="Complete Gold + Companion")
        assert obj2.pay_for_order(pay_by='bank_transfer', festival='cl')

        assert obj3.upgrade_booked_pass(
            pass_type='Complete Gold + Companion', upgrade='replace',
            data=replace_delegate, person="someone", account='new',
            replace_comp=True,)
        assert obj2.pay_for_order(pay_by='bank_transfer', festival='cl')

        assert obj3.upgrade_booked_pass(
            pass_type='Complete Gold + Companion', upgrade='pass_type',
            data=companion_details,
            upgrade_pass="Complete Platinum + Companion", correction=True)
        res = obj2.pay_for_order(pay_by='bank_transfer', festival='cl')
        self.assert_and_log('pass13', res)

    @Expects(['pass13'])
    def test_rr14_buy_unnamed_comp_replace_replace_upgrade(self):
        obj1 = PassPage(self.driver)
        obj2 = OrderPage(self.driver)
        obj3 = PassManagerPage(self.driver)
        # unnamed registration + companion >> replace >> replace >> upgrade
        (result, total) = obj1.add_single_pass(pass_type='complete')
        assert result
        assert obj2.process_order(person="unnamed", total=total, account="old",
                                  data=someother_delegate, )
        assert obj2.add_invoice(company_data)
        assert obj2.pay_for_order(pay_by='bank_transfer', festival='cl')

        obj2.process_delegate_details(
            person="provide_more", account="old", data=someother_delegate,
            fill=True, pass_type='Complete')
        assert obj3.upgrade_booked_pass(
            pass_type='Complete', upgrade='companion',
            data=companion_details, person="someone", account="old")
        assert obj2.pay_for_order(pay_by='bank_transfer', festival='cl')

        assert obj3.upgrade_booked_pass(
            pass_type='Complete + Companion', upgrade='replace',
            data=replace_delegate, person="someone", account='new',
            replace_comp=True, )
        assert obj2.pay_for_order(pay_by='bank_transfer', festival='cl')

        assert obj3.upgrade_booked_pass(
            pass_type='Complete + Companion', upgrade='replace',
            data=replace_delegate, person="someone", account='new',
            replace_comp=True, )
        assert obj2.pay_for_order(pay_by='bank_transfer', festival='cl')

        assert obj3.upgrade_booked_pass(
            pass_type='Complete + Companion', upgrade='pass_type',
            data=companion_details,
            upgrade_pass="Complete Gold + Companion")
        res = obj2.pay_for_order(pay_by='bank_transfer', festival='cl')
        self.assert_and_log('pass14', res)

    @Expects(['pass14'])
    def test_rr15_buy_unnamed_replace_comp_upgrade(self):
        obj1 = PassPage(self.driver)
        obj2 = OrderPage(self.driver)
        obj3 = PassManagerPage(self.driver)
        # unnamed registration + named >> replace >> companion >> upgrade
        (result, total) = obj1.add_single_pass(pass_type='complete')
        assert result
        assert obj2.process_order(person="unnamed", total=total,
                                  account="old",
                                  data=someother_delegate, )
        assert obj2.add_invoice(company_data)
        assert obj2.pay_for_order(pay_by='bank_transfer', festival='cl')

        obj2.process_delegate_details(
            person="provide_more", account="old", data=someother_delegate,
            fill=True, pass_type='Complete')

        assert obj3.upgrade_booked_pass(
            pass_type='Complete', upgrade='replace',
            data=replace_delegate, person="someone", account='new')
        assert obj2.pay_for_order(pay_by='bank_transfer', festival='cl')

        assert obj3.upgrade_booked_pass(
            pass_type='Complete', upgrade='companion',
            data=companion_details, person="someone", account="old")
        assert obj2.pay_for_order(pay_by='bank_transfer', festival='cl')

        assert obj3.upgrade_booked_pass(
            pass_type='Complete + Companion', upgrade='pass_type',
            data=companion_details,
            upgrade_pass="Complete Gold + Companion")
        res = obj2.pay_for_order(pay_by='bank_transfer', festival='cl')
        self.assert_and_log('pass15', res)

    @Expects(['pass15'])
    def test_rr16_buy_unnamed_named_upgrade_comp(self):
        obj1 = PassPage(self.driver)
        obj2 = OrderPage(self.driver)
        obj3 = PassManagerPage(self.driver)
        # unnamed registration + named >> upgrade >> companion
        (result, total) = obj1.add_single_pass(pass_type='complete')
        assert result
        assert obj2.process_order(person="unnamed", total=total,
                                  account="old",
                                  data=someother_delegate, )
        assert obj2.add_invoice(company_data)
        assert obj2.pay_for_order(pay_by='bank_transfer', festival='cl')

        obj2.process_delegate_details(
            person="provide_more", account="old", data=someother_delegate,
            fill=True, pass_type='Complete')

        assert obj3.upgrade_booked_pass(
            pass_type='Complete', upgrade='pass_type',
            data=companion_details,
            upgrade_pass="Complete Gold")
        assert obj2.pay_for_order(pay_by='bank_transfer', festival='cl')

        assert obj3.upgrade_booked_pass(
            pass_type='Complete Gold', upgrade='companion',
            data=companion_details, person="someone", account="old")
        res = obj2.pay_for_order(pay_by='bank_transfer', festival='cl')
        self.assert_and_log('pass16', res)

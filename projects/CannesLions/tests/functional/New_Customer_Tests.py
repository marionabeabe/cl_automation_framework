from TestDriver import TestDriver
from utils.TestUtils import Expects
from projects.CannesLions.fixtures.TestData import test_data, delegate_data
from projects.CannesLions.fixtures.PaymentData import \
    cred_card_mastercard, cred_card_amex
from projects.CannesLions.fixtures.OrderData import company_data, \
    someother_delegate
from projects.CannesLions.pages.LandingPage import LandingPage
from projects.CannesLions.pages.AccountPage import AccountPage
from projects.CannesLions.pages.OrderPage import OrderPage
from projects.CannesLions.pages.PassPage import PassPage
import strgen


class NewCustomerTests(TestDriver):

    def test_n1_create_account_login(self):
        obj = LandingPage(self.driver)
        assert obj.load_landing_page(test_data)
        obj = AccountPage(self.driver)
        delegate_data['email'] = "new_cust_tests_" + delegate_data['email']
        res = obj.create_account(delegate_data)
        self.assert_and_log('new_account', res)

    @Expects(['new_account'])
    def test_n2_buy_networking_pass_with_for_myself_with_bank_transfer(self):
        obj = PassPage(self.driver)
        (result, total) = obj.add_single_pass(pass_type='networking')
        assert result
        obj = OrderPage(self.driver)
        assert obj.process_order("myself", total)
        assert obj.add_invoice(company_data)
        res = obj.pay_for_order(pay_by='bank_transfer', festival='cl')
        self.assert_and_log('pass1', res)

    @Expects(['pass1'])
    def test_n3_buy_complete_pass_for_some_one_with_visa_credit_card(self):
        someother_delegate['email'] = \
            str(strgen.StringGenerator("[\d\w]{6}").render()) + \
            someother_delegate['email']
        obj = PassPage(self.driver)
        (result, total) = obj.add_single_pass(pass_type='complete')
        assert result
        obj1 = OrderPage(self.driver)
        assert obj1.process_order("someone", total, account="old",
                                  data=someother_delegate)
        assert obj1.add_invoice(company_data)
        res = obj1.pay_for_order('credit_card', card='visa',
                                 data=cred_card_mastercard)
        self.assert_and_log('pass2', res)

    @Expects(['pass2'])
    def test_n4_buy_complete_gold_pass_for_some_one_with_bank_transfer(self):
        someother_delegate['email'] = \
            str(strgen.StringGenerator("[\d\w]{6}").render()) + \
            someother_delegate['email']
        obj = PassPage(self.driver)
        (result, total) = obj.add_single_pass(pass_type='complete',
                                              quantity=1,
                                              upgrade='complete_gold')
        assert result
        obj1 = OrderPage(self.driver)
        assert obj1.process_order("someone", total, account="old",
                                  data=someother_delegate)
        assert obj1.add_invoice(company_data)
        res = obj1.pay_for_order('bank_transfer', festival='cl')
        self.assert_and_log('pass3', res)

    @Expects(['pass3'])
    def test_n5_buy_complete_platinum_pass_for_someone_with_mastercard(self):
        someother_delegate['email'] = \
            str(strgen.StringGenerator("[\d\w]{6}").render()) + \
            someother_delegate['email']
        obj = PassPage(self.driver)
        (result, total) = obj.add_single_pass(pass_type='complete', quantity=1,
                                              upgrade='complete_gold')
        assert result
        obj1 = OrderPage(self.driver)
        assert obj1.process_order("someone", total, account="old",
                                  data=someother_delegate)
        assert obj1.add_invoice(company_data)
        res = obj1.pay_for_order('credit_card', card='mc',
                                 data=cred_card_mastercard)
        self.assert_and_log('pass4', res)

    @Expects(['pass4'])
    def test_n6_buy_6_complete_gold_pass_for_someone_with_mastercard(self):
        someother_delegate['email'] = \
            str(strgen.StringGenerator("[\d\w]{6}").render()) + \
            someother_delegate['email']
        obj = PassPage(self.driver)
        (result, total) = obj.add_single_pass(pass_type='complete', quantity=1,
                                              upgrade='complete_gold')
        assert result
        obj1 = OrderPage(self.driver)
        assert obj1.process_order("someone", total, account="old",
                                  data=someother_delegate)
        assert obj1.add_invoice(company_data)
        res = obj1.pay_for_order('credit_card', card='mc',
                                 data=cred_card_mastercard)
        self.assert_and_log('pass5', res)

    @Expects(['pass5'])
    def test_n7_buy_lions_health_pass_for_someone_with_amex(self):
        someother_delegate['email'] = \
            str(strgen.StringGenerator("[\d\w]{6}").render()) + \
            someother_delegate['email']
        obj = PassPage(self.driver)
        (result, total) = obj.add_single_pass(pass_type='lions_health',
                                              quantity=1)
        assert result
        obj1 = OrderPage(self.driver)
        assert obj1.process_order("someone", total, account="old",
                                  data=someother_delegate)
        assert obj1.add_invoice(company_data)
        res = obj1.pay_for_order('credit_card', card='amex',
                                 data=cred_card_amex)
        self.assert_and_log('pass6', res)

    @Expects(['pass6'])
    def test_n8_buy_complete_for_someone_with_banktransfer(self):
        someother_delegate['email'] = \
            str(strgen.StringGenerator("[\d\w]{6}").render()) + \
            someother_delegate['email']
        obj = PassPage(self.driver)
        (result, total) = obj.add_single_pass(pass_type='complete')
        assert result
        obj1 = OrderPage(self.driver)
        assert obj1.process_order("someone", total, account="old",
                                  data=someother_delegate)
        assert obj1.add_invoice(company_data)
        res = obj1.pay_for_order('bank_transfer', festival='cl')
        self.assert_and_log('pass7', res)

    # @Expects(['pass7'])
    # def test_n9_buy_complete_pass_for_unnamed_using_mastercard(self):
    #     someother_delegate['email'] = \
    #         str(strgen.StringGenerator("[\d\w]{6}").render()) + \
    #         someother_delegate['email']
    #     obj = PassPage(self.driver)
    #     (result, total) = obj.add_single_pass(pass_type='complete')
    #     assert result
    #     obj1 = OrderPage(self.driver)
    #     assert obj1.process_order("someone", total, account="old",
    #                               data=someother_delegate)
    #     assert obj1.add_invoice(company_data)
    #     res = obj1.pay_for_order('credit_card', card='mc',
    #                              data=cred_card_mastercard)
    #     self.assert_and_log('pass8', res)

    @Expects(['new_account'])
    def test_nz10_logout(self):
        obj = AccountPage(self.driver)
        assert obj.logout(test_data)

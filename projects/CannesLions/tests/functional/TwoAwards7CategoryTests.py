from TestDriver import TestDriver
from projects.CannesLions.fixtures.TestData import entries_data
from projects.CannesLions.fixtures.PaymentData import awards_cc_visa
from projects.CannesLions.fixtures.OrderData import entries_company_data, \
    senior_officer_data, billing_address_data
from projects.CannesLions.pages.AccountPage import AccountPage
from projects.CannesLions.pages.OrderPage import OrderPage
from utils.TestUtils import Expects
from projects.DubaiLynx.pages.CampaignPage import CampaignPage
from projects.DubaiLynx.fixtures.CampaignData import campaign_data,\
    categories_data
from projects.CannesLions.fixtures.AwardsCategoriesData import \
    two_awards_seven_categories
from projects.DubaiLynx.pages.OrderPage import OrderPage as DLOrderPage

import platform

linux_path = '/var/lib/jenkins/workspace/files/File2.jpg'


class CLAwardsTests2(TestDriver):

    def test_cl_aw2_1_create_cl_account_and_update_details(self):
        obj = AccountPage(self.driver)
        entries_data['email'] = "Two_Awards_Mult_" + entries_data['email']
        res = obj.create_account(entries_data, site='cl_awards')
        # obj = DLAccountPage(self.driver)
        # res = obj.update_details(entries_data)
        self.assert_and_log('login', res)

    @Expects(['login'])
    def test_cl_aw2_2_create_campaign_with_one_entry(self):
        if platform.system() == 'Linux':
            categories_data['media_upload']['file'] = linux_path
        obj = CampaignPage(self.driver)
        (campaign, res) = obj.create_campaign(campaign_data, brand='cl')
        assert res
        assert obj.create_entry(campaign, two_awards_seven_categories['award'],
                                two_awards_seven_categories['category'],
                                entries=None,
                                brand='cl')
        res = obj.fill_campaign_details(categories_data, campaign,
                                        two_awards_seven_categories['entries'],
                                        brand='cl')
        self.assert_and_log('multiple_fill', res)

    @Expects(['multiple_fill'])
    def test_cl_aw2_3_process_order(self):
        obj = OrderPage(self.driver)
        obj.go_to_payment_page()
        assert obj.add_company_details('invoice', entries_company_data)
        assert obj.add_senior_officer(senior_officer_data)
        assert obj.add_company_details('billing', billing_address_data)

        obj1 = DLOrderPage(self.driver)
        entries = obj1.process_entries(two_awards_seven_categories['entries'],
                                       brand='cl')
        obj1.process_totals(entries, brand='cl')
        assert obj.process_entries_payment('credit_card', awards_cc_visa)

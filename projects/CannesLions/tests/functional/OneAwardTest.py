from TestDriver import TestDriver
from projects.CannesLions.fixtures.TestData import entries_data
from projects.CannesLions.pages.AccountPage import AccountPage
from projects.CannesLions.pages.OrderPage import OrderPage
from projects.CannesLions.fixtures.OrderData import \
    senior_officer_data, billing_address_data, entries_company_data
from utils.TestUtils import Expects
from projects.DubaiLynx.pages.CampaignPage import CampaignPage
from projects.DubaiLynx.pages.OrderPage import OrderPage as DLOrderPage
from projects.DubaiLynx.fixtures.CampaignData import campaign_data,\
    categories_data, linux_file_paths
from projects.CannesLions.fixtures.AwardsCategoriesData import awards_data
from projects.CannesLions.fixtures.PaymentData import awards_cc_visa
import platform


linux_path = '/var/lib/jenkins/workspace/files/File2.jpg'


class CLAwardsTests1(TestDriver):

    def test_cl_aw1_1_create_cl_account_and_update_details(self):
        obj = AccountPage(self.driver)
        entries_data['email'] = "One_Award_" + entries_data['email']
        res = obj.create_account(entries_data, site='cl_awards')
        self.assert_and_log('login', res)

    @Expects(['login'])
    def test_cl_aw1_2_create_campaign_with_one_entry(self):
        if platform.system() == 'Linux':
            categories_data['media_upload'] = linux_file_paths
        obj = CampaignPage(self.driver)
        (campaign, res) = obj.create_campaign(campaign_data, brand='cl')
        assert res
        assert obj.create_entry(campaign, awards_data['award'],
                                awards_data['category'], entries=None,
                                brand='cl')
        res = obj.fill_campaign_details(categories_data, campaign,
                                        award=awards_data['award'], brand='cl',
                                        selector=awards_data['selector'])
        self.assert_and_log('single_fill', res)

    @Expects(['single_fill'])
    def test_cl_aw1_3_process_order(self):
        obj = OrderPage(self.driver)
        obj.go_to_payment_page()
        assert obj.add_company_details('invoice', entries_company_data)
        assert obj.add_senior_officer(senior_officer_data)
        assert obj.add_company_details('billing', billing_address_data)

        obj1 = DLOrderPage(self.driver)
        entries = obj1.process_entries(awards_data['entries'], brand='cl')
        obj1.process_totals(entries, brand='cl')
        assert obj.process_entries_payment('credit_card', awards_cc_visa)

# from unittest import TestLoader, TestSuite
# from HtmlTestRunner import HTMLTestRunner
#
# example_tests = TestLoader().loadTestsFromTestCase(CLAwardsTests1)
#
# suite = TestSuite([example_tests])
#
# runner = HTMLTestRunner(output='example_suite')
#
# #runner.run(suite)
import strgen

from TestDriver import TestDriver
from projects.CannesLions.fixtures.TestData import test_data, delegate_data
from projects.CannesLions.fixtures.UpdateData import update_pass_data, \
    companion_details, replace_delegate, visa_details
from projects.CannesLions.pages.LandingPage import LandingPage
from projects.CannesLions.pages.AccountPage import AccountPage
from projects.CannesLions.pages.PassPage import PassPage
from projects.CannesLions.pages.OrderPage import OrderPage
from projects.CannesLions.fixtures.PaymentData import cred_card_visa,\
    cred_card_mastercard
from projects.CannesLions.fixtures.OrderData import company_data, \
    someother_delegate
from projects.CannesLions.pages.PassManagerPage import PassManagerPage
from utils.TestUtils import Expects


class AccountUpgradeTests(TestDriver):

    def test_au1_add_account(self):
        obj = LandingPage(self.driver)
        assert obj.load_landing_page(test_data)
        obj = AccountPage(self.driver)
        delegate_data['email'] = "acco_upgrade_tests_" + delegate_data['email']
        res = obj.create_account(delegate_data)
        self.assert_and_log('new_account', res)

    @Expects(['new_account'])
    def test_au2_buy_complete_pass_for_some_one_with_visa_credit_card(self):
        obj = PassPage(self.driver)
        (result, total) = obj.add_single_pass(pass_type='complete')
        assert result
        obj1 = OrderPage(self.driver)
        assert obj1.process_order("myself", total)
        assert obj1.add_invoice(company_data)
        res = obj1.pay_for_order('credit_card', card='mc',
                                 data=cred_card_mastercard)
        self.assert_and_log('pass_1', res)

    @Expects(['pass_1'])
    def test_au3_change_delegate_details(self):
        obj = PassManagerPage(self.driver)
        res = obj.upgrade_booked_pass('Complete', 'delegate', update_pass_data)
        self.assert_and_log('pass_2', res)

    @Expects(['pass_2'])
    def test_au4_replace_delegate(self):
        obj = PassManagerPage(self.driver)
        assert obj.upgrade_booked_pass('Complete', 'replace', replace_delegate,
                                       person="someone", account="old")
        obj = OrderPage(self.driver)
        res = obj.pay_for_order('credit_card', card='mc',
                                data=cred_card_mastercard)
        self.assert_and_log('res_1', res)

    @Expects(['res_1'])
    def test_au5_upgrade_pass(self):
        obj = PassManagerPage(self.driver)
        assert obj.upgrade_booked_pass('Complete', 'pass_type',
                                       companion_details, "Complete Gold")
        obj = OrderPage(self.driver)
        res = obj.pay_for_order('bank_transfer', festival='cl')
        self.assert_and_log('upgrade', res)

    @Expects(['upgrade'])
    def test_au6_request_visa_letter(self):
        obj = PassManagerPage(self.driver)
        res = obj.upgrade_booked_pass('Complete Gold', 'visa', visa_details)
        self.assert_and_log('visa', res)

    # def test_au9_add_a_companion(self):
    #     obj = PassManagerPage(self.driver)
    #     assert obj.upgrade_booked_pass('Complete Gold', 'companion',
    #                                    companion_details)
    #     obj = OrderPage(self.driver)
    #     assert obj.pay_for_order('bank_transfer')

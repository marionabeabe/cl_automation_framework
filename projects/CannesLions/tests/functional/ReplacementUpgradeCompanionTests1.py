import platform
import strgen

from TestDriver import TestDriver
from projects.CannesLions.fixtures.TestData import test_data, delegate_data
from projects.CannesLions.fixtures.OrderData import company_data, \
    someother_delegate
from projects.CannesLions.fixtures.UpdateData import companion_details, \
    replace_delegate
from projects.CannesLions.pages.LandingPage import LandingPage
from projects.CannesLions.pages.PassManagerPage import PassManagerPage
from projects.CannesLions.pages.AccountPage import AccountPage
from projects.CannesLions.pages.OrderPage import OrderPage
from projects.CannesLions.pages.PassPage import PassPage
from utils.TestUtils import Expects
from projects.CannesLions.fixtures.PaymentData import \
    cred_card_mastercard

linux_path = '/var/lib/jenkins/workspace/files/Capture1.PNG'


class ReplacementUpgradeCompTests(TestDriver):

    def test_r0_create_account_login(self):
        obj = LandingPage(self.driver)
        assert obj.load_landing_page(test_data)
        obj = AccountPage(self.driver)
        delegate_data['email'] = "RUC_" + delegate_data['email']
        res = obj.create_account(delegate_data)
        self.assert_and_log('new_account', res)

    @Expects(['new_account'])
    def test_r1_buy_pass_replace_companion_upgrade(self):
        obj1 = PassPage(self.driver)
        obj2 = OrderPage(self.driver)
        obj3 = PassManagerPage(self.driver)

        # named registration >> replacement >> companion >> upgrade
        (result, total) = obj1.add_single_pass(pass_type='complete')
        assert result
        assert obj2.process_order("myself", total)
        assert obj2.add_invoice(company_data)
        assert obj2.pay_for_order(pay_by='bank_transfer', festival='cl')
        assert obj3.upgrade_booked_pass('Complete', 'replace',
                                        replace_delegate,
                                        person="someone", account="old")
        assert obj2.pay_for_order(pay_by='credit_card', card='mc',
                                  data=cred_card_mastercard)
        assert obj3.upgrade_booked_pass('Complete', 'companion',
                                        companion_details,
                                        person="someone", account="old")
        assert obj2.pay_for_order(pay_by='credit_card', card='mc',
                                  data=cred_card_mastercard)
        assert obj3.upgrade_booked_pass('Complete + Companion', 'pass_type',
                                        companion_details,
                                        "Complete Gold + Companion")
        res = obj2.pay_for_order(pay_by='credit_card', card='mc',
                                 data=cred_card_mastercard)
        self.assert_and_log('pass1', res)

    @Expects(['pass1'])
    def test_r2_buy_pass_replace_upgrade(self):
        someother_delegate['email'] = \
            str(strgen.StringGenerator("[\d\w]{6}").render()) + \
            someother_delegate['email']
        obj1 = PassPage(self.driver)
        obj2 = OrderPage(self.driver)
        obj3 = PassManagerPage(self.driver)

        # named registration >> replacement >> upgrade
        (result, total) = obj1.add_single_pass(pass_type='networking')
        assert result
        assert obj2.process_order("someone", total, account="old",
                                  data=someother_delegate)
        assert obj2.add_invoice(company_data)
        assert obj2.pay_for_order(pay_by='credit_card', card='mc',
                                  data=cred_card_mastercard)
        assert obj3.upgrade_booked_pass('Networking', 'replace',
                                        replace_delegate,
                                        person="someone", account="old")
        assert obj2.pay_for_order(pay_by='credit_card', card='mc',
                                  data=cred_card_mastercard)
        assert obj3.upgrade_booked_pass('Networking', 'pass_type',
                                        companion_details,
                                        "Complete Gold")
        res = obj2.pay_for_order(pay_by='credit_card', card='mc',
                                 data=cred_card_mastercard)
        self.assert_and_log('pass2', res)

    @Expects(['pass2'])
    def test_r3_buy_companion_replacement(self):
        someother_delegate['email'] = \
            str(strgen.StringGenerator("[\d\w]{6}").render()) + \
            someother_delegate['email']
        obj1 = PassPage(self.driver)
        obj2 = OrderPage(self.driver)
        obj3 = PassManagerPage(self.driver)

        # named registration >> companion >> replacement
        (result, total) = obj1.add_single_pass(pass_type='complete')
        assert result
        assert obj2.process_order("someone", total, account="old",
                                  data=someother_delegate)
        assert obj2.add_invoice(company_data)
        assert obj2.pay_for_order(pay_by='bank_transfer', festival='cl')
        assert obj3.upgrade_booked_pass('Complete', 'companion',
                                        companion_details,
                                        person="someone", account="old")
        assert obj2.pay_for_order(pay_by='credit_card', card='mc',
                                  data=cred_card_mastercard)
        assert obj3.upgrade_booked_pass('Complete + Companion', 'replace',
                                        replace_delegate, person="someone",
                                        account="old", replace_comp=True)
        res = obj2.pay_for_order(pay_by='credit_card', card='mc',
                                 data=cred_card_mastercard)
        self.assert_and_log('pass3', res)

    @Expects(['pass3'])
    def test_r4_buy_pass_companion_upgrade(self):
        someother_delegate['email'] = \
            str(strgen.StringGenerator("[\d\w]{6}").render()) + \
            someother_delegate['email']
        obj1 = PassPage(self.driver)
        obj2 = OrderPage(self.driver)
        obj3 = PassManagerPage(self.driver)

        # named registration >> companion >> upgrade
        (result, total) = obj1.add_single_pass(pass_type='complete')
        assert result
        assert obj2.process_order("someone", total, account="old",
                                  data=someother_delegate)
        assert obj2.add_invoice(company_data)
        assert obj2.pay_for_order(pay_by='bank_transfer', festival='cl')
        assert obj3.upgrade_booked_pass('Complete', 'companion',
                                        companion_details,
                                        person="someone", account="old")
        assert obj2.pay_for_order(pay_by='credit_card', card='mc',
                                  data=cred_card_mastercard)
        assert obj3.upgrade_booked_pass('Complete + Companion',
                                        'pass_type', companion_details,
                                        "Complete Gold + Companion")
        res = obj2.pay_for_order(pay_by='credit_card', card='mc',
                                 data=cred_card_mastercard)
        self.assert_and_log('pass4', res)

    @Expects(['pass4'])
    def test_r5_buy_pass_upgrade_companion(self):
        someother_delegate['email'] = \
            str(strgen.StringGenerator("[\d\w]{6}").render()) + \
            someother_delegate['email']
        obj1 = PassPage(self.driver)
        obj2 = OrderPage(self.driver)
        obj3 = PassManagerPage(self.driver)

        # named registration >> upgrade >> companion
        (result, total) = obj1.add_single_pass(pass_type='networking')
        assert result
        assert obj2.process_order("someone", total, account="old",
                                  data=someother_delegate)
        assert obj2.add_invoice(company_data)
        assert obj2.pay_for_order(pay_by='bank_transfer', festival='cl')
        assert obj3.upgrade_booked_pass('Networking',
                                        'pass_type', companion_details,
                                        "Complete Gold")
        assert obj2.pay_for_order(pay_by='credit_card', card='mc',
                                  data=cred_card_mastercard)
        assert obj3.upgrade_booked_pass('Complete Gold',
                                        'companion',
                                        companion_details,
                                        person="someone", account="old")
        res = obj2.pay_for_order(pay_by='credit_card', card='mc',
                                 data=cred_card_mastercard)
        self.assert_and_log('pass5', res)

    @Expects(['pass5'])
    def test_r6_buy_pass_and_comp_upgrade_replacement_upgrade(self):
        someother_delegate['email'] = \
            str(strgen.StringGenerator("[\d\w]{6}").render()) + \
            someother_delegate['email']
        obj1 = PassPage(self.driver)
        obj2 = OrderPage(self.driver)
        obj3 = PassManagerPage(self.driver)

        # named registration + companion >> upgrade >> replacement >> upgrade
        (result, total) = obj1.add_single_pass(pass_type='complete')
        assert result
        assert obj2.process_order("someone", total, account="old",
                                  data=someother_delegate, add_comp=True)
        assert obj2.add_invoice(company_data)
        assert obj2.pay_for_order(pay_by='bank_transfer', festival='cl')
        assert obj3.upgrade_booked_pass('Complete + Companion',
                                        'pass_type', companion_details,
                                        "Complete Gold + Companion")
        assert obj2.pay_for_order(pay_by='credit_card', card='mc',
                                  data=cred_card_mastercard)
        assert obj3.upgrade_booked_pass('Complete Gold + Companion', 'replace',
                                        replace_delegate, person="someone",
                                        account="old", replace_comp=True)
        res = obj2.pay_for_order(pay_by='credit_card', card='mc',
                                 data=cred_card_mastercard)
        # Complete Platinum is removed
        # assert obj3.upgrade_booked_pass(
        #     pass_type='Complete Gold + Companion', upgrade='pass_type',
        #     data=companion_details,
        #  upgrade_pass="Complete Platinum + Companion", person='provide_more')
        # res = obj2.pay_for_order(pay_by='bank_transfer, festival='cl'')
        self.assert_and_log('pass6', res)

    @Expects(['pass6'])
    def test_r7_buy_pass_and_comp_replacement_replacement_upgrade(self):
        someother_delegate['email'] = \
            str(strgen.StringGenerator("[\d\w]{6}").render()) + \
            someother_delegate['email']
        obj1 = PassPage(self.driver)
        obj2 = OrderPage(self.driver)
        obj3 = PassManagerPage(self.driver)
        if platform.system() == 'Linux':
            someother_delegate['path'] = linux_path
            replace_delegate['path'] = linux_path

        # named registration + companion >> replace >> replace >> upgrade
        (result, total) = obj1.add_single_pass(pass_type='complete_young_lion')
        assert result
        assert obj2.process_order(person="someone", total=total, account="old",
                                  data=someother_delegate, add_comp=True,
                                  pass_type='complete_young_lion')
        assert obj2.add_invoice(company_data)
        assert obj2.pay_for_order(pay_by='credit_card', card='mc',
                                  data=cred_card_mastercard)

        assert obj3.upgrade_booked_pass(
            pass_type='Complete Young Lion + Companion', upgrade='replace',
            data=replace_delegate, person="someone", account="old",
            replace_comp=True)
        assert obj2.pay_for_order(pay_by='credit_card', card='mc',
                                  data=cred_card_mastercard)

        assert obj3.upgrade_booked_pass(
            pass_type='Complete Young Lion + Companion', upgrade='replace',
            data=replace_delegate, person="someone", account="old",
            replace_comp=True)
        assert obj2.pay_for_order(pay_by='credit_card', card='mc',
                                  data=cred_card_mastercard)

        assert obj3.upgrade_booked_pass(
            pass_type='Complete Young Lion + Companion', upgrade='pass_type',
            data=companion_details, upgrade_pass="Complete Gold + Companion")
        res = obj2.pay_for_order(pay_by='credit_card', card='mc',
                                 data=cred_card_mastercard)
        self.assert_and_log('pass7', res)

    # @Expects(['pass7'])
    # def test_r8_buy_unnamed_pass_replacement_companion_upgrade(self):
    #     obj1 = PassPage(self.driver)
    #     obj2 = OrderPage(self.driver)
    #     obj3 = PassManagerPage(self.driver)
    #
    #     # unnamed registration >> replacement >> companion >> upgrade
    #     (result, total) = obj1.add_single_pass(pass_type='complete')
    #     assert result
    #     assert obj2.process_order(person="unnamed", total=total, account="old",
    #                               data=someother_delegate,)
    #     assert obj2.add_invoice(company_data)
    #     assert obj2.pay_for_order(pay_by='bank_transfer', festival='cl')
    #
    #     obj2.process_delegate_details(
    #         person="provide_more", account="old", data=someother_delegate,
    #         fill=True, pass_type='Complete')
    #
    #     assert obj3.upgrade_booked_pass(
    #         pass_type='Complete', upgrade='replace',
    #         data=replace_delegate, person="someone", account="old")
    #     assert obj2.pay_for_order(pay_by='bank_transfer', festival='cl')
    #
    #     assert obj3.upgrade_booked_pass(
    #         pass_type='Complete', upgrade='companion', data=companion_details,
    #         person="someone", account="old")
    #     assert obj2.pay_for_order(pay_by='bank_transfer', festival='cl')
    #
    #     assert obj3.upgrade_booked_pass(
    #         pass_type='Complete + Companion', upgrade='pass_type',
    #         data=companion_details, upgrade_pass="Complete Gold + Companion")
    #     res = obj2.pay_for_order(pay_by='bank_transfer', festival='cl')
    #     self.assert_and_log('pass8', res)
    #
    # @Expects(['pass8'])
    # def test_r9_buy_unnamed_pass_replacement_upgrade(self):
    #     obj1 = PassPage(self.driver)
    #     obj2 = OrderPage(self.driver)
    #     obj3 = PassManagerPage(self.driver)
    #     if platform.system() == 'Linux':
    #         someother_delegate['path'] = linux_path
    #
    #     # unnamed registration >> replacement >> upgrade
    #     (result, total) = obj1.add_single_pass(pass_type='lions_health')
    #     assert result
    #     assert obj2.process_order(person="unnamed", total=total, account="old",
    #                               data=someother_delegate,)
    #     assert obj2.add_invoice(company_data)
    #     assert obj2.pay_for_order(pay_by='bank_transfer', festival='cl')
    #
    #     obj2.process_delegate_details(
    #         person="provide_more", account="old", data=someother_delegate,
    #         fill=True, pass_type='Lions Health')
    #
    #     assert obj3.upgrade_booked_pass(
    #         pass_type='Lions Health', upgrade='replace',
    #         data=replace_delegate, person="someone", account="old")
    #     assert obj2.pay_for_order(pay_by='bank_transfer', festival='cl')
    #
    #     assert obj3.upgrade_booked_pass(
    #         pass_type='Lions Health', upgrade='pass_type',
    #         data=companion_details, upgrade_pass="Complete Gold")
    #     res = obj2.pay_for_order(pay_by='bank_transfer', festival='cl')
    #     self.assert_and_log('pass9', res)
    #
    # @Expects(['pass9'])
    # def test_rr10_buy_unnamed_pass_companion_replacement(self):
    #     obj1 = PassPage(self.driver)
    #     obj2 = OrderPage(self.driver)
    #     obj3 = PassManagerPage(self.driver)
    #     if platform.system() == 'Linux':
    #         someother_delegate['id_path'] = linux_path
    #
    #     # unnamed registration >> companion >> replacement
    #     (result, total) = obj1.add_single_pass(pass_type='lions_health')
    #     assert result
    #     assert obj2.process_order(person="unnamed", total=total, account="old",
    #                               data=someother_delegate,)
    #     assert obj2.add_invoice(company_data)
    #     assert obj2.pay_for_order(pay_by='bank_transfer', festival='cl')
    #
    #     obj2.process_delegate_details(
    #         person="provide_more", account="old", data=someother_delegate,
    #         fill=True, pass_type='Lions Health')
    #
    #     assert obj3.upgrade_booked_pass(
    #         pass_type='Lions Health', upgrade='companion',
    #         data=companion_details, person="someone", account="old")
    #     assert obj2.pay_for_order(pay_by='bank_transfer', festival='cl')
    #
    #     assert obj3.upgrade_booked_pass(
    #         pass_type='Lions Health + Companion', upgrade='replace',
    #         data=replace_delegate, person="someone", account="old",
    #         replace_comp=True)
    #     res = obj2.pay_for_order(pay_by='bank_transfer', festival='cl')
    #     self.assert_and_log('pass10', res)

from TestDriver import TestDriver
from utils.TestUtils import Expects
from projects.CannesLions.fixtures.TestData import test_data, delegate_data,\
    update_delegate_data
from projects.CannesLions.fixtures.PaymentData import \
    cred_card_mastercard, cred_card_amex
from projects.CannesLions.fixtures.OrderData import company_data, \
    someother_delegate
from projects.CannesLions.pages.LandingPage import LandingPage
from projects.CannesLions.pages.AccountPage import AccountPage
from projects.CannesLions.pages.MailTrap import MailTrapPage
from projects.CannesLions.pages.OrderPage import OrderPage
from projects.CannesLions.pages.PassPage import PassPage

uat_mailbox = 'ascentialevents-sitecore-uat'
staging_mailbox = 'ascentialevents-sitecore-staging'
integration_mailbox = 'ascentialevents-sitecore-dev'
mailbox = uat_mailbox if test_data['env'] == 'uat' else integration_mailbox \
    if test_data['env'] == 'integration' else staging_mailbox
credentials = {
    "url": "https://mailtrap.io/signin",
    "email": "hema.sudheer@canneslions.com",
    "password": "Junnu@13"

}


class BrochureSanityTests(TestDriver):

    def test_brt1_create_account_login(self):
        obj = LandingPage(self.driver)
        assert obj.load_landing_page(test_data)
        obj = AccountPage(self.driver)
        delegate_data['email'] = "Brochure_Smoke_" + delegate_data['email']
        res = obj.create_account(delegate_data)
        self.assert_and_log('new_account', res)

    @Expects(['new_account'])
    def test_brt2_logout(self):
        obj = AccountPage(self.driver)
        assert obj.logout(test_data)

    @Expects(['new_account'])
    def test_brt3_forgot_password_and_generate_new(self):
        data = {
            'email': delegate_data['email'],
            'new_password': 'Automation1234'
        }
        obj = AccountPage(self.driver)
        assert obj.forgot_password(delegate_data)

        obj1 = MailTrapPage(self.driver)
        assert obj1.do_login(credentials)
        url = obj1.select_mailbox_and_email(mailbox, delegate_data['email'])

        res = obj.create_password_and_login(url, data)
        self.assert_and_log('new_password', res)

    @Expects(['new_password'])
    def test_brt4_check_my_account_sections(self):
        obj = AccountPage(self.driver)
        update_delegate_data['password'] = 'Automation1234'
        assert obj.update_account_details('profile', update_delegate_data)

    @Expects(['new_account'])
    def test_brt5_update_marketing_preferences(self):
        obj = AccountPage(self.driver)
        update_delegate_data['marketing'] = {
            'cl': 'true', 'eb': 'false', 'dl': 'true', 'sa': 'false',
            'ae': 'true'
        }
        assert obj.update_account_details('marketing', update_delegate_data)

    @Expects(['new_account'])
    def test_brt6_update_password(self):
        obj = AccountPage(self.driver)
        assert obj.update_account_details('password', update_delegate_data)

from TestDriver import TestDriver
from projects.CannesLions.fixtures.TestData import test_data, delegate_data,\
    update_delegate_data
from projects.CannesLions.pages.LandingPage import LandingPage
from projects.CannesLions.pages.AccountPage import AccountPage
from projects.CannesLions.pages.PassPage import PassPage
from projects.CannesLions.pages.OrderPage import OrderPage
from projects.CannesLions.fixtures.PaymentData import cred_card_visa, \
    cred_card_mastercard
from projects.CannesLions.fixtures.OrderData import company_data, \
    someother_delegate
from utils.TestUtils import Expects


class AccountTests(TestDriver):

    def test_a1_add_account(self):
        obj = LandingPage(self.driver)
        assert obj.load_landing_page(test_data)
        obj = AccountPage(self.driver)
        delegate_data['email'] = "account_tests_" + delegate_data['email']
        res = obj.create_account(delegate_data)
        self.assert_and_log('new_account', res)

    @Expects(['new_account'])
    def test_a2_update_account_details(self):
        obj = AccountPage(self.driver)
        assert obj.update_account_details('profile', update_delegate_data)

    @Expects(['new_account'])
    def test_a3_update_marketing_preferences(self):
        obj = AccountPage(self.driver)
        update_delegate_data['marketing'] = {
            'cl': 'true', 'eb': 'false', 'dl': 'true', 'sa': 'false',
            'ae': 'true'
        }
        assert obj.update_account_details('marketing', update_delegate_data)

    @Expects(['new_account'])
    def test_a4_update_password(self):
        obj = AccountPage(self.driver)
        assert obj.update_account_details('password', update_delegate_data)

    @Expects(['new_account'])
    def test_a5_buy_networking_pass_with_for_myself_with_bank_transfer(self):
        obj = PassPage(self.driver)
        (result, total) = obj.add_single_pass(pass_type='networking')
        assert result
        obj = OrderPage(self.driver)
        assert obj.process_order("myself", total)
        assert obj.add_invoice(company_data)
        res = obj.pay_for_order(pay_by='bank_transfer', festival='cl')
        self.assert_and_log('new_pass', res)

    @Expects(['new_pass'])
    def test_a6_check_count_increase(self):
        obj = AccountPage(self.driver)
        assert obj.check_pass_count(pending_count=0, ready_count=1)

    @Expects(['new_pass'])
    def test_a7_check_registration_invoices(self):
        obj = AccountPage(self.driver)
        assert obj.check_registration_invoices(1)

    @Expects(['new_account'])
    def test_a8_buy_complete_pass_for_some_one_with_visa_credit_card(self):
        obj = PassPage(self.driver)
        (result, total) = obj.add_single_pass(pass_type='complete')
        assert result
        obj1 = OrderPage(self.driver)
        assert obj1.process_order("someone", total, account="old",
                                  data=someother_delegate)
        assert obj1.add_invoice(company_data)
        res = obj1.pay_for_order('credit_card', card='mc',
                                 data=cred_card_mastercard)
        self.assert_and_log('new_pass_2', res)

    @Expects(['new_pass_2'])
    def test_a9_check_count_increase(self):
        obj = AccountPage(self.driver)
        assert obj.check_pass_count(pending_count=0, ready_count=2)

    @Expects(['new_pass_2'])
    def test_aa10_check_registration_invoices(self):
        obj = AccountPage(self.driver)
        assert obj.check_registration_invoices(2)

    @Expects(['new_pass_2'])
    def test_aa11_logout(self):
        obj = AccountPage(self.driver)
        assert obj.logout(test_data)

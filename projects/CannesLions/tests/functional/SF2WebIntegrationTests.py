from TestDriver import TestDriver
from projects.CannesLions.fixtures_salesforce.TestData import credentials, \
    published_basket_data, cannes_basket_data
from projects.CannesLions.salesforce_pages.LoginPage import SFLoginPage
from projects.CannesLions.salesforce_pages.CRMPage import CRMPage
from projects.CannesLions.fixtures.TestData import test_data, delegate_data
from projects.CannesLions.fixtures.OrderData import company_data
from projects.CannesLions.pages.LandingPage import LandingPage
from projects.CannesLions.pages.AccountPage import AccountPage
from projects.CannesLions.pages.OrderPage import OrderPage
from projects.CannesLions.pages.PassPage import PassPage
from utils.TestUtils import Expects

delegate_data['email'] = "sf_tests_" + delegate_data['email']

cannes_credentials = {
    'url': 'https://cannes-uat.lionsfestivals.com/',
    'username': delegate_data['email'],
    'password': 'Automation123'
}


class SF2WebTests(TestDriver):

    def test_sf_001_create_account_in_web(self):
        obj = LandingPage(self.driver)
        assert obj.load_landing_page(test_data)
        obj = AccountPage(self.driver)

        res = obj.create_account(delegate_data)
        self.assert_and_log('new_account', res)

    @Expects(['new_account'])
    def test_sf_002_buy_complete_pass(self):
        obj = PassPage(self.driver)
        (result, total) = obj.add_single_pass(pass_type='networking')
        assert result
        obj = OrderPage(self.driver)
        assert obj.process_order("myself", total)
        assert obj.add_invoice(company_data)
        res = obj.pay_for_order(pay_by='bank_transfer', festival='cl')
        self.assert_and_log('new_pass', res)

    @Expects(['new_pass'])
    def test_sf_003_log_into_sf(self):
        published_basket_data['contact'] = delegate_data['email']
        obj = SFLoginPage(self.driver)
        res = obj.log_into_salesforce(credentials)
        self.assert_and_log('sf_login', res)

    @Expects(['sf_login'])
    def test_sf_004_assign_contact_to_published_pass(self):
        obj = CRMPage(self.driver)
        res = obj.publish_to_purchased_pass(published_basket_data)
        self.assert_and_log('contact', res)

    @Expects(['contact'])
    def test_sf_005_check_pass(self):
        obj = AccountPage(self.driver)
        assert obj.check_for_pass(cannes_basket_data)

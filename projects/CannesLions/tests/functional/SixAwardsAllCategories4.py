from TestDriver import TestDriver
from projects.CannesLions.fixtures.TestData import entries_data
from projects.CannesLions.pages.AccountPage import AccountPage
from projects.CannesLions.fixtures.OrderData import entries_company_data, \
    senior_officer_data, billing_address_data
from utils.TestUtils import Expects
from projects.DubaiLynx.pages.CampaignPage import CampaignPage
from projects.CannesLions.pages.OrderPage import OrderPage
from projects.DubaiLynx.pages.OrderPage import OrderPage as DLOrderPage
from projects.CannesLions.fixtures.PaymentData import awards_cc_visa
from projects.DubaiLynx.fixtures.CampaignData import campaign_data,\
    categories_data, linux_file_paths
from projects.CannesLions.fixtures.AwardsCategoriesData import \
    six_awards_4
import platform


class CLAllAwards4(TestDriver):

    def test_cl_aw3_1_create_cl_account_and_update_details(self):
        obj = AccountPage(self.driver)
        entries_data['email'] = "All_Awards_4_" + entries_data['email']
        res = obj.create_account(entries_data, site='cl_awards')
        self.assert_and_log('login', res)

    @Expects(['login'])
    def test_cl_aw3_2_create_campaign_with_six_entries(self):
        if platform.system() == 'Linux':
            categories_data['media_upload'] = linux_file_paths
        obj = CampaignPage(self.driver)
        (campaign, res) = obj.create_campaign(campaign_data, brand='cl')
        assert res
        assert obj.create_entry(campaign, six_awards_4['award'],
                                six_awards_4['category'],
                                entries=six_awards_4['entries'], brand='cl')
        res = obj.fill_campaign_details(categories_data, campaign,
                                        six_awards_4['entries'],
                                        six_awards_4['award'], brand='cl')
        self.assert_and_log('single_fill', res)

    @Expects(['single_fill'])
    def test_cl_aw3_3_process_order(self):
        obj = OrderPage(self.driver)
        obj.go_to_payment_page()
        assert obj.add_company_details('invoice', entries_company_data)
        assert obj.add_senior_officer(senior_officer_data)
        assert obj.add_company_details('billing', billing_address_data)

        obj1 = DLOrderPage(self.driver)
        entries = obj1.process_entries(six_awards_4['entries'], brand='cl')
        obj1.process_totals(entries, brand='cl')
        assert obj.process_entries_payment('credit_card', awards_cc_visa)
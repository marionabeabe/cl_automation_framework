from TestDriver import TestDriver
from projects.CannesLions.fixtures.TestData import test_data
from projects.CannesLions.pages.LandingPage import LandingPage
from projects.CannesLions.pages.AccountPage import AccountPage
from utils.TestUtils import Expects


class SmokeTests(TestDriver):

    def test_s1_check_landing_page(self):
        obj = LandingPage(self.driver)
        res = obj.load_landing_page(test_data)
        self.assert_and_log('res_1', res)

    @Expects(['res_1'])
    def test_s2_check_account_login_page(self):
        obj = AccountPage(self.driver)
        res = obj.check_account_page(test_data, 'login')
        self.assert_and_log('res_2', res)

    @Expects(['res_2'])
    def test_s3_check_account_create_page(self):
        obj = AccountPage(self.driver)
        res = obj.check_account_page(test_data, 'create')
        self.assert_and_log('res_3', res)

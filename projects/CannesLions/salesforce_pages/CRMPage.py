from BasePage import BasePage
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import datetime


loc = {
    "all_tabs": "xpath=//li[@id='AllTab_Tab']",
    "genetic_tab": "xpath=//li[text()='{}']",
    "generic_tab_heading": "xpath=//h1[text()='{}']",
    "oppor_view": "xpath=//select[@id='fcf']",
    "opportunity":"xpath=//span[text()='{}']",
    "all_tabs_heading": "xpath=//h1[text()='All Tabs']",
    "basket_reg_button": "xpath=//img[@title='Basket Registration']",
    "basket_reg_heading": "xpath=//input[contains("
                          "@name,'publishToPendingBasketButton')]",
    "publish_to_pending": "xpath=//input["
                          "contains(@name,'publishToPendingBasketButton')]",
    "publish_to_purchased": "xpath=//input["
                            "contains(@name,'publishToPurchasedPassButton')]",
    "refresh_button": "xpath=//input[contains(text(), 'RefreshButton')]",
    "search_button": "xpath=//img[@class='lookupIcon']",
    "search_frame": "xpath=//frame[@id='searchFrame']",
    "search_frame_input": "xpath=//input[@id='lksrch']",
    "search_frame_go": "xpath=//input[@name='go']",
    "search_frame_all_fields": "xpath=//input[@value='SEARCH_ALL']",
    "payment_method":
        "xpath=//label[contains(text(),'PAYMENT METHOD')]/..//select",
    "allotment": "xpath=//input[contains(@name,'allot')]",
    "registration_type": "xpath=//label[text()='REGISTRATION TYPE']/..//select",
    "group_settings": "xpath=//label[text()='GROUP SETTINGS']/..//select",
    "select_pass": "xpath=//select[contains(@name, 'dynamicPassTable')]",
    "results_frame": "//frame[@id='resultsFrame']",
    "results_name": "xpath=//a[@href='#']",
    "results_email": "xpath=//td[text()='{}']",
    "save_and_close": "xpath=//input[@value='Save & Close']",
    "loader_gif": "",
    "publish": "xpath=//input[@value='Publish']",
    "publish_success": "xpath=//div[contains(text(),'Successfully published"
                       " the opportunity')]",
    "global_search": "xpath=//input[@id='phSearchInput']",
    "global_search_button": "xpath=//input[@id='phSearchButton']",
    "global_search_again_button": "xpath=//input[@id='secondSearchButton']",
    "no_matches": "xpath=//div[text()='No matches found']",
    "opportunity_row": "xpath=//a[text()='{}']",
    "opportunity_heading": "xpath=//h2[contains(text(),'{}')]",
    "opportunity_amount":
        "xpath=//td[contains(text(),'Opportunity Amount')]/..//div["
        "contains(@id,'opp7_ileinner')]",
    "total_tax_amount":
        "xpath=//span[contains(text(),'Total Tax Amount')]/../..//div["
        "contains(@id,'vnd6')]",
    "total_amount_due":
        "xpath=//td[contains(text(),'Total Amount Due')]/../..//div[contains("
        "@id,'00N0O00000DOCQ1_ileinner')]",
    "opportunity_generic_fields":
        "xpath=//span[text()='{}']/../..//div[@xpath='2']",
    "opportunity_order_id":
        "xpath=//span[text()='{}']/../..//div[@id='00N0O000003wBea_ileinner']",
    "billing_details_fields": "xpath=//td[text()='{}']/..//a",
    "billing_contact": "xpath=//span[text()='Billing Contact']/../..//div["
                       "@id='CF00N2000000ACqMd_ileinner']",
}


class CRMPage(BasePage):

    def go_to_basketreg(self):
        self.press(loc["all_tabs"])
        self.wait_until_appear(loc["all_tabs_heading"])
        assert self.is_element_visible(loc["all_tabs_heading"])
        self.press(loc["basket_reg_button"])
        self.wait_until_appear(loc["basket_reg_heading"])
        assert self.is_element_visible(loc["basket_reg_heading"])

    def publish_to_purchased_pass(self, data):
        self.go_to_basketreg()
        self.press(loc["publish_to_purchased"])
        self.wait_until_disappear(loc["publish_to_pending"])
        assert not self.is_element_visible(loc["publish_to_pending"])
        self.assign_contact(data)
        return True

    def publish_to_pending_basket(self):
        self.go_to_basketreg()
        self.press(loc["publish_to_pending"])
        self.wait_until_disappear(loc["publish_to_purchased"])
        assert not self.is_element_visible(loc["publish_to_purchased"])
        return True

    def assign_contact(self, data):
        parent = self.driver.current_window_handle
        self.press(loc["search_button"])
        WebDriverWait(self.driver, 10).until(
            EC.number_of_windows_to_be(2)
        )
        child = self.driver.window_handles[1]
        # First switch to window
        self.driver.switch_to.window(child)
        self.wait_in_seconds(2)
        # Secondly then switch to the frame
        self.switch_to_iframe("//frame[@id='searchFrame']")

        self.type(loc["search_frame_input"], data['contact'])
        self.wait_in_seconds(2)
        self.press(loc["search_frame_all_fields"])
        self.wait_in_seconds(1)
        self.press(loc["search_frame_go"])
        self.wait_in_seconds(2)
        self.switch_to_default_content()
        self.switch_to_iframe(loc["results_frame"])
        old_time = int((datetime.datetime.now()).strftime("%M"))
        while self.is_element_visible(
                loc["results_name"], screenshot=False) is False:
            print "Contact not found in Salesforce.Trying in 10 seconds"
            self.wait_in_seconds(10)
            self.switch_to_default_content()
            self.wait_in_seconds(2)
            self.switch_to_iframe("//frame[@id='searchFrame']")
            self.wait_in_seconds(2)
            self.press(loc["search_frame_go"])
            self.wait_in_seconds(2)
            self.switch_to_default_content()
            self.wait_in_seconds(2)
            self.switch_to_iframe(loc["results_frame"])
            self.wait_in_seconds(2)
            if self.is_element_visible(loc["results_name"], screenshot=False):
                break
        new_time = int((datetime.datetime.now()).strftime("%M"))
        print "Total time from portal to sf: " + \
              str(new_time - old_time) + " mins"
        print "Contact Found"
        self.press(loc["results_name"])
        self.wait_in_seconds(5)
        self.driver.switch_to.window(parent)
        self.wait_until_appear(loc["save_and_close"])
        self.select(loc["payment_method"], data['payment_method'])
        self.select(loc["select_pass"], data['pass_type'])
        self.press(loc["save_and_close"])
        self.wait_until_disappear(loc["save_and_close"])
        assert self.is_element_visible(loc["publish"])
        self.press(loc["publish"])
        self.wait_until_appear(loc["publish_success"])
        assert self.is_element_visible(loc["publish_success"])
        self.wait_in_seconds(5)

    def switch_to_window_and_frame(self):
        parent = self.driver.current_window_handle
        self.press(loc["search_button"])
        WebDriverWait(self.driver, 10).until(
            EC.number_of_windows_to_be(2)
        )
        child = self.driver.window_handles[1]
        # First switch to window
        self.driver.switch_to.window(child)
        self.wait_in_seconds(2)
        # Secondly then switch to the frame
        self.switch_to_iframe("//frame[@id='searchFrame']")

    def select_tab(self, tab, view=None):
        self.press(loc["genetic_tab"].format(tab))
        self.wait_until_appear(loc["generic_tab_heading"].format(tab))
        assert self.is_element_visible(loc["generic_tab_heading"].format(tab))
        if view is not None:
            self.select(loc["oppor_view"], view)
            self.press(loc["search_frame_go"])
            self.wait_in_seconds(5)

    def select_opportunity(self, order):
        self.press()

    def search_and_select(self, search_text):
        self.type(loc["global_search"], search_text)
        self.press(loc["global_search_button"])
        self.wait_until_appear(loc["generic_tab_heading"].format(
            "Search Results"))
        assert self.is_element_visible(loc["generic_tab_heading"].format(
            "Search Results"))
        opportunity_name = "Awesome Company - REFCL2019 Delegate_" + search_text
        try:
            while self.is_element_visible(
                    loc["no_matches"], screenshot=False) is True:
                print "Order not Synced in Salesforce.Will try in 15 seconds."
                self.wait_in_seconds(15)
                self.press(loc["global_search_again_button"])
                if self.is_element_visible(loc["opportunity_row"].format(
                        opportunity_name), screenshot=False):
                    break
                self.reload()
                continue
        except None:
            pass

        self.press(loc["opportunity_row"].format(opportunity_name))
        self.wait_until_appear(
            loc["opportunity_heading"].format(opportunity_name))
        assert self.is_element_visible(
            loc["opportunity_heading"].format(opportunity_name))
        return True

    def check_prices_in_opportunity(self, order_id, prices):

        try:
            while len(str(self.find_by_locator(
                    loc["opportunity_amount"]).text)) == 1:
                print "Price not Synced yet.Will check in 15 seconds."
                self.wait_in_seconds(15)
                self.reload()
        except None:
            pass

        total_tax_amount = str(self.find_by_locator(
            loc["total_tax_amount"]).text).split('EUR ')[1]
        total_tax_amount = float(total_tax_amount.replace(',', ''))
        opportunity_price = str(self.find_by_locator(
            loc["opportunity_amount"]).text).split('EUR ')[1]
        opportunity_price = float(opportunity_price.replace(',', ''))

        total_amount_due = str(self.find_by_locator(
            loc["total_amount_due"]).text).split('EUR ')[1]
        total_amount_due = float(total_amount_due.replace(',', ''))

        web_order_id = str(self.find_by_locator(
            loc["opportunity_order_id"].format("Web Order Id")).text)
        login_contact = str(self.find_by_locator(
            loc["billing_details_fields"].format('Login Contact')).text)
        booking_contact = str(self.find_by_locator(
            loc["billing_details_fields"].format('Booking Contact')).text)
        billing_contact = str(self.find_by_locator(
            loc["billing_contact"]).text)

        assert order_id == web_order_id
        assert total_tax_amount == float(prices['tax'])
        assert opportunity_price == float(prices['price_before_vat'])
        assert total_amount_due == float(prices['total_price'])
        return True





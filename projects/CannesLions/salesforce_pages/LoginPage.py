from BasePage import BasePage

loc = {
    "sf_logo": "xpath=//img[@id='logo']",
    "sf_username": "xpath=//input[@id='username']",
    "sf_password": "xpath=//input[@id='password']",
    "sf_login": "xpath=//input[@id='Login']",
    "cl_logo": "xpath=//img[contains(@src, 'https://lions')]"
}


class SFLoginPage(BasePage):

    def log_into_salesforce(self, data):
        self.get_web_page(data['url'])
        self.wait_until_appear(loc["sf_logo"])
        self.type(loc["sf_username"], data['username'])
        self.type(loc["sf_password"], data['password'])
        self.press(loc["sf_login"])
        self.wait_until_appear(loc["cl_logo"])
        assert self.is_element_visible(loc["cl_logo"])
        return True

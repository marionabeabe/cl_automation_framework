# -*- coding: utf-8 -*-
import datetime
import strgen

from BasePage import BasePage
from selenium.common.exceptions import NoSuchElementException
from projects.CannesLions.pages.PassPage import pass_prices, passes_mapping, \
    companion_prices


loc = {
    "pending_order": "xpath=//h2[contains(text(),'Pending Order')]",
    "add_new_pass": "xpath=//span[text()='Add new pass']",
    "myself": "xpath=//button[text()='Myself']",
    "someone": "xpath=//button[text()='Someone Else']",
    "new_delegate": "xpath=//button[text()='New Delegate']",
    "unnamed": "xpath=//button[text()='Unnamed Delegate']",
    "unnamed_checkbox_ok": "xpath=//label[@id='checkbox-nxftcpggsb']",
    "confirm": "xpath=//button[text()='Confirm']",
    "provide_more":
        "xpath=//h4[text()='{}']/../../../..//button["
        "text()='Provide more details']",
    "ready_to_checkout": "xpath=//label[text()='Ready to checkout']",
    "checkout_button": "xpath=//button[text()='Checkout']",
    "billing_info": "xpath=//h2[text()='Billing Information']",
    "payment_method": "xpath=//h2[text()='Payment Method']",
    "choose_payment_heading": "xpath=//h3[text()='Choose Payment Method']",
    "credit_card": "xpath=//div[text()='Credit Card']",
    "bank_transfer": "xpath=//div[text()='Bank Transfer']",

    "add_invoice": "xpath=//button[text()='Add Invoice Company']",
    "invoice_company_heading": "xpath=//h3[text()='Invoice Company']",
    "select_company": "xpath=//select[@id='avail-contact']",
    "company_name": "xpath=//input[@id='root_companyName']",
    "address_1": "xpath=//input[@id='root_address1']",
    "address_2": "xpath=//input[@id='root_address2']",
    "city": "xpath=//input[@id='root_city']",
    "tax_identification_number":
        "xpath=//input[@label='Tax Identification Number']",
    "postcode": "xpath=//input[@id='root_postcode']",
    "country": "xpath=//select[@id='root_country']",
    "country2": "xpath=//select[contains(@id,'_country')]",
    "save_and_close": "xpath=//button[text()='Save and close']",
    "save_and_continue": "xpath=//button[text()='Save and continue']",
    "place_order": "xpath=//button[text()='Place Order']",

    "name_on_card": "xpath=//input[@id='root_name']",
    "credit_card_number": "xpath=//input[@id='card-number']",
    "expiry_date": "xpath=//input[@id='card-expiration']",
    "expiry_month": "xpath=//input[@id='root_cardExpirationMonth']",
    "expiry_year": "xpath=//input[@id='root_cardExpirationYear']",
    "cvv": "xpath=//input[@id='card-cvv']",
    "pay_securely": "xpath=//button[text()='Pay Securely']",

    "payment_iframe": "//iframe[@id='payment-frame']",
    "payment_pin": "xpath=//input[@name='st_username']",
    "payment_submit": "xpath=//input[@value='Submit']",

    "see_your_passes": "xpath=//button[text()='See your passes']",
    "book_accomodation": "xpath=//button[text()='Book Accommodation']",
    "download_receipt": "xpath=//button[text()='Download Receipt']",
    
    "del_title": "xpath=//select[@id='root_title']",
    "del_first_name": "xpath=//input[@id='root_firstName']",
    "del_last_name": "xpath=//input[@id='root_secondName']",
    "del_job_title": "xpath=//input[@id='root_jobTitle']",
    "del_email": "xpath=//input[@id='root_emailAddress']",
    "del_comp_activity":
        "xpath=//select[@id='root_companyData_companyActivity']",
    "del_comp_type":
        "xpath=//select["
        "@id='root_companyData_activitySubsection_companyTypeMedia']",
    "del_comp_type2": "xpath=//select[contains(@id, 'companyType')]",
    "del_mobile": "xpath=//input[@id='root_mobilePhone']",
    "total_beforeVat": "xpath==//span[contains(@class, 'price-net-amount]",
    "vat": "xpath=//span[@class='summary__row--price-tax-amount']",
    "total_with_vat": "xpath=//span[@class='summary__row--total-amount']",
    "press_heading": "xpath=//h4[text()='Press']",
    "code_of_conduct": "xpath=//span[text()='Code of Conduct']",
    "code_of_conduct_check": "xpath=//span[contains(text(),'Please check that "
                             "you have read and understood the')]",
    "code_of_conduct_terms": "xpath=//a[text()='Terms and Conditions']",
    "code_of_conduct_link": "xpath=//a[text()='Code of Conduct']",
    "media_activity": "xpath=//span[text()='Media Activity']",
    "media_activity_online": "xpath=//span[text()='Online']",
    "media_activity_publication": "xpath=//span[text()='Publication']",
    "media_activity_radio": "xpath=//span[text()='Radio']",
    "media_activity_tv": "xpath=//span[text()='TV']",
    "web_address": "xpath=//input[@label='Website Address']",
    "page_impressions": "xpath=//select[contains(@id, 'reach_4_audience')]",
    "unique_users": "xpath=//select[contains(@id, 'other_4_frequency')]",


    "proof_of_press": "xpath=//span[text()='Proof of Press']",
    "browse_files3": "xpath=//div[text()='Browse files...']",
    "browse_files":
        "xpath=//div[contains(text(), 'Word, PDF or JPG')]/..//input",
    "choose_files":
        "xpath=//div[contains(text(), 'Choose file')]/../..//input",
    "file_upload":
        "xpath=//legend[text()='{}']/..//div["
        "contains(text(), 'Choose file')]/../..//input",
    "file_formats": "xpath=//div[contains(text(), 'Word, PDF, GIF or JPG')]",
    "file_in_ui": "xpath=//div[text()='{}']",
    "application_ok": "xpath=//button[text()='OK']",
    "summary_price": "xpath=//span[contains(@class, 'payment-summary__price')]",
    "dob": "xpath=//input[@id='date-of-birth']",
    "nationality": "xpath=//select[@id='root_nationality']",
    "city_of_birth": "xpath=//input[@id='root_cityOfBirth']",
    "country_of_birth": "xpath=//select[@id='root_countryOfBirth']",
    "contractor_build": "xpath=//label[text()='Build 8 - 16 June']",
    "contractor_festival": "xpath=//label[text()='Festival 17 - 21 June']",
    "university": "xpath=//input[@id='root_educationalEstablishment']",
    "competition": "xpath=//select[@id='root_competition']",
    "add_senior_officer": "xpath=//button[text()='Add Senior Officer']",
    "officer_full_name": "xpath=//input[@name='officer']",
    "officer_position": "xpath=//input[@name='position']",
    "officer_terms": "xpath=//p[contains(text(),'I have read')]",
    "add_billing_company": "xpath=//button[text()='Add Billing Company']",
    "billing_company_name":
        "xpath=//form[@id='{}']//input[@name='company_name']",
    "billing_address_1": "xpath=//form[@id='{}']//input[@name='address1']",
    "billing_address_2": "xpath=//form[@id='{}']//input[@name='address2']",
    "billing_pobox": "xpath=//form[@id='{}']//input[@name='address3']",
    "billing_city": "xpath=//form[@id='{}']//input[@name='city']",
    "billing_postcode": "xpath=//form[@id='{}']//input[@name='postcode']",
    "billing_country": "xpath=//form[@id='{}']//select[@name='countrycode']",
    "cart_button": "xpath=//span[contains(text(), 'My Cart')]",
    "proceed_to_checkout": "xpath=//input[@value='Proceed to checkout']",
    "billing_email": "xpath=//form[@id='{}']//input[@name='email']",
    "officer_save_and_close": "xpath=//form[@id='officerForm']//button["
                              "text()='Save and close']",
    "invoice_save_and_close": "xpath=//form[@id='invoiceForm']//button["
                              "contains(text(),'Save and')]",
    "invoice_cancel": "xpath=//form[@id='invoiceForm']//button[text()='Cancel']",
    "billing_save_and_close": "xpath=//form[@id='billingForm']//button["
                              "contains(text(),'Save and')]",
    "billing_checked": "xpath=//h4[text()='Billing Address']/../..//li["
                       "contains(@class, '--completed')]",
    "invoice_checked": "xpath=//h4[text()='Invoice Information']/../..//li["
                       "contains(@class, '--completed')]",
    "officer_checked": "xpath=//h4[text()='Senior Officer']/../..//li["
                       "contains(@class, '--completed')]",

    "awards_cc_type": "xpath=//label[@for='radio-1']",
    "awards_bank_type": "xpath=//label[@for='radio-2']",
    "awards_name_on_card": "xpath=//input[@name='name_on_card']",
    "awards_cc_number": "xpath=//input[@id='ccnumber']",
    "awards_month": "xpath=//input[@id='month']",
    "awards_year": "xpath=//input[@id='year']",
    "awards_cvv_1": "xpath=//input[@id='securityCode1']",
    "awards_cvv_2": "xpath=//input[@id='securityCode2']",
    "awards_cvv_3": "xpath=//input[@id='securityCode3']",
    "awards_cvv_4": "xpath=//input[@id='securityCode4']",
    "awards_pay_securely": "xpath=//input[@value='Pay Securely']",
    "awards_confirm_payment": "xpath=//input[@value='Confirm Payment']",
    "gateway_heading": "xpath=//th[contains(text(), 'Secure Trading')]",
    "gateway_pin": "xpath=//input[@name='st_username']",
    "gateway_submit": "xpath=//input[@value='Submit']",
    "payment_confirmation": "xpath=//h3[text()='Transaction Summary']",
    "my_dashboard": "xpath=//span[contains(text(), 'My Dashboard')]",
    "new_campaign": "xpath=//span[text()='Create New Campaign']",
    "vat_number": "xpath=//input[@name='vat_number']",
    "change_invoice":
        "xpath=//h4[text()='Invoice Information']/..//a[text()='Edit']",
    "cl_awards_vat":
        "xpath=//span[@id='spTaxAmount']",
    "card_fees": "xpath=//td[@data-name='surchargeAmount']",
    "college_name": "xpath=//input[@id='root_educationalEstablishment']",
    "sa_cc_fee": "xpath=//span[@id='ccfeeAmount']",
    "sa_gst": "xpath=//span[@id='spTaxAmount']",
    "sa_net_total": "xpath=//span[@id='nettotal']",
    "sa_total_with_fee": "xpath=//span[@id='amountTotal']",
    "sales_tax": "xpath=//span[contains(@class,'tax-amount')]",
    "edit_invoice_company":
        "xpath=//a[@data-click='add-invoice-details'][text()='Edit']",
    "edit_booking_contact":
        "xpath=//a[@data-click='add-booking-contact'][text()='Edit']",
    "edit_finance_contact":
        "xpath=//a[@data-click='add-finance-contact'][text()='Edit']",
    "process_multiple_pass":
        "xpath=//h4[text()='{}']/../../../..//button[contains(text(),'New ')]",
    "companion_first_name":
        "xpath=//input[@id='root_companionDetails_companionFirstName']",
    "companion_last_name":
        "xpath=//input[@id='root_companionDetails_companionLastName']",
    "add_companion_toggle": "xpath=//label[@class='[ e-form__label ]']",
    "airport_transfers": "xpath=//span[text()='Airport Transfers']",
    "proof_of_age": "xpath=//legend[text()='Proof Of Age']",
    "purchased_pass_count": "xpath=//div[@id='purchased-passes-total']",
    "pp_tile_view":
        "xpath=//h2[text()='Purchased Passes']/../../../../..//i["
        "@data-click='show-cardView']",
    "contact_heading": "xpath=//h3[text()='{}']",
    "select_contact": "xpath=//select[@id='avail-contact']",
    "finance_booking_contact_data":
        "xpath=//h3[text()='{}']/..//p[contains(@class,'contact')]",
    "cancel_button": "xpath=//button[text()='Cancel']",
    "order_id": "xpath=//div[contains(@class,'success')]/*[1]",
    "social_networking": "xpath=//div[contains(@class, '--{}')]",
    "pay_at_festival_tab": "xpath=//div[text()='Pay At Festival']",
    "pay_at_festival_button":
        "xpath=//button[contains("
        "@class, 'payment-summary__btn')][text()='Pay At Festival']",
    "job_department": "xpath=//select[contains(@id, 'jobDepartment')]",
    "diet_requirements":
        "xpath=//select[@id='root_DietaryRequirementsDropdown']",
    "m2020_company_stage": "xpath=//select[@id='root_companyStage']",
    "m2020_company_sector": "xpath=//select[@id='root_companySector']",
    "m2020_revenue_bracket": "xpath=//select[@id='root_revenueBracket']",
    "m2020_add_billing": "xpath=//button[@id='invoice-details']",
}
eu_countries = ['FRANCE', 'ITALY', 'SPAIN', 'SWITZERLAND']


class OrderPage(BasePage):

    def process_delegate_details(self, person, account=None, data=None,
                                 delegate=None, extra_form=None, multiple=None,
                                 passes=None, replace_comp=None, add_comp=None,
                                 pass_type=None, fill=None, festival=None):
        assert self.is_element_visible(loc["pending_order"])
        if multiple:
            for passe in passes:
                if passe == 'digital':
                    passes_mapping[passe] = 'Digital Pass'
                self.press(loc["process_multiple_pass"].format(
                    passes_mapping[passe]))
                self.wait_until_appear(loc["save_and_close"])
                self.wait_until_appear(loc["del_first_name"])
                self.fill_other_delegate_details(data, delegate=passe)
                self.wait_in_seconds(2)
            return
        if person == 'myself':
            self.wait_until_appear(loc["myself"])
            self.press(loc["myself"])
            # code to handle extra form
            if festival == 'm2020':
                self.wait_until_appear(loc["save_and_close"])
                self.wait_in_seconds(2)
                self.select(loc["del_title"], data['title'])
                self.select(loc["job_department"], data['job_department'])
                self.select(loc["diet_requirements"], data['diet_requirements'])
                self.select(loc["m2020_company_stage"], data['company_stage'])
                self.select(loc["m2020_company_sector"], data['company_sector'])
                self.select(loc["m2020_revenue_bracket"],
                            data['revenue_bracket'])
            if delegate == 'contractor':
                self.wait_in_seconds(2)
                self.screen_shot()
                self.hover_then_click(loc["contractor_build"],
                                      loc["contractor_build"])
                self.press(loc["contractor_build"])
                self.press(loc["contractor_festival"])
                self.type(loc["dob"], data["dob"])
                self.select(loc["nationality"], data['nationality'])
                self.select(loc["country_of_birth"], data['country_of_birth'])
                self.select(loc["country"], data['country'])
                self.type(loc["city_of_birth"], data['city_of_birth'])
                self.wait_in_seconds(2)
            if extra_form:
                return
            self.wait_until_appear(loc["save_and_close"])
            if replace_comp:
                self.type(loc["companion_first_name"], data['first_name'])
                self.type(loc["companion_last_name"], data['last_name'])
            self.wait_in_seconds(3)
            self.press(loc["save_and_close"])
            self.wait_until_appear(loc["checkout_button"])
            assert self.is_element_visible(loc["checkout_button"])
        elif person == 'companion':
            print "Delegate and Companion details already processed"
        elif person == 'someone':
            if account == "old":
                self.wait_until_appear(loc["new_delegate"])
                self.press(loc["new_delegate"])
            elif account == "new":
                self.wait_until_appear(loc["someone"])
                self.press(loc["someone"])
            self.wait_until_appear(loc["del_first_name"])
            self.fill_other_delegate_details(
                data, delegate=delegate, replace_comp=replace_comp,
                add_comp=add_comp, pass_type=pass_type, festival=festival)
        elif person == 'unnamed':
            self.press(loc["unnamed"])
            if festival != 'm2020':
                try:
                    self.press(loc["unnamed_checkbox_ok"])
                    self.press(loc["confirm"])
                    self.wait_in_seconds(5)
                finally:
                    pass
            else:
                self.wait_in_seconds(2)
        elif person == 'provide_more':
            self.press(loc["provide_more"].format(pass_type))
            if fill:
                self.wait_until_appear(loc["save_and_close"])
                self.fill_other_delegate_details(
                    data, delegate=delegate, replace_comp=replace_comp,
                    add_comp=add_comp, pass_type=pass_type)
            else:
                self.wait_until_appear(loc["airport_transfers"])
                self.press(loc["airport_transfers"])
                self.wait_until_appear(loc["save_and_close"])
                self.press(loc["save_and_close"])
        else:
            print "No action need as these are upgrades"
        self.wait_until_appear(loc["pending_order"])
        assert self.is_element_visible(loc["pending_order"])

    def process_tax(self, total, country, billing=None, digital=None,
                    combined=None, invoice=None, non_digital_tax=None):
        digital_pass = pass_prices['digital']

        if billing == 'prebilling' and digital is not None:
            # mainly used for non-digital passes
            tax = total * 0.2
            vat_from_ui = (self.find_by_locator(
                loc["vat"])).text.replace(',', '')
            total_price = float(total) + tax
            total_with_vat_from_ui = (self.find_by_locator(
                loc["total_with_vat"])).text.replace(',', '')
            print "ui_vat:" + str(vat_from_ui), "method_vat:" + str(tax)
            print "ui_total:" + str(total_with_vat_from_ui), \
                "method_total:" + str(total_price)
            assert int(tax) == int(vat_from_ui)
            assert int(total_price) == int(total_with_vat_from_ui)
            return True

        if digital:
            taxed_countries = ['United Kingdom', 'Russian Federation']
            if country in taxed_countries:
                print "Sales Tax applicable for " + country + \
                      " for digital pass"
                tax = digital_pass * 0.2
                vat_from_ui = (self.find_by_locator(
                    loc["vat"])).text.replace(',', '')
                assert float(vat_from_ui) == tax
            else:
                print "Sales Tax not applicable for " + country + \
                      " for digital pass"
                tax = 0
            total_with_vat_from_ui = (self.find_by_locator(
                loc["total_with_vat"])).text.replace(',', '')
            total_with_vat = int(tax) + digital_pass
            assert int(total_with_vat_from_ui) == total_with_vat
            return True

        if combined:
            if invoice == 'before':
                net_total = total
                tax_with_out_digital = (net_total - 195) * 0.2
                grand_total = net_total + tax_with_out_digital
                vat_from_ui = (self.find_by_locator(
                    loc["vat"])).text.replace(',', '')
                total_with_vat_from_ui = (self.find_by_locator(
                    loc["total_with_vat"])).text.replace(',', '')
                assert tax_with_out_digital == int(vat_from_ui)
                assert grand_total == int(total_with_vat_from_ui)
                print "ui_vat:" + str(vat_from_ui), "method_vat:" + str(
                    tax_with_out_digital)
                print "ui_total:" + str(total_with_vat_from_ui), \
                    "method_total:" + str(grand_total)
            elif invoice == 'after':

                digital_tax_countries = ['United Kingdom', 'Russian Federation']
                if country in digital_tax_countries:
                    print "Sales Tax applicable for " + country + \
                          " for digital pass"
                    digital_tax = digital_pass * 0.2
                else:
                    print "Sales Tax not applicable for " + country + \
                          " for digital pass"
                    digital_tax = 0
                passes_with_out_digital = total - 195
                tax_for_non_digital_pass = 0
                if non_digital_tax:
                    tax = digital_tax + tax_for_non_digital_pass
                else:
                    _tax = passes_with_out_digital * 0.2
                    tax = digital_tax + _tax
                pre_tax_total = passes_with_out_digital + digital_pass
                combined_tax = int(tax)
                pass_plus_tax = pre_tax_total + combined_tax
                if not non_digital_tax:
                    vat_from_ui = (self.find_by_locator(
                        loc["vat"])).text.replace(',', '')
                    print "ui_vat:" + str(vat_from_ui), "method_vat:" + str(
                        combined_tax)
                    assert int(combined_tax) == int(vat_from_ui)
                total_with_vat_from_ui = (self.find_by_locator(
                    loc["total_with_vat"])).text.replace(',', '')

                print "ui_total:" + str(total_with_vat_from_ui), \
                    "method_total:" + str(pass_plus_tax)
                assert int(pass_plus_tax) == int(total_with_vat_from_ui)
            return True

    def is_vat_free(self, data):
        if data['country'] in eu_countries and 'vat_number' in data.keys():
            print "Country is in EU and VAT Registered NO VAT"
            return True
        elif data['country'] in eu_countries and \
                'vat_number' not in data.keys():
            print "Country is in EU but not VAT Registered VAT is 20 %"
            return False
        elif data['country'] == 'UNITED KINGDOM' and \
                'vat_number' in data.keys():
            print "Country is UK VAT Registered VAT is 20 %"
            return False
        elif data['country'] == 'UNITED KINGDOM' and \
                'vat_number' not in data.keys():
            print "Country is UK but not VAT Registered VAT is 20 %"
            return False
        elif data['country'] not in eu_countries and \
                data['country'] != 'UNITED KINGDOM':
            print "Country is Rest of World NO VAT"
            return True

    def process_order(self, person, total, account=None, data=None,
                      delegate=None, festival=None, card_fees=None, skip=None,
                      multiple=None, passes=None, replace_comp=None,
                      add_comp=None, pass_type=None, vat_free=None,
                      company=None):
        self.process_delegate_details(person, account, data, delegate=delegate,
                                      multiple=multiple, passes=passes,
                                      replace_comp=replace_comp,
                                      add_comp=add_comp, pass_type=pass_type,
                                      festival=festival)
        if add_comp:
            if pass_type == 'complete_young_lion':
                total += companion_prices['complete_young_lion']
            elif pass_type == 'lions_health':
                total += companion_prices['lions_health']
            elif pass_type == 'lion_health_student':
                total += companion_prices['lions_health_student']
            elif pass_type == 'lions_health_young_lion' or \
                    pass_type == 'complete_student':
                total += companion_prices['complete_student']
            else:
                total += 1285
        self.wait_until_appear(loc["checkout_button"])
        self.press(loc["checkout_button"])
        self.wait_in_seconds(2)
        # '' if person == 'companion' or person == 'noneed' else \
        #     self.wait_until_appear(loc["add_invoice"])
        if festival == 'spikes_asia' and card_fees is not None:
            if card_fees == 'mc':
                fee = 0.0165
            elif card_fees == 'visa':
                fee = 0.0165
            elif card_fees == 'amex':
                fee = 0.0395
            total_card_fee = round((total * fee), 2)
        else:
            total_card_fee = 0
        if skip:
            return True
        vat, total_price = self.check_order_summary(
            person, total, festival=festival, card_fees=total_card_fee,
            pass_type=pass_type, company=company)
        prices = None
        if festival == 'spikes_asia':
            print "Total Price: " + str(total)
            if total_card_fee != 0:
                print "Total Card Fee for " + card_fees + ": " + \
                      str(total_card_fee)
            return True, total_card_fee
        if festival == 'm2020':
            assert self.add_invoice(company, festival=festival)
        if vat_free is None and vat != 0.0:
            vat_from_ui = \
                (self.find_by_locator(loc["vat"])).text.replace(',', '')
            total_with_vat_from_ui = self.find_by_locator(
                loc["total_with_vat"]).text
            total_with_vat_from_ui = total_with_vat_from_ui.replace(',', '')
            assert float(vat) == float(vat_from_ui)
            print "ui_vat:" + str(vat_from_ui), "method_vat:" + str(vat)

            print "ui_total:" + str(total_with_vat_from_ui), \
                "method_total:" + str(total_price)
            assert float(total_price) == float(total_with_vat_from_ui)
            prices = {
                'total_price': float(total_price),
                'tax': float(vat),
                'price_before_vat':  float(total_price) - float(vat)
            }
        elif vat == 0.0:
            total_with_vat_from_ui = self.find_by_locator(
                loc["total_with_vat"]).text
            total_with_vat_from_ui = total_with_vat_from_ui.replace(',', '')
            print "ui_total:" + str(float(total_with_vat_from_ui)), \
                "method_total:" + str(total_price)
            assert float(total_price) == float(total_with_vat_from_ui)
            prices = {
                'total_price': float(total_price),
                'tax': float(vat),
                'price_before_vat': float(total_price) - float(vat)
            }
        return True, prices

    def process_special_passes(self, person, pass_type, account=None,
                               data=None, extra_form=None):
        self.process_delegate_details(person, account, data,
                                      delegate=pass_type, extra_form=extra_form)
        if pass_type == 'press':
            #self.press(loc["provide_more"])
            self.wait_until_appear(loc["press_heading"])
            self.press(loc["code_of_conduct"])
            self.wait_until_appear(loc["save_and_continue"])
            self.press(loc["code_of_conduct_check"])
            assert self.is_element_visible(loc["code_of_conduct_terms"])
            assert self.is_element_visible(loc["code_of_conduct_link"])
            self.press(loc["save_and_continue"])
            self.wait_in_seconds(5)
            self.press(loc["save_and_continue"])
            #self.press(loc["media_activity"])
            #self.wait_until_appear(loc["save_and_continue"])
            self.press(loc["media_activity_online"])
            self.type(loc["web_address"], data['web_address'])
            self.select(loc["page_impressions"], data['page_impressions'])
            self.select(loc["unique_users"], data['unique_users'])
            self.press(loc["save_and_continue"])
            self.wait_in_seconds(5)
            #self.press(loc["proof_of_press"])
            #self.wait_until_appear(loc["save_and_close"])
            self.upload_file(loc["browse_files"], data['id_path'])
            self.wait_in_seconds(5)
            self.wait_until_appear(loc["file_in_ui"].format(data['file_name']))
            self.press(loc["save_and_close"])
            self.wait_until_appear(loc["application_ok"])
            self.press(loc["application_ok"])
            self.wait_until_appear(loc["checkout_button"])
        elif pass_type in ['festival_rep', 'roger_hatchuel', 'contractor']:
            pass
        self.press(loc["checkout_button"])
        price = (self.find_by_locator(loc["summary_price"]
                                          ).text).encode('utf-8').strip()
        formatted_price = int(price.rpartition('0')[1])
        assert formatted_price == 0
        self.press(loc["place_order"])
        self.wait_in_seconds(5)
        self.wait_until_appear(loc["see_your_passes"], timeout=120)
        assert self.is_element_visible(loc["see_your_passes"])
        self.press(loc["see_your_passes"])
        self.wait_until_appear(loc["pending_order"], timeout=60)
        assert self.is_element_visible(loc["pending_order"])
        return True

    def check_order_summary(self, person, total, festival, card_fees,
                            pass_type=None, company=None):
        current_date = datetime.date.today().strftime("%d-%B-%Y")
        current_date = current_date.split('-')
        late_fee_1_months = ['March', 'April', 'May']
        late_fee_2_months = ['June']
        no_late_fee_months = ['October', 'November', 'December', 'January',
                              'February']
        no_unnamed_fee_months = ['May', 'June']
        if festival != 'spikes_asia':
            if current_date[1] in late_fee_1_months and \
                    int(current_date[0]) >= 1:
                print "Late Fees 1 apply if unnamed chosen"
                unnamed_fee = 220
            elif current_date[1] in late_fee_2_months and \
                    int(current_date[0]) >= 1:
                print "Late fee 2 apply if unnamed chosen"
                unnamed_fee = 280
            elif current_date[1] in no_late_fee_months:
                unnamed_fee = 150

        if current_date[0] >= 17 and current_date[1] == 'June' and  \
                current_date[2] == '2019':
            total += 90
        if festival == 'spikes_asia':
            vat_percentage = 0
            unnamed_fee = 50  # 30 for students
            total += card_fees
        elif festival == 'm2020':
            unnamed_fee = 0
            print "No unnamed fee for Money 2020"
            if company['country'] == 'Singapore':
                vat_percentage = 0.07
            else:
                vat_percentage = 0
        else:
            vat_percentage = 0.2
        if person == 'unnamed':
            total = float(total + unnamed_fee)
        if pass_type is not None and 'Platinum' in pass_type:
            total = float(total)
        vat = round(float(total * vat_percentage), 2)
        total_order = total + vat
        return vat, total_order

    def fill_other_delegate_details(self, data, delegate=None,
                                    replace_comp=None, add_comp=None,
                                    pass_type=None, festival=None):
        data['email'] = \
            str(strgen.StringGenerator("[\d\w]{6}").render()) + \
            data['email']
        skip = ['roger_hatchuel', 'student', 'spikes_student']
        self.wait_until_appear(loc["del_title"])
        self.wait_in_seconds(3)
        if delegate == 'young_lion_competitor' or delegate == 'competitor':
            self.select(loc["competition"], data['competition'])
            self.type(loc["dob"], data['dob'])
            self.upload_file(loc["choose_files"], data['path'])
        self.wait_until_appear(loc["del_title"])
        self.select(loc["del_title"], data['title'])
        self.type(loc["del_first_name"], data['first_name'])
        self.type(loc["del_last_name"], data['last_name'])
        self.type(loc["del_job_title"], data['job_title'])
        self.wait_in_seconds(1)
        self.type(loc["del_email"], data['email'])
        if festival == 'm2020':
            self.type(loc["company_name"], data['company'])
            self.select(loc["job_department"], data['job_department'])
            self.select(loc["diet_requirements"], data['diet_requirements'])
            self.select(loc["m2020_company_stage"], data['company_stage'])
            self.select(loc["m2020_company_sector"], data['company_sector'])
            self.select(loc["m2020_revenue_bracket"],
                        data['revenue_bracket'])
        if delegate not in skip and festival != 'm2020':
            self.type(loc["company_name"], data['company'])
            self.select(loc["del_comp_activity"], data['comp_activity'])
            self.wait_in_seconds(2)
            self.select(loc["del_comp_type"], data['company_type'])
        if delegate == 'spikes_student':
            self.type(loc["college_name"], data['college_name'])
            self.upload_file(loc["file_upload"].format('Student Photo Card'),
                             data['path'])
            self.upload_file(loc["file_upload"].format('Proof Of Age'),
                             data['path'])
        self.type(loc["address_1"], data['address1'])
        self.type(loc["address_2"], data['address2'])
        self.type(loc["city"], data['city'])
        self.type(loc["postcode"], data['postcode'])
        self.select(loc["country"], data['country'])
        if delegate != 'digital' or delegate == None:
            self.type(loc["del_mobile"], data['mobile'])
        self.wait_in_seconds(2)
        if add_comp:
            pass_types = ['complete_young_lion']
            self.press(loc["add_companion_toggle"])
            self.wait_until_appear(loc["companion_first_name"])
            self.type(loc["companion_first_name"], data['first_name'])
            self.type(loc["companion_last_name"], data['last_name'])
            if pass_type in pass_types:
                self.type(loc["dob"], data['dob'])
                self.upload_file(loc["choose_files"], data['path'])

        if delegate == 'contractor':
            self.press(loc["contractor_build"])
            self.press(loc["contractor_festival"])
            self.type(loc["dob"], data["dob"])
            self.select(loc["nationality"], data['nationality'])
            self.select(loc["country_of_birth"], data['country_of_birth'])
            self.select(loc["country"], data['country'])
            self.type(loc["city_of_birth"], data['city_of_birth'])
        if delegate == 'roger_hatchuel' or delegate == 'student':
            self.type(loc["university"], data['university'])
            self.type(loc["dob"], data['dob'])
            self.upload_file(loc["choose_files"], data['path'])
        spikes_passes = ['media_academy', 'young_spikes', 'spikes_student',
                         'account_leadership_academy', 'digital_academy',
                         'marketers_academy', 'strategy_effectiveness_academy']
        if replace_comp:
            self.type(loc["companion_first_name"], data['first_name'])
            self.type(loc["companion_last_name"], data['last_name'])
            try:
                if self.is_element_visible(loc["dob"], screenshot=False):
                    self.type(loc["dob"], data['dob'])
            except NoSuchElementException:
                pass
            try:
                if self.is_element_visible(loc["proof_of_age"],
                                           screenshot=False):
                    self.upload_file(loc["choose_files"], data['path'])
            except NoSuchElementException:
                pass

        if delegate in spikes_passes:
            self.type(loc["dob"], data['dob'])
            self.upload_file(loc["choose_files"], data['path'])
        self.wait_in_seconds(3)
        self.press(loc["save_and_close"])
        assert self.is_element_visible(loc["pending_order"])
        return True

    def add_invoice(self, data, festival=None):
        if festival == 'm2020':
            self.press(loc["m2020_add_billing"])
        else:
            self.press(loc["add_invoice"])
            self.wait_until_appear(loc["save_and_close"])
        self.wait_until_appear(loc["select_company"])
        self.wait_in_seconds(1)
        self.select(loc["select_company"], data["add_new_company"])
        self.type(loc["company_name"], data['company_name'])
        self.type(loc["address_1"], data['address_1'])
        self.type(loc["address_2"], data['address_2'])
        self.type(loc["city"], data['city'])
        self.type(loc["postcode"], data['postcode'])
        self.wait_in_seconds(1)
        if festival == 'sa' or festival == 'dl' or festival == 'm2020':
            self.select(loc["country2"], data['country'])
        else:
            self.select(loc["country"], data['country'])
        self.press(loc["save_and_close"])
        self.wait_in_seconds(3)
        return True

    def modify_contact(self, contact, data, modify):
        if contact == 'booking':
            self.press(loc["edit_booking_contact"])
            self.wait_until_appear(loc["cancel_button"])
            assert self.is_element_visible(
                loc["contact_heading"].format('Booking Contact'))
        elif contact == 'finance':
            self.press(loc["edit_finance_contact"])
            self.wait_until_appear(loc["cancel_button"])
            assert self.is_element_visible(
                loc["contact_heading"].format('Finance Contact'))
        self.select(loc["select_contact"], data['contact'])
        self.wait_until_appear(loc["del_first_name"])
        first_name_from_ui = self.find_by_locator(loc["del_first_name"]
                                                  ).get_attribute('value')
        last_name_from_ui = self.find_by_locator(loc["del_last_name"]
                                                 ).get_attribute('value')
        email_from_ui = self.find_by_locator(loc["del_email"]
                                             ).get_attribute('value')
        assert str(first_name_from_ui) == data['first_name']
        assert str(last_name_from_ui) == data['last_name']
        assert str(email_from_ui) == data['email']
        if contact == 'booking' and modify:
            self.select(loc["select_contact"], 'Add new contact')
            self.select(loc["del_title"], data['title'])
            self.type(loc["del_first_name"], data['new_first_name'])
            self.type(loc["del_last_name"], data['new_last_name'])
            self.type(loc["del_job_title"], data['job_title'])
            self.type(loc["del_email"], data['new_email'])
            self.type(loc["company_name"], data['company_name'])
            self.type(loc["address_1"], data['address1'])
            self.type(loc["address_2"], data['address2'])
            self.type(loc["city"], data['city'])
            self.type(loc["postcode"], data['postcode'])
            self.select(loc["country"], data['country'])
            self.select(loc["del_comp_activity"], data['company_activity'])
            self.wait_in_seconds(2)
            self.select(loc["del_comp_type2"], data['company_type'])
        elif contact == 'finance' and modify:
            self.select(loc["select_contact"], 'Add new contact')
            self.type(loc["del_first_name"], data['new_first_name'])
            self.type(loc["del_last_name"], data['new_last_name'])
            self.type(loc["del_email"], data['new_email'])

        self.press(loc["save_and_close"])
        self.wait_in_seconds(3)
        if contact == 'booking':
            data_ui = self.find_by_locator(loc["finance_booking_contact_data"].
                                           format('Booking Contact')).text
            if modify:
                assert data['new_first_name'] in data_ui
                assert data['new_last_name'] in data_ui
                assert data['new_email'] in data_ui
            else:
                assert data['first_name'] in data_ui
                assert data['last_name'] in data_ui
                assert data['email'] in data_ui
        elif contact == 'finance':
            data_ui = self.find_by_locator(loc["finance_booking_contact_data"].
                                           format('Finance Contact')).text
            if modify:
                assert data['new_first_name'] in data_ui
                assert data['new_last_name'] in data_ui
                assert data['new_email'] in data_ui
            else:
                assert data['first_name'] in data_ui
                assert data['last_name'] in data_ui
                assert data['email'] in data_ui
        return True

    def change_invoice_company(self, data, vat_reg=None):
        self.press(loc["edit_invoice_company"])
        self.wait_until_appear(loc["save_and_close"])
        self.wait_until_appear(loc["select_company"])
        self.wait_in_seconds(1)
        self.select(loc["select_company"], data["add_new_company"])
        self.clear_and_type(loc["company_name"], data['company_name'])
        self.clear_and_type(loc["address_1"], data['address_1'])
        self.clear_and_type(loc["address_2"], data['address_2'])
        self.clear_and_type(loc["city"], data['city'])
        self.clear_and_type(loc["postcode"], data['postcode'])
        if vat_reg:
            self.type(loc["tax_identification_number"], data['tax_number'])
            print "Tax Identifaction Number Applied"
        self.wait_in_seconds(1)
        self.select(loc["country"], data['country'])
        self.press(loc["save_and_close"])
        self.wait_in_seconds(3)
        return True

    def pay_for_order(self, pay_by, card_fee=None, card=None, data=None,
                      festival=None):
        if pay_by == 'credit_card':
            #self.press(loc["credit_card"])
            self.type(loc["name_on_card"], data["name_on_card"])
            self.wait_in_seconds(1)
            self.type(loc["credit_card_number"], data["credit_card_number"])
            self.wait_in_seconds(1)
            if card_fee is not None:
                self.wait_until_appear(loc["card_fees"], timeout=20)
                fee_from_ui = float(str(
                    (self.find_by_locator(
                        loc["card_fees"]).text).split('$')[1]))
                assert fee_from_ui == card_fee
                self.type(loc["expiry_month"], data['expiry_month'])
                self.wait_in_seconds(1)
                self.type(loc["expiry_year"], data['expiry_year'])
            elif festival == 'm2020':
                self.type(loc["expiry_month"], data['expiry_month'])
                self.wait_in_seconds(1)
                self.type(loc["expiry_year"], data['expiry_year'])
            else:
                self.type(loc["expiry_date"], data["expiry_date"])
                self.wait_in_seconds(1)
            self.type(loc["cvv"], data["cvv"])
            self.wait_in_seconds(1)
            self.press(loc["pay_securely"])
            self.wait_in_seconds(5)
            if card_fee is None and (card == 'visa' or card == 'mc') and \
                    festival != 'm2020':
                self.switch_to_iframe(loc["payment_iframe"])
                self.type(loc["payment_pin"], data['pin'])
                self.press(loc["payment_submit"])
        elif pay_by == 'bank_transfer':
            if festival == 'cl':
                current_date = datetime.date.today().strftime("%d-%B-%Y")
                current_date = current_date.split('-')
                if current_date[1] == 'June' and current_date[0] >= '06':
                    self.press(loc["pay_at_festival_tab"])
                    self.press(loc["pay_at_festival_button"])
                else:
                    self.press(loc['bank_transfer'])
                    self.press(loc["place_order"])
            elif festival == 'sa' or festival == 'm2020':
                self.press(loc['bank_transfer'])
                self.press(loc["place_order"])
        elif pay_by == 'free':
            self.press(loc["place_order"])
        self.wait_in_seconds(5)
        self.wait_until_appear(loc["see_your_passes"], timeout=120)
        order_id = str(self.find_by_locator(
            loc["order_id"]).text).split("\n")[1]
        order_id = order_id.split(' (')[0]
        self.wait_in_seconds(2)
        if card is not None:
            print "Card Type: " + card
        else:
            print "Payment Type: Bank Transfer"
        print order_id
        assert self.is_element_visible(loc["see_your_passes"])
        assert self.is_element_visible(
            loc["social_networking"].format('facebook'))
        assert self.is_element_visible(
            loc["social_networking"].format('twitter'))
        assert self.is_element_visible(
            loc["social_networking"].format('linkedin'))
        self.press(loc["see_your_passes"])
        self.wait_until_appear(loc["pending_order"], timeout=60)
        assert self.is_element_visible(loc["pending_order"])
        pp_count = self.find_by_locator(loc["purchased_pass_count"])
        pp_count = int(pp_count.text.split()[0])
        if pp_count > 7:
            self.press(loc["pp_tile_view"])
            print "Changing to Tile View"
        return True, order_id

    def add_senior_officer(self, data):
        self.press(loc["add_senior_officer"])
        self.wait_until_appear(loc["officer_save_and_close"])
        self.type(loc["officer_full_name"], data['officer_full_name'])
        self.type(loc["officer_position"], data['officer_position'])
        self.press(loc["officer_terms"])
        self.press(loc["officer_save_and_close"])
        self.wait_until_appear(loc["choose_payment_heading"])
        assert self.is_element_visible(loc["officer_checked"])
        return True

    def change_country_and_vat(self, data, fill_vat=None, skip=None):
        self.press(loc["change_invoice"])
        self.wait_until_appear(loc["invoice_save_and_close"])
        self.select(loc["billing_country"].format(
            'invoiceForm'), data['country'])
        if fill_vat:
            self.clear_and_type(loc["vat_number"], data['vat_number'])
        if fill_vat is None:
            try:
                if self.is_element_visible(loc["vat_number"]):
                    self.clear(loc["vat_number"])
            except NoSuchElementException:
                pass
        self.wait_in_seconds(2)
        self.press(loc["invoice_save_and_close"])
        self.wait_until_disappear(loc["invoice_cancel"], timeout=60)
        return True

    def is_vat_needed(self, data):
        vat_free = self.is_vat_free(data)
        if vat_free:
            assert not self.is_element_visible(loc["cl_awards_vat"])
            return False
        else:
            assert self.is_element_visible(loc["cl_awards_vat"])
            return True

    def add_company_details(self, details, data, vat_fill=None):
        form_id = None
        if details == 'invoice':
            form_id = 'invoiceForm'
            self.press(loc["add_invoice"])
            self.wait_until_appear(loc["invoice_save_and_close"])
        elif details == 'billing':
            form_id = 'billingForm'
            self.press(loc["add_billing_company"])
            self.wait_until_appear(loc["billing_save_and_close"])
        self.type(loc["billing_company_name"].format(
            form_id), data['company_name'])
        self.type(loc["billing_address_1"].format(
            form_id), data['address_1'])
        self.type(loc["billing_address_2"].format(
            form_id), data['address_2'])
        self.type(loc["billing_pobox"].format(form_id), data['pobox'])
        self.type(loc["billing_city"].format(form_id), data['city'])
        self.type(loc["billing_postcode"].format(form_id), data['postcode'])
        self.select(loc["billing_country"].format(form_id), data['country'])
        if details == 'invoice':
            self.type(loc["billing_email"].format(form_id), data['email'])
            if vat_fill:
                self.type(loc["vat_number"], data['vat_number'])
                self.wait_in_seconds(3)
            self.press(loc["invoice_save_and_close"])
        elif details == 'billing':
            self.press(loc["billing_save_and_close"])
        self.wait_in_seconds(2)
        self.wait_until_appear(loc["choose_payment_heading"])
        if details == 'invoice':
            assert self.is_element_visible(loc["invoice_checked"])
        elif details == 'billing':
            assert self.is_element_visible(loc["billing_checked"])
        return True

    def go_to_payment_page(self):
        self.press(loc["cart_button"])
        self.wait_until_appear(loc["proceed_to_checkout"])
        self.press(loc["proceed_to_checkout"])

    def process_entries_payment(self, pay_by, data=None, total=None, gst=None,
                                 card_type=None, festival=None):
        if pay_by == 'credit_card':
            self.press(loc["awards_cc_type"])
            self.type(loc["awards_name_on_card"], data['name_on_card'])
            self.type(loc["awards_cc_number"], data['cc_number'])
            if festival == 'sa' and total is not None:
                if festival == 'sa':
                    if card_type == 'mc':
                        fee = 0.0165
                    elif card_type == 'visa':
                        fee = 0.0165
                    elif card_type == 'amex':
                        fee = 0.0395
                    total_card_fee = round((total * fee), 2)
                if gst is not None:
                    fee = 0.07
                    gst = total * fee
                    assert self.is_element_visible(loc["sa_gst"])
                    gst_from_ui = float(self.find_by_locator(
                        loc["sa_gst"]).text)
                    assert gst_from_ui == gst
                else:
                    gst = 0
                self.wait_until_appear(loc["sa_cc_fee"], timeout=20)
                fee_from_ui = float(self.find_by_locator(loc["sa_cc_fee"]).text)
                total_plus_cc_from_ui = float(self.find_by_locator(
                    loc["sa_total_with_fee"]).text)
                total_with_card_fee = float(total) + float(total_card_fee) + \
                                      float(gst)
                print "Card Fee: " + str(total_card_fee)
                print "Total: " + str(total_with_card_fee)
                assert float(fee_from_ui) == float(total_card_fee)
                assert float(total_plus_cc_from_ui) == float(total_with_card_fee)

            self.type(loc["awards_month"], data['expiry_month'])
            self.type(loc["awards_year"], data['expiry_year'])
            self.type(loc["awards_cvv_1"], data['cvv_1'])
            self.type(loc["awards_cvv_2"], data['cvv_2'])
            self.type(loc["awards_cvv_3"], data['cvv_3'])
            if card_type == 'amex':
                self.type(loc["awards_cvv_4"], data['cvv_4'])
            self.press(loc["awards_pay_securely"])
            if festival == 'sa':
                print "No Secure Payment Gateway"
            else:
                self.process_secure_gateway(data)
        elif pay_by == 'bank_transfer':
            self.press(loc["awards_bank_type"])
            self.wait_until_appear(loc["awards_confirm_payment"])
            self.press(loc["awards_confirm_payment"])

        self.wait_until_appear(loc["payment_confirmation"], timeout=100)
        assert self.is_element_visible(loc["payment_confirmation"])
        self.press(loc["my_dashboard"])
        self.wait_until_appear(loc["new_campaign"])
        return True

    def process_secure_gateway(self, data):
        self.wait_until_appear(loc["gateway_heading"])
        self.type(loc["gateway_pin"], data['pin'])
        self.press(loc["gateway_submit"])

    def process_card_fee(self, festival, card_type, total):
        if festival == 'spikes_asia':
            if card_type == 'mc':
                fee = 0.0165
            elif card_type == 'visa':
                fee = 0.0165
            elif card_type == 'amex':
                fee = 0.0395
            total_card_fee = round((total * fee), 2)
            return total_card_fee

from BasePage import BasePage
from selenium.webdriver.common.by import By
from Exceptions import NoSuchElementException
import datetime

loc = {
    "account_button": "xpath=//a[contains(@href, 'my-account')]",
    "my_account_logo": "xpath=//span[text()='My Account']",
    "create_account_button": "xpath=//a[text()='Create Account']",
    "create_new_account": "xpath=//a[contains(text(), 'create a new']",
    "title": "xpath=//select[@name='title']",
    "first_name": "xpath=//input[@name='firstName']",
    "last_name": "xpath=//input[@name='lastName']",
    "job_title": "xpath=//input[@name='jobTitle']",
    "job_role": "xpath=//select[@name='companyRole']",
    "mobile": "xpath=//input[@name='mobile']",
    "email": "xpath=//input[@name='email']",
    "password": "xpath=//input[@name='password']",
    "next_step": "xpath=//button[text()='Next step']",
    "login_button": "xpath=//button[contains(@class, 'c-btn--primary')]",
    "company_name": "xpath=//input[@name='companyName']",
    "address1": "xpath=//input[@name='address']",
    "address2": "xpath=//input[@name='address2']",
    "city": "xpath=//input[@name='city']",
    "postcode": "xpath=//input[@name='postcode']",
    "country": "xpath=//select[@name='country']",
    "company_role": "xpath=//select[@name='companyRole']",
    "company_activity": "xpath=//select[@name='companyActivity']",
    "company_type": "xpath=//select[@name='companyType']",
    "create_your_account": "xpath=//button[text()='Create your Account']",
    "my_account_heading": "xpath=//span[text()='My Account']",
    "my_passes_heading": "xpath=//span[text()='My Passes']",
    "purchased_passes_heading": "xpath=//h2[text()='Purchased Passes']",
    "my_store_orders_heading": "xpath=//span[text()='My store orders']",
    "account_details": "xpath=//a[contains(@href, 'edit-profile')]",
    "help": "xpath=//a[contains(@href, 'support')]",
    "logout_button": "xpath=//a[contains(@href, 'logout')]",
    "pass_manager": "xpath=//a[contains(@href, 'pass-manager')]",
    "go_to_pass_manager": "xpath=//*[text()='Go to Pass Manager']",
    "buy_a_pass": "xpath=//a[text()='Buy a Pass']",
    "registration_invoices":
        "xpath=//a[contains(@href,'registration-invoices')]",
    "go_to_store": "xpath=//span[text()='Go to Store']",
    "pending_passes": "xpath=//p[contains(text(), 'Pending passes')]",
    "pending_passes_count":
        "xpath=//p[contains(text(), 'Pending passes')]//span",
    "ready_to_go_passes": "xpath=//p[contains(text(), 'Purchased Passes')]",
    "ready_to_go_passes_count":
        "xpath=//p[contains(text(), 'Purchased Passes')]//span",
    "generic_data": "xpath=//*[text()={}]",
    "edit_profile": "xpath=//span[text()='Edit Profile']",
    "save_changes": "xpath=//button[text()='Save Changes']",
    "profile_tab": "xpath=//a[contains(@href, 'edit-profile')]",
    "marketing_tab": "xpath=//a[contains(@href, 'marketing-preferences')]",
    "password_tab": "xpath=//a[contains(@href, 'change-password')]",
    "tab_heading": "xpath=//span[text()='{}']",
    "current_password": "xpath=//input[@id='input-current-password']",
    "new_password": "xpath=//input[@id='input-new-password']",
    "confirm_pass_change":
        "xpath=//strong[text()='Your password has been changed']",
    "marketing_change": "xpath=//strong[text()='Your Marketing "
                        "Preferences have been changed']",
    "profile_changed":
        "xpath=//strong[text()='Profile was successfully updated']",
    "marketing_check_box": "xpath=//label[contains(text(),'{}')]/..//"
                           "input[contains(@id, 'Data_MarketingPreferences')]",
    "sf_title": "xpath=//select[@id='Data_Title']",
    "sf_first_name": "xpath=//input[@id='Data_FirstName']",
    "sf_last_name": "xpath=//input[@id='Data_LastName']",
    "sf_password": "xpath=//input[@id='Data_Password']",
    "sf_job_title": "xpath=//input[@id='Data_JobTitle']",
    "sf_company_role": "xpath=//select[@id='Data_CompanyRole']",
    "sf_company_name": "xpath=//input[@id='Data_CompanyName']",
    "sf_address1": "xpath=//input[@id='Data_Address']",
    "sf_address2": "xpath=//input[@id='Data_Address2']",
    "sf_city": "xpath=//input[@id='Data_City']",
    "sf_postcode": "xpath=//input[@id='Data_Postcode']",
    "sf_country": "xpath=//select[@id='Data_Country']",
    "sf_mobile": "xpath=//input[@id='Data_Mobile']",
    "reg_invoice": "xpath=//span[text()='Registration invoices']",
    "reg_invoice_table": "xpath=//table[contains(@class, 'c-table')]//tbody",
    "pass_manager_passes": "xpath=//div[@id='purchased-passes-total']",
    "pending_order": "xpath=//h2[contains(text(),'Pending Order')]",
    "update_details": "xpath=//h3[text()='Update Your Details']",
    "create_campaign": "xpath=//span[text()='Create New Campaign']",
    "pass_type": "xpath=//h4[text()='{}']",
    "forgot_password": "xpath=//a[text()='Forgot password?']",
    "send_email": "xpath=//button[contains(text(),'Send')]",
    "forgot_email": "xpath=//input[@id='Email']",
    "sent_confirmation": "xpath=//h1[text()='Check your email']",
    "new_password_heading": "xpath=//label[text()='New password']",
    "save_new_password": "xpath=//button[text()='Save new password']"


}

dl = {
    "register_heading": "xpath=//h1[text()='Buy Passes']",
    "create_campaign": "xpath=//span[text()='Create New Campaign']",
    "brochure_menu_generic": "xpath=//span[text()='{}']",
    "update_details": "xpath=//h3[text()='Update Your Details']",
    "title": "xpath=//select[@id='title_id']",
    "job_role": "xpath=//select[@id='job_role_id']",
    "country": "xpath=//select[@id='countrycode']",
    "company_activity": "xpath=//select[@id='cotype']",
    "change_details": "xpath=//form[@name='registrationForm']//input["
                      "@type='submit']",
    "company_type": "xpath=//select[@id='company_subtype_id']",
    "create_entry": "xpath=//a[text()='Create an entry']",
    "person_icon": "xpath=//a[contains(@href, 'my-account')]",
    "awards_person_icon": "xpath=//span[contains(@class, 'icon-user')]",
    "close": "xpath=//a[text()='Close']",

}
err = {
    "error_email": "xpath=//div[text()='Please provide an email address']",
    "error_pwd": "xpath=//div[text()='Please provide a password']",
    "invalid_creds": "xpath=//p[text()='Username or password is not valid.']",
    "error_title": "xpath=//div[text()='Required']",
    "error_first_name": "xpath=//div[text()='First name is required']",
    "error_last_name": "xpath=//div[text()='Last name is required']",
    "error_create_email": "xpath=//div[text()='Email Address is required']",
    "error_pasword": "xpath=//div[contains(text(), 'Password')]",
    "error_job_title": "xpath=//div[text()='Job title is required']",
    "error_mobile":
        "xpath=//div[contains(text(), 'hone number')]",  # on purpose
    "error_company": "xpath=//div[text()='Company name is required']",
    "error_address": "xpath=//div[text()='Address is required']",
    "error_city": "xpath=//div[text()='City is required']",
    "error_postcode": "xpath=//div[text()='Postcode/Zip is required']",
    "error_country": "xpath=//div[text()='Country is required']",
    "error_company_role": "xpath=//div[text()='Company role is required']",
    "error_company_activity":
        "xpath=//div[text()='Company activity is required']",
    "error_company_type": "xpath=//div[text()='Company type is required']",



}
sa = {
    "account_button": "xpath=//span[contains(@class,'icon-user')]"
}

m2020 = {
    "company_stage": "xpath=//select[@id='companyStage']",
    "company_sector": "xpath=//select[@id='companySector']",
    "revenue_bracket": "xpath=//select[@id='revenueBracket']",
    "job_department": "xpath=//select[@id='jobDepartment']",
    "job_role": "xpath=//input[@id='jobTitle']"
}


class AccountPage(BasePage):

    def __init__(self, driver):
        super(AccountPage, self).__init__(driver=driver)

    def switch_system(self, system):
        url = self.current_url()
        if 'awards' in url:
            print "Current system is Awards"
        else:
            print "Current system is Delegates"
        if system == 'delegates':
            print "Changing to Delegates"
            self.press(dl["awards_person_icon"])
            self.wait_until_appear(loc["pending_passes"])
            return self.is_element_visible(loc["pending_passes"])

    def create_account(self, data, site=None, system=None):
        if site == 'dubai':
            self.wait_until_appear(loc["login_button"])
            self.press(loc["create_account_button"])
        elif site == 'cl_awards' or site == 'sa_awards':
            self.go_to_create_account(data, awards=True)
        elif site == 'spikes_asia':
            self.go_to_create_account(data, festival='spikes_asia')
        elif site == 'm2020':
            self.go_to_create_account(data, festival='m2020')
        else:
            self.go_to_create_account(data)
        self.fill_page_1_details(data, festival=site)
        self.press(loc["next_step"])
        self.fill_page_2_details(data, festival=site)
        self.press(loc["create_your_account"])
        if site == 'dubai':
            self.wait_in_seconds(5)
            self.wait_until_appear(dl["update_details"])
            return self.is_element_visible(dl["update_details"])
        if site == 'cl_awards' or site == 'sa_awards':
            self.wait_until_appear(loc["create_campaign"])
            return self.is_element_visible(loc["create_campaign"])
        self.wait_until_appear(loc["my_account_heading"])
        assert self.is_element_visible(loc["my_account_heading"])
        # val1 = self.find_by_locator(loc["ready_to_go_passes_count"]).text
        # val2 = self.find_by_locator(loc["pending_passes_count"]).text
        # assert val1 == '0' and val2 == '0'
        print self.current_url()
        return True

    def check_account_page(self, data, check):
        self.driver.get(data['url'])
        self.press(loc["account_button"])
        if check == 'login':
            self.wait_until_appear(loc["email"])
            assert self.is_element_visible(loc["email"])
            assert self.is_element_visible(loc["password"])
            assert self.is_element_visible(loc["login_button"])
        elif check == 'create':
            self.press(loc["create_account_button"])
            assert self.is_element_visible(loc["title"])
            assert self.is_element_visible(loc["first_name"])
            assert self.is_element_visible(loc["last_name"])
            assert self.is_element_visible(loc["job_title"])
            assert self.is_element_visible(loc["email"])
            assert self.is_element_visible(loc["mobile"])
            assert self.is_element_visible(loc["next_step"])
        return True

    def update_account_details(self, change, data):
        self.wait_in_seconds(2)
        if self.is_element_visible(loc["logout_button"]):
            self.press(loc["account_details"])
            self.wait_until_appear(loc["edit_profile"])
        else:
            print "Not logged in. Login First"
        if change == 'profile':
            assert self.is_element_visible(loc["tab_heading"].format(
                'Edit Profile'))
            self.select(loc["sf_title"], data['title'])
            self.clear_and_type(loc["sf_first_name"], data['first_name'])
            self.clear_and_type(loc["sf_last_name"], data['last_name'])
            self.clear_and_type(loc["sf_password"], data['password'])
            self.clear_and_type(loc["sf_job_title"], data['job_title'])
            self.clear_and_type(loc["sf_company_name"], data['company_name'])
            self.clear_and_type(loc["sf_address1"], data['address1'])
            self.clear_and_type(loc["sf_address2"], data['address2'])
            self.clear_and_type(loc["sf_city"], data['city'])
            self.clear_and_type(loc["sf_postcode"], data['postcode'])
            self.select(loc["sf_country"], data['country'])
            self.clear_and_type(loc["sf_mobile"], data['mobile'])
            self.press(loc["save_changes"])
            assert self.is_element_visible(loc["profile_changed"])
        elif change == 'marketing':
            self.press(loc["marketing_tab"])
            assert self.is_element_visible(loc["tab_heading"].format(
                'Marketing Preferences'))
            brand_mapping = {
                "cl": "Cannes Lions", "dl": "Dubai Lynx", "eb": "eurobest",
                "sa": "Spikes Asia", "ae": "Ascential Events"
            }
            for brand in data['marketing'].keys():
                brand_value = self.find_by_locator(
                    loc["marketing_check_box"].format(brand_mapping[brand])
                ).get_attribute('checked')
                if brand_value == 'true' and data['marketing'][brand] == 'true':
                    print "Previous and Current Value are same"
                elif brand_value == 'true' and \
                        data['marketing'][brand] == 'false':
                    self.press(loc["marketing_check_box"].format(
                        brand_mapping[brand]))
                    updated_value = self.find_by_locator(
                        loc["marketing_check_box"].format(brand_mapping[brand])
                    ).get_attribute('checked')
                    assert updated_value is None
                elif brand_value is None and \
                        data['marketing'][brand] == 'false':
                    print "Previous and Current Value are same -"
                elif brand_value is None and \
                        data['marketing'][brand] == 'true':
                    self.press(loc["marketing_check_box"].format(
                        brand_mapping[brand]))
                    updated_value = self.find_by_locator(
                        loc["marketing_check_box"].format(brand_mapping[brand])
                    ).get_attribute('checked')
                    assert updated_value == 'true'
            self.press(loc["save_changes"])
            assert self.is_element_visible(loc["marketing_change"])
        elif change == 'password':
            self.press(loc["password_tab"])
            assert self.is_element_visible(loc["tab_heading"].format(
                'Change Password'))
            self.type(loc["current_password"], data['password'])
            self.type(loc["new_password"], data['new_password'])
            self.press(loc["save_changes"])
            assert self.is_element_visible(loc["confirm_pass_change"])
        self.press(loc["account_button"])
        assert self.is_element_visible(loc["my_account_heading"])
        return True

    def go_to_create_account(self, data, awards=None, festival=None):
        print data['email']
        self.driver.get(data['url'])
        if awards:
            pass
        elif festival == 'spikes_asia' or festival == 'm2020':
            self.press(sa["account_button"])

        else:
            self.press(loc["account_button"])
        self.press(loc["create_account_button"])
        assert "register" in self.current_url()
        return True

    def check_registration_invoices(self, expected_rows):
        self.press(loc["account_button"])
        assert self.is_element_visible(loc["my_account_heading"])
        self.press(loc["reg_invoice"])
        assert "registration-invoices" in self.current_url()
        assert self.is_element_visible(loc["reg_invoice"])
        table = self.find_by_locator(loc["reg_invoice_table"])
        rows = table.find_elements(By.TAG_NAME, "tr")
        count = 0
        for row in rows:
            self.driver.find_element_by_tag_name('tr')
            count += 1
        return expected_rows == count

    def check_pass_count(self, pending_count=None, ready_count=None):
        self.press(loc["account_button"])
        assert self.is_element_visible(loc["my_account_heading"])
        ready = str(self.find_by_locator(loc["ready_to_go_passes_count"]).text)
        pending = str(self.find_by_locator(loc["pending_passes_count"]).text)
        if pending_count is not None:
            assert int(pending_count) == int(pending)
        if ready_count is not None:
            assert int(ready_count) == int(ready)
        self.press(loc["go_to_pass_manager"])
        self.wait_until_appear(loc["pending_order"])
        passes = str(self.find_by_locator(loc["pass_manager_passes"]).text)
        pass_count = int(passes.split()[0])
        assert ready_count == pass_count
        return True

    def fill_page_1_details(self, data, skip=None, festival=None):
        self.press(loc["first_name"])
        self.select(loc["title"], data['title'])
        self.type(loc["first_name"], data['first_name'])
        self.type(loc["last_name"], data['last_name'])
        self.type(loc["email"], data['email'])
        if skip == "password":
            print "Entering password is skipped"
        else:
            self.type(loc["password"], data['password'])
        self.type(loc["job_title"], data['job_title'])
        self.type(loc["mobile"], data['mobile'])
        if festival != 'm2020':
            self.select(loc["job_role"], data['job_role'])
        if festival == 'm2020':
            self.type(m2020["job_role"], data['job_role'])
            self.select(m2020["job_department"], data['job_department'])

    def fill_page_2_details(self, data, skip=None, festival=None):
        '' if skip == "company" else self.type(
            loc["company_name"], data['company_name'])
        '' if skip == "address1" else self.type(
            loc["address1"], data['address1'])
        '' if skip == "address2" else self.type(
            loc["address2"], data['address2'])
        '' if skip == "postcode" else self.type(
            loc["postcode"], data['postcode'])
        '' if skip == "city" else self.type(
            loc["city"], data["city"])
        '' if skip == "country" else self.select(
            loc["country"], data['country'])
        # '' if skip == "company_role" else self.select(
        #     loc["company_role"], data['company_role'])
        if festival != 'm2020':
            '' if skip == "company_activity" else self.select(
                loc["company_activity"], data['company_activity'])
            '' if skip == "company_type" else self.select(
                loc["company_type"], data['company_type'])
        if festival == 'm2020':
            self.select(m2020["company_stage"], data['company_stage'])
            self.select(m2020["company_sector"], data['company_sector'])
            self.select(m2020["revenue_bracket"], data['revenue_bracket'])

    def log_into_account(self, data, site=None, system=None, account=None):
        if site == 'dubai' and system == 'brochure':
            #self.screen_shot()
            self.wait_until_appear(
                dl["brochure_menu_generic"].format("The Festival"))
            return self.is_element_visible(
                dl["brochure_menu_generic"].format("The Festival"))
        if site == 'dubai':
            pass
        else:
            self.driver.get(data['url'])
            self.press(loc["account_button"])
            assert "my-account" in self.current_url()
        self.wait_until_appear(loc["login_button"])
        self.press(loc["email"])
        self.type(loc["email"], data["username"])
        self.type(loc["password"], data["password"])
        self.press(loc["login_button"])
        if site == 'dubai':
            if system == 'register':
                self.wait_until_appear(dl["register_heading"])
                return self.is_element_visible(dl["register_heading"])
            elif system == 'awards':
                if account == 'old':
                    self.wait_until_appear(dl["update_details"], timeout=5)
                    self.select(dl["title"], data['title'])
                    self.select(dl["job_role"], data['job_role'])
                    self.select(dl["country"], data['country'])
                    self.select(dl["company_activity"], data['comp_activity'])
                    self.wait_until_appear(dl["company_type"])
                    self.select(dl["company_type"], data['company_type'])
                    if self.is_element_visible(dl["close"]):
                        self.press(dl["close"])
                    self.press(dl["change_details"])
                    self.wait_until_appear(dl["create_entry"])
                    self.press(dl["create_entry"])
                self.wait_until_appear(dl["create_campaign"])
                return self.is_element_visible(dl["create_campaign"])
        else:
            self.wait_until_appear(loc["my_account_logo"])
            assert self.is_element_visible(loc["my_account_logo"])
            return True

    def check_account_validations(self, data, skip, domain=None):
        if domain == 'dubai':
            pass
        elif domain == 'spikes_asia':
            self.driver.get(data['url'])
            self.press(sa["account_button"])
            assert "my-account" in self.current_url()
        else:
            self.driver.get(data['url'])
            self.press(loc["account_button"])
            assert "my-account" in self.current_url()
        self.wait_until_appear(loc["login_button"])
        if skip == 'both':
            self.press(loc["login_button"])
            self.wait_until_appear(err["error_pwd"])
            assert self.is_element_visible(err["error_email"])
            assert self.is_element_visible(err["error_pwd"])
        elif skip == 'email':
            self.type(loc["password"], data["password"])
            self.press(loc["login_button"])
            self.wait_until_appear(err["error_email"])
            assert self.is_element_visible(err["error_email"])
        elif skip == 'password':
            self.press(loc["email"])
            self.type(loc["email"], data["username"])
            self.press(loc["login_button"])
            self.wait_until_appear(err["error_pwd"])
            assert self.is_element_visible(err["error_pwd"])
        elif skip == 'invalid':
            self.type(loc["email"], data["username"])
            self.type(loc["password"], data["password"])
            self.press(loc["login_button"])
            self.wait_until_appear(err["invalid_creds"])
            assert self.is_element_visible(err["invalid_creds"])
        return True

    def check_account_creation_validations(self, skip, data, festival=None):
        errors_1 = [err["error_title"], err["error_first_name"],
                    err["error_last_name"], err["error_create_email"],
                    err["error_pasword"], err["error_job_title"],
                    err["error_mobile"]]
        if festival == 'dl':
            pass
        else:
            self.driver.get(data['url'])
            self.press(loc["account_button"])
        self.press(loc["create_account_button"])
        if skip == "all_1":
            if festival == 'm2020':
                errors_1.remove(err["error_title"])
            self.wait_until_appear(loc["next_step"])
            self.press(loc["next_step"])
            for error in errors_1:
                assert self.is_element_visible(error)
                return True
        if skip == "password":
            self.fill_page_1_details(data, skip="password", festival=festival)
            self.press(loc["next_step"])
            assert self.is_element_visible(err["error_pasword"])

        self.fill_page_1_details(data, festival=festival)
            # The data will be filled for page 1
        if skip == "first_name":
            self.clear(loc["first_name"])
            self.press(loc["next_step"])
            assert self.is_element_visible(err["error_first_name"])
        elif skip == "last_name":
            self.clear(loc["last_name"])
            self.press(loc["next_step"])
            assert self.is_element_visible(err["error_last_name"])
        elif skip == "email":
            self.clear(loc["email"])
            self.press(loc["next_step"])
            assert self.is_element_visible(err["error_create_email"])
        elif skip == "job_title":
            self.clear(loc["job_title"])
            self.press(loc["next_step"])
            assert self.is_element_visible(err["error_job_title"])
        elif skip == "mobile":
            self.clear(loc["mobile"])
            self.press(loc["next_step"])
            assert self.is_element_visible(err["error_mobile"])
        self.press(loc["next_step"])
        if skip == "company_name":
            self.fill_page_2_details(data, skip="company", festival=festival)
            self.press(loc["create_your_account"])
            assert self.is_element_visible(err["error_company"])
        elif skip == "address1":
            self.fill_page_2_details(data, skip="address1", festival=festival)
            self.press(loc["create_your_account"])
            assert self.is_element_visible(err["error_address"])
        elif skip == "city":
            self.fill_page_2_details(data, skip="city", festival=festival)
            self.press(loc["create_your_account"])
            assert self.is_element_visible(err["error_city"])
        elif skip == "postcode":
            self.fill_page_2_details(data, skip="postcode", festival=festival)
            self.press(loc["create_your_account"])
            assert self.is_element_visible(err["error_postcode"])
        elif skip == "country":
            self.fill_page_2_details(data, skip="country", festival=festival)
            self.press(loc["create_your_account"])
            assert self.is_element_visible(err["error_country"])
        elif skip == "company_role":
            self.fill_page_2_details(
                data, skip="company_role", festival=festival)
            self.press(loc["create_your_account"])
            assert self.is_element_visible(err["error_company_role"])
        elif skip == "company_activity":
            self.fill_page_2_details(
                data, skip="company_activity", festival=festival)
            self.press(loc["create_your_account"])
            assert self.is_element_visible(err["error_company_activity"])
        elif skip == "company_type":
            self.fill_page_2_details(
                data, skip="company_type", festival=festival)
            self.press(loc["create_your_account"])
            assert self.is_element_visible(err["error_company_type"])
        return True

    def logout(self, data, site=None):
        if site == 'dubai':
            self.press(dl["person_icon"])
        else:
            url = data['url'] + 'my-account'
            self.get_web_page(url)
        self.wait_until_appear(loc["logout_button"])
        assert self.is_element_visible(loc["logout_button"])
        self.press(loc["logout_button"])
        return True

    def forgot_password(self, data):
        self.get_web_page(data['url'])
        self.wait_until_appear(loc["account_button"], timeout=20)
        self.press(loc["account_button"])
        self.press(loc["forgot_password"])
        self.wait_until_appear(loc["send_email"])
        assert self.is_element_visible(loc["send_email"])
        self.type(loc["forgot_email"], data['email'])
        self.press(loc["send_email"])
        self.wait_until_appear(loc["sent_confirmation"])
        assert self.is_element_visible(loc["sent_confirmation"])
        return True

    def create_password_and_login(self, url, data):
        self.get_web_page(url)
        self.wait_until_appear(loc["new_password_heading"])
        self.type(loc["new_password"], data['new_password'])
        self.press(loc["save_new_password"])
        self.wait_until_appear(loc["email"], timeout=10)
        self.type(loc["email"], data['email'])
        self.type(loc["password"], data['new_password'])
        self.press(loc["login_button"])
        self.wait_until_appear(loc["my_account_heading"])
        assert self.is_element_visible(loc["my_account_heading"])
        return True

    def check_for_pass(self, data):
        self.get_web_page(data['url'])
        self.wait_until_appear(loc["my_account_logo"])
        assert self.is_element_visible(loc["my_account_logo"])
        self.press(loc["pass_manager"])
        self.wait_until_appear(loc["purchased_passes_heading"])
        old_time = int((datetime.datetime.now()).strftime("%M"))
        print old_time
        assert self.is_element_visible(loc["purchased_passes_heading"])
        while self.is_element_visible(loc["pass_type"].format(data['pass_type']), screenshot=False) is False:
            print "Pass not synced yet.Trying in 30 seconds"
            self.wait_in_seconds(30)
            self.reload()
            time = int((datetime.datetime.now()).strftime("%M"))
            elapsed_time = time - old_time
            if elapsed_time > 20:
                print "Total time is more than 20 mins"
            #import pdb;pdb.set_trace()
            if self.is_element_visible(loc["pass_type"].format(data['pass_type']), screenshot=False):
                print "im in if"
                break
        new_time = int((datetime.datetime.now()).strftime("%M"))
        print new_time
        print "Total time from SF to Web: " + str(new_time - old_time) + "mins"
        print "Salesforce to Webportal Sync successful"
        return True





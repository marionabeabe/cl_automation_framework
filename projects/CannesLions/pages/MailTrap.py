from BasePage import BasePage

loc = {
    "login_heading": "xpath=//h1[text()='Log In]",
    "email": "xpath=//input[@id='user_email']",
    "password": "xpath=//input[@id='user_password']",
    "login_button": "xpath=//input[@name='commit']",
    "create_inbox": "xpath=//input[@value='Create Inbox']",
    "mail_box_generic": "xpath=//span[text()='{}']",
    "selected_mailbox": "xpath=//*[contains(text(),'{}')]",
    "correct_email":
        "xpath=//span[text()='Cannes Lions Password Reset Instructions'"
        "]/..//span[text()='{}']",
    "selected_email": "xpath=//span[text()='<{}>']",
    "click_here": "xpath=//a[text()='click here']",
    "frame": "//iframe[@class='flex-item']",
}


class MailTrapPage(BasePage):

    def __init__(self, driver):
        super(MailTrapPage, self).__init__(driver=driver)

    def do_login(self, data):
        self.get_web_page(data['url'])
        self.wait_in_seconds(3)
        self.type(loc["email"], data['email'])
        self.type(loc["password"], data['password'])
        self.press(loc["login_button"])
        self.wait_until_appear(loc["create_inbox"], timeout=10)
        assert self.is_element_visible(loc["create_inbox"])
        return True

    def select_mailbox_and_email(self, mailbox, email):
        self.press(loc["mail_box_generic"].format(mailbox))
        self.wait_in_seconds(2)
        self.press(loc["correct_email"].format(email))
        self.wait_in_seconds(2)
        self.switch_to_iframe(loc["frame"])
        token_with_url = self.find_by_locator(
            loc["click_here"]).get_attribute('href')
        self.switch_to_default_content()
        return token_with_url

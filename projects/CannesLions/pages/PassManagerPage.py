import datetime

from selenium.common.exceptions import NoSuchElementException

from projects.CannesLions.pages.OrderPage import OrderPage
from projects.CannesLions.pages.PassPage import pass_prices, companion_prices
from projects.SpikesAsia.pages.PassPage import pass_prices as sa_pass_prices

loc = {
    "pending_order": "xpath=//h2[contains(text(),'Pending Order')]",
    "purchased_passes": "xpath=//h2[text()='Purchased Passes']",
    "pass_options":
        "xpath=//h4[text()='{}']/../..//span[text()='Pass Options']",
    "current_pass": "xpath=//h4[text()='{}']",
    "pass_options_generic":
        "xpath=//h4[text()='{}']/../../../..//span[contains(text(),'{}')]",
    "upload_photo": "xpath=//button[text()='Upload photo']",
    "book_accommodation": "xpath=//button[text()='Book Accommodation']",
    "edit_delegate": "xpath=//button[text()='Edit delegate information']",

    "del_first_name": "xpath=//input[@id='root_firstName']",
    "del_last_name": "xpath=//input[@id='root_secondName']",
    "del_job_title": "xpath=//input[@id='root_jobTitle']",
    "del_email": "xpath=//input[@id='root_emailAddress']",
    "company_name": "xpath=//input[@id='root_companyName']",
    "del_comp_activity":
        "xpath=//select[@id='root_companyData_companyActivity']",
    "del_comp_type":
        "xpath=//select["
        "@id='root_companyData_activitySubsection_companyTypeMedia']",
    "address1": "xpath=//input[@id='root_address1']",
    "address2": "xpath=//input[@id='root_address2']",
    "city": "xpath=//input[@id='root_city']",
    "postcode": "xpath=//input[@id='root_postcode']",
    "country": "xpath=//select[@id='root_country']",
    "save_and_close": "xpath=//button[text()='Save and close']",
    "save_and_continue": "xpath=//button[text()='Save and continue']",
    "del_mobile": "xpath=//input[@id='root_mobilePhone']",
    "progress_bar": "xpath=//svg[contains(@class, 'CircularProgressbar')]",
    "companion_first_name":
        "xpath=//input[@id='root_companionDetails_companionFirstName']",
    "companion_last_name":
        "xpath=//input[@id='root_companionDetails_companionLastName']",
    "upgrade_heading": "xpath=//h3[text()='Choose your upgrade option']",
    "pass_generic": "xpath=//span[contains(text(), '{}')]",
    "add_to_basket": "xpath=//button[contains(text(),'Add to basket')]",
    "submit_request": "xpath=//button[text()='Submit request']",
    "dob": "xpath=//input[@id='date-of-birth']",
    "place_of_birth": "xpath=//input[@id='root_placeOfBirth']",
    "nationality": "xpath=//select[@id='root_nationality']",
    "embarkation": "xpath=//select[@id='root_countryOfEmbarkation']",
    "passport_number": "xpath=//input[@id='root_passportNumber']",
    "place_of_issue": "xpath=//input[@id='root_passportPlaceOfIssue']",
    "pp_issue_date": "xpath=//input[@id='root_passportIssueDate']",
    "pp_expiry_date": "xpath=//input[@id='root_passportExpiryDate']",
    "visa_start_date": "xpath=//select[@id='root_visaStartDate']",
    "visa_end_date": "xpath=//select[@id='root_visaEndDate']",
    "purchased_pass_count": "xpath=//div[@id='purchased-passes-total']",
    "pp_tile_view":
        "xpath=//h2[text()='Purchased Passes']/../../../../..//i["
        "@data-click='show-cardView']",
    "edit_delegate_info": "xpath=//button[text()='Edit delegate information']",
    "upgrade_price_from_ui":
        "xpath=//span[text()='{}']/..//span[contains(@class,'price')]"

}


class PassManagerPage(OrderPage):

    def upgrade_booked_pass(self, pass_type, upgrade, data, upgrade_pass=None,
                            person=None, account=None, replace_comp=None,
                            correction=None, festival=None, vat_free=None,
                            delegate=None):
        self.reload()
        self.wait_in_seconds(5)
        self.wait_until_appear(loc["purchased_passes"])
        try:
            if self.is_element_visible(loc["purchased_pass_count"]):
                pp_count = self.find_by_locator(loc["purchased_pass_count"])
                pp_count = int(pp_count.text.split()[0])
                if pp_count > 7:
                    self.press(loc["pp_tile_view"])
                    print "Changing to Tile View"
        except NoSuchElementException:
            pass
        self.wait_in_seconds(1)
        self.hover_then_click(loc["current_pass"].format(pass_type),
                              loc["pass_options"].format(pass_type))
        self.wait_in_seconds(2)
        # self.wait_until_appear(
        #     loc["pass_options_generic"].format('Cancel pass'))
        if upgrade == 'delegate':
            self.press(loc["pass_options_generic"].format(pass_type,
                                                          'Delegate details'))
            self.wait_until_appear(loc["edit_delegate"])
            self.press(loc["edit_delegate"])
            self.wait_until_appear(loc["save_and_close"])
            self.clear_and_type(loc["del_job_title"], data['job_title'])
            self.clear_and_type(loc["del_email"], data['email'])
            self.clear_and_type(loc["company_name"], data['company_name'])
            self.clear_and_type(loc["address1"], data['address_1'])
            self.clear_and_type(loc["address2"], data['address_2'])
            self.clear_and_type(loc["city"], data['city'])
            self.clear_and_type(loc["postcode"], data['postcode'])
            self.select(loc["country"], data['country'])
            #self.select(loc["del_comp_type"], data['company_type'])
            self.clear_and_type(loc["del_mobile"], data['mobile'])
        elif upgrade == 'companion':
            self.press(loc["pass_options_generic"].format(pass_type,
                                                          'Add a companion'))
            self.wait_until_appear(loc["save_and_close"])
            self.type(loc["companion_first_name"], data['first_name'])
            self.type(loc["companion_last_name"], data['last_name'])
            self.wait_in_seconds(2)
            self.press(loc["save_and_close"])
            self.wait_until_appear(loc["current_pass"].format(
                pass_type + " + Companion"))
            assert self.is_element_visible(loc["current_pass"].format(
                pass_type + " + Companion"))
            self.is_element_visible(loc["purchased_passes"])
            special_comp_prices = {
                'Complete Young Lion': 'complete_young_lion',
                'Lions Health': 'lions_health',
                'Lions Health Student': 'lions_health_student',
                'Complete Student': 'complete_student',
                'Lions Health Young Lion': 'lions_health_young_lion'
            }
            if pass_type in special_comp_prices.keys():
                self.process_order(
                    upgrade, companion_prices[special_comp_prices[pass_type]],
                    vat_free=vat_free, delegate=delegate)
            else:
                self.process_order(upgrade, pass_prices['companion'],
                                   replace_comp=replace_comp, vat_free=vat_free,
                                   delegate=delegate)
            return True
        elif upgrade == 'pass_type':
            if festival != 'spikes_asia' or festival is None:
                old_pass_price = pass_prices[
                    (pass_type.lower()).replace(' ', '_')]
                new_pass_price = pass_prices[
                    (upgrade_pass.lower()).replace(' ', '_')]
                if correction:
                    upgrade_price = new_pass_price - old_pass_price - 220
                else:
                    upgrade_price = new_pass_price - old_pass_price
            self.press(loc["pass_options_generic"].format(pass_type,
                                                          'Change pass'))
            self.wait_until_appear(loc["upgrade_heading"])
            self.press(loc["pass_generic"].format(upgrade_pass))
            if festival == 'spikes_asia':
                current_pass_price = sa_pass_prices[pass_type]
                upgrade_pass_price = sa_pass_prices[upgrade_pass]
                upgrade_price = upgrade_pass_price - current_pass_price
                upgrade_price_from_ui = \
                    self.find_by_locator(loc["upgrade_price_from_ui"].format(
                        upgrade_pass)).text
                upgrade_price_from_ui = \
                    ((upgrade_price_from_ui.rpartition('+')[0]).split('$'))[1]
                assert int(upgrade_price_from_ui) == upgrade_price
            self.wait_until_appear(loc["add_to_basket"])
            self.press(loc["add_to_basket"])
            self.wait_until_appear(loc["pending_order"])
            assert self.is_element_visible(loc["pending_order"])
            if 'Platinum' in upgrade_pass:
                print "Process Airport Transfers"
                self.process_order(
                    person, upgrade_price, pass_type=upgrade_pass,
                    vat_free=vat_free)
            else:
                self.process_order('noneed', upgrade_price, vat_free=vat_free)
            return True, upgrade_price
        elif upgrade == 'replace':
            current_date = datetime.date.today().strftime("%d-%B-%Y")
            current_date = current_date.split('-')
            late_fee_1_months = ['March', 'April', 'May']
            late_fee_2_months = ['June']
            no_late_fee_months = ['October', 'November', 'December', 'January',
                                  'February']
            if current_date[1] in late_fee_1_months and \
                    int(current_date[0]) >= 1:
                print "Late Fees 1 apply if unnamed chosen"
                pass_prices['replace'] = 220
            elif current_date[1] in late_fee_2_months and \
                    int(current_date[0]) >= 1:
                print "Late fee 2 apply if unnamed chosen"
                pass_prices['replace'] = 280
            elif current_date[1] in no_late_fee_months:
                pass_prices['replace'] = 150
            self.press(loc["pass_options_generic"].format(pass_type,
                                                          'Replace delegate'))
            self.wait_until_appear(loc["pending_order"])
            assert self.is_element_visible(loc["pending_order"])
            self.process_order(person, pass_prices['replace'],
                               account=account, data=data,
                               replace_comp=replace_comp, festival=festival,
                               vat_free=vat_free, delegate=delegate)
            return True
        elif upgrade == 'visa':
            self.press(loc["pass_options_generic"].format(
                pass_type, 'Request visa letter'))
            self.wait_until_appear(loc["submit_request"])
            if festival == 'spikes_asia':
                self.type(loc["place_of_birth"], data['place_of_birth'])
                self.select(loc["embarkation"], data['embarkation'])
                self.type(loc["passport_number"], data['passport_number'])
            else:
                self.type(loc["dob"], data['dob'])
                self.type(loc["place_of_birth"], data['place_of_birth'])
                self.select(loc["nationality"], data['nationality'])
                self.type(loc["passport_number"], data['passport_number'])
                self.type(loc["place_of_issue"], data['place_of_issue'])
                self.type(loc["pp_issue_date"], data['passport_issue_date'])
                self.type(loc["pp_expiry_date"], data['passport_expiry_date'])
                self.select(loc["visa_start_date"], data['visa_start_date'])
                self.select(loc["visa_end_date"], data['visa_end_date'])
            self.wait_in_seconds(1)
            self.press(loc["submit_request"])
            assert self.is_element_visible(loc["purchased_passes"])
            return True
        elif upgrade == 'cancel':
            self.press(loc["pass_options_generic"].format(pass_type,
                                                          'Cancel pass'))
        self.press(loc["save_and_close"])
        self.is_element_visible(loc["purchased_passes"])

        if upgrade == 'photo':
            pass
        elif upgrade == 'accommodation':
            pass
        return True

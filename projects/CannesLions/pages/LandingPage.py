from BasePage import BasePage

loc = {
    "account_button": "xpath=//a[contains(@href, 'my-account')]",
    "buy_pass_button": "xpath=//a[text()='Buy a Pass']",
    "attend_menu": "xpath=//a[text()='Attend']",
    "enter_menu": "xpath=//a[text()='Enter']",
    "learn_menu": "xpath=//a[text()='Learn']",
    "opportunities_menu": "xpath=//a[text()='Opportunities']",
    "email": "xpath=//input[@name='email']",
    "password": "xpath=//input[@name='password']",
    "login_button": "xpath=//button[contains(@class, 'c-btn--primary')]",

}


class LandingPage(BasePage):

    def __init__(self, driver):
        super(LandingPage, self).__init__(driver=driver)

    def load_landing_page(self, data):
        self.driver.get(data['url'])
        home_menus = [loc["attend_menu"], loc["enter_menu"], loc["learn_menu"],
                      loc["opportunities_menu"]]
        for menu in home_menus:
            assert self.is_element_visible(menu)
            return True
        assert self.is_element_visible(loc["account_button"])
        assert self.is_element_visible(loc["buy_pass_button"])





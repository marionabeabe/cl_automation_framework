import requests

from BasePage import BasePage


url = "http://api-{}.canneslions.com/index.cfm/1-0/registration/debug?" \
      "apikey=AE9CE69D39868DA2FO7H0R4OKI8VUDNEYLMDCDJL&passcode=Hello!Bonjour&" \
      "eventId={}&userPersonId={}"


class APICalls(BasePage):

    def api_remove_all_passes(self, person_id, festival=None, env=None):
        environment = 'uat' if env == 'uat' \
            else 'staging' if env == 'staging' else 'integration'
        event_id = 9 if festival is None else 12 if festival == 'sa' \
            else 14 if festival == 'm2020' else 1
        r = requests.delete(url.format(environment, event_id, person_id))
        if r.status_code == 200:
            print "All old passes deleted"
            return True

from BasePage import BasePage
import time
import pytest

test_env = pytest.config.getoption('LAB')
loc = {
    "add_new_pass": "xpath=//span[contains(@class, 'add-pass__icon']",
    "pass_manager": "xpath=//a[contains(@href, 'pass-manager')]",
    "complete_pass_ss": "xpath=//input["
    "@id='add-pass-uniqueId-D193D2AB-53C6-E811-99AF-22000A4AA935']",
    "pass_picker": "xpath=//a[text()='Buy a Pass']",
    "add_to_basket":
        "xpath=//div//div//div//div[contains(@class,'[ c-categories__panel--"
        "active false]')]//div//div//div[@class='[ c-cannes-buttons ][ u-text"
        "--right ]']//button[2]",
    "add_to_basket3": "xpath=//button[contains(text(),'Add To Basket')]",
    "add_to_basket_2":
        "xpath=//div[contains(@class,'rah-static rah-static--height-auto')]"
        "//div//div[contains(@class,'[ c-categories__"
        "content ]')]//div//button[contains(@type,'button')]",
    "add_digital":
        "xpath=//*[text()='Virtual Access']/..//span[text()='Add to Basket']",
    "compare_passes":
        "xpath=//button[contains(@class, 'c-btn--primary']"
        "//span[text()='Compare Passes']",
    "proceed":
        "xpath=//div[contains(@class, 'c-basket-sidebar')]//button["
        "text()='Proceed']",
    "pending_order": "xpath=//h2[contains(text(),'Pending Order')]",
    "complete_pass": "xpath=//h3[text()='Complete']",
    "digital_pass": "xpath=//h3[text()='Digital Pass']",
    "complete_gold_pass": "xpath=//h4[text()='Complete Gold']",
    "complete_platinum_pass": "xpath=//h4[text()='Complete Platinum']",
    "networking_pass": "xpath=//h3[text()='Networking']",
    "lions_health_pass": "xpath=//h3[text()='Lions Health']",
    "lions_health_student_pass": "xpath=//h4[text()='Lions Health Student']",
    "lions_health_young_lion_pass":
        "xpath=//h4[text()='Lions Health Young Lion']",
    "complete_young_lion_pass": "xpath=//h3[text()='Complete Young Lion']",
    "complete_student_pass": "xpath=//h4[text()='Complete Student']",
    "cmo_prog_pass": "xpath=//h3[text()='CMO Accelerator Programme']",
    "generic_pass": "xpath= //*[text()={}",
    "quantity_input": "xpath=//*[text()='{}']/..//input["
                      "contains(@class, 'sidebar__quantity-input')]",
    "quantity_increase": "",
    "quantity_decrease": "",
    "total_before_vat": "xpath=//span[contains(@class, 'total-price')]",
    "pass_details_generic": "xpath=//a[@href='/attend/buy-a-pass/the-{}-pass]",
    "cancel_pass": "xpath=//button[text()='Cancel']",
    "provide_more_details": "xpath=//button[text()='Provide more details']",
    "press_heading": "xpath=//h4[text()='Press']",
    "list_view_generic_pass": "xpath=//span[text()='{}']",
    "pre_pass_pick":
        "xpath=//h2[text()='{}']/../..//span[text()='Buy this Pass']",
    "pre_pass_pick_2":
        "xpath=//h2[text()='The Digital Pass']/../../..//span["
        "text()='Buy this pass']"

}

passes_dict = {
    'complete': loc["complete_pass"],
    'complete_gold': loc["complete_gold_pass"],
    'complete_platinum': loc["complete_platinum_pass"],
    'networking': loc["networking_pass"],
    'lions_health': loc["lions_health_pass"],
    'lions_health_student': loc["lions_health_student_pass"],
    'lions_health_young_lion': loc["lions_health_young_lion_pass"],
    'complete_young_lion': loc["complete_young_lion_pass"],
    'complete_student': loc["complete_student_pass"],
    'cmo_pass': loc["cmo_prog_pass"],
    'digital': loc["digital_pass"],

}

all_passes = [loc["complete_pass"], loc["complete_gold_pass"],
              loc["complete_platinum_pass"], loc["networking_pass"],
              loc["lions_health_pass"],
              loc["lions_health_student_pass"],
              loc["lions_health_young_lion_pass"],
              loc["complete_young_lion_pass"], loc["complete_student_pass"],
              loc["cmo_prog_pass"],
              loc["digital_pass"]
              ]

passes_mapping = {
    'complete': 'Complete',
    'complete_gold': 'Complete Gold',
    'complete_platinum': 'Complete Platinum',
    'networking': 'Networking',
    'lions_health': 'Lions Health',
    'lions_health_student': 'Lions Health Student',
    'lions_health_young_lion': 'Lions Health Young',
    'complete_young_lion': 'Complete Young Lion',
    'complete_student': 'Complete Student',
    'cmo_pass': 'CMO Accelerator Programme',
    'digital': 'Digital Pass',
}

pass_prices = {
    'complete': 3365,
    'complete_gold': 4855,
    'complete_platinum': 8499,
    'networking': 2195,
    'lions_health': 1725,
    'lions_health_student': 445,
    'lions_health_young_lion': 945,
    'complete_young_lion': 1845,
    'complete_student': 845,
    'cmo_pass': 3995,
    'young_competitor': 1425,
    'companion': 1285,
    'replace': 150,
    'digital': 195,
    'complete_+_companion': 4650,
    'complete_gold_+_companion': 6140,
    'complete_platinum_+_companion': 8499,
    'cmo_accelerator_programme_+_companion': 5280,
    'complete_young_lion_+_companion': 2500,
    'lions_health_+_companion': 2490,
}

companion_prices = {
    'complete_young_lion': 655,
    'lions_health': 545,
    'lions_health_student': 235,
    'complete_student': 445,
    'lions_health_young_lion': 445
}

special_passes_url = {
    'representative': 'representatives',
    'roger_academy': 'representatives',
    'lions_competitor': 'representatives'

}

pre_pass_picker = {
    'complete': 'The Complete Pass',
    'complete_gold': 'The Complete Gold Pass',
    'complete_platinum': 'The Complete Platinum Pass',
    'complete_student': 'The Complete Student Pass',
    'complete_young_lion': 'The Complete Young Lions Pass',
    'lions_health': 'The Lions Health Pass',
    'student_health': 'The Student Health Pass',
    'young_lions_health': 'The Young Lions Health Pass',
    'digital': 'The Digital Pass',
    'cmo_pass': 'The CMO Accelerator Pass',
    'networking': 'The Networking Pass'

}


class PassPage(BasePage):

    def add_single_pass(self, pass_type, quantity=1, upgrade=None):
        self.press(loc["pass_picker"])
        if test_env == 'staging':
            if pass_type in pre_pass_picker.iterkeys():
                if pass_type != 'digital' and upgrade is None:
                    self.press(loc["pre_pass_pick"].format(
                        pre_pass_picker[pass_type]))
                elif pass_type == 'digital' and upgrade is None:
                    self.press(loc["pre_pass_pick_2"])
                else:
                    self.press(loc["pre_pass_pick"].format(
                        pre_pass_picker[upgrade]))
            else:
                print "Pass not found check keys and try again"
        self.wait_until_appear(loc["complete_pass"])
        assert self.is_element_visible(loc["complete_pass"])
        if test_env != 'staging':
            if pass_type in passes_dict.keys():
                self.press(passes_dict[pass_type])
            else:
                print "Type of pass not in Dict Check the Passed data"
            self.wait_in_seconds(1)
            if upgrade:
                self.press(passes_dict[upgrade])
            if pass_type == "cmo_pass":
                self.press(loc["add_to_basket_2"])
            elif pass_type == 'digital':
                self.press(loc["add_digital"])
                self.scroll_to_top()
            else:
                #self.wait_until_appear(loc["add_to_basket"], 30)
                self.press(loc["add_to_basket"])
        self.wait_in_seconds(2)
        total_price_before_vat = 0
        if upgrade:
            current_quantity = self.find_by_locator(
                loc["quantity_input"].format(passes_mapping[upgrade]))
            total_price_before_vat = self.check_prices(upgrade, quantity)
        else:
            current_quantity = self.find_by_locator(
                loc["quantity_input"].format(passes_mapping[pass_type]))
            total_price_before_vat = self.check_prices(pass_type, quantity)
        qa = current_quantity.get_attribute('value')

        if quantity > 1:
            self.clear_and_type(loc["quantity_input"].format(
                passes_mapping[pass_type]), quantity)
            updated_quantity = current_quantity.get_attribute('value')
            assert quantity == int(updated_quantity)
        #total_price_before_vat = self.check_prices(pass_type, quantity)
        total = self.find_by_locator(loc["total_before_vat"]).text
        total = total.replace(',', '')

        assert int(total) == int(total_price_before_vat)
        self.press(loc["proceed"])
        self.wait_until_appear(loc["pending_order"])
        assert self.is_element_visible(loc["pending_order"])
        assert "pass-manager" in self.driver.current_url
        return True, total_price_before_vat

    def check_prices(self, pass_type, quantity):
        total_before_vat = pass_prices[pass_type] * quantity
        return total_before_vat

    def add_pass_from_list_view(self, data, pass_type=None):
        if pass_type == 'press':
            self.get_web_page(data['url'] + 'press')
            return True
        elif pass_type == 'contractor':
            self.get_web_page(data['url'] + 'contractors')
            self.wait_until_appear(loc["add_to_basket3"])
            self.wait_in_seconds(3)
            self.press(loc["add_to_basket3"])
            self.wait_until_appear(loc["pending_order"])
            assert "pass-manager" in self.driver.current_url
            return True
        self.get_web_page(data['url'] + special_passes_url['representative'])
        self.wait_in_seconds(2)
        self.wait_until_appear(loc["add_to_basket3"])
        if pass_type == 'festival_rep':
            self.wait_in_seconds(2)
            self.press(loc["list_view_generic_pass"].format(
                'Festival Representative'))
        elif pass_type == 'roger_hatchuel':
            self.wait_in_seconds(2)
            self.press(loc["list_view_generic_pass"].format(
                'Roger Hatchuel Academy'))
        elif pass_type == 'young_lion_competitor':
            self.wait_in_seconds(2)
            self.press(loc["list_view_generic_pass"].format(
                'Young Lion Competitor'))
        self.wait_in_seconds(2)
        self.press(loc["add_to_basket3"])
        self.wait_until_appear(loc["pending_order"])
        assert "pass-manager" in self.driver.current_url
        return True

    def check_pass_details(self, pass_type):
        # this method is to check the details in a new tab
        pass_format = pass_type.replace('_', '-')
        self.press(loc["pass_picker"])
        self.wait_until_appear(loc["complete_pass"])
        if pass_type in passes_dict.keys():
            self.press(loc["pass_details_generic"].format(pass_format))
        else:
            print "Type of pass not in Dict Check the Passed data"
        assert pass_format in self.current_url()

    def add_multiple_passes(self, passes, data=None, upgrade=None):
        self.press(loc["pass_picker"])
        for a_pass in passes:
            if a_pass in passes_dict.keys():
                self.press(passes_dict[a_pass])
            if upgrade:
                self.press(data['upgrade_type'])
            if a_pass == 'cmo_pass':
                self.press(loc["add_to_basket_2"])
            elif a_pass == 'digital':
                self.press(loc["add_digital"])
                self.scroll_to_top()
            else:
                self.press(loc["add_to_basket"])
        time.sleep(3)
        total = self.find_by_locator(loc["total_before_vat"]).text
        total = total.replace(',', '')
        self.press(loc["proceed"])
        self.wait_until_appear(loc["pending_order"])
        assert "pass-manager" in self.driver.current_url
        print total
        return True, int(total)

    def check_every_pass(self):
        self.press(loc["pass_picker"])
        for passes in all_passes:
            self.press(passes)
            time.sleep(1)

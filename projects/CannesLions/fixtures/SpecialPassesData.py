import os

user = os.getenv('username')
fpath = 'C://Users/{}/automation_framework\projects\CannesLions\\fixtures\C' \
       'apture1.PNG'.format(user)
media_data = {
    'web_address': 'www.google.com',
    'page_impressions': 'Under 50K',
    'unique_users': 'Between 50K and 100K',
    'file_name': 'Capture1.PNG',
    'id_path': fpath
}

ylc_data = {
    'competition': 'Digital Competition',
    'title': 'Dr',
    'first_name': 'Robert',
    'last_name': 'Downey',
    'job_title': 'Iron Man',
    'email': 'robert.downey@ironman.com',
    'company': 'Test Awesome Company',
    'address1': '55 Star Wars Road',
    'address2': 'Boba Fett Building',
    'city': 'London',
    'postcode': 'UHO1 UH2',
    'country': 'United Kingdom',
    'comp_activity': 'Media Owner',
    'company_type': 'Entertainment',
    'mobile': '23456789',
    'dob': '05/08/1996',
    'path': fpath
}

student_data = {
    'title': 'Dr',
    'first_name': 'Robert',
    'last_name': 'Downey',
    'job_title': 'Iron Man',
    'email': 'robert.downey@ironman.com',
    'university': 'Start Uni',
    'address1': 'Stark Road',
    'address2': 'Boba Fett Building',
    'city': 'London',
    'country': 'United Kingdom',
    'postcode': 'RO1 CH2',
    'mobile': '23456789',
    'dob': '05/08/1996',
    'path': fpath
}
import datetime
import random
cred_card_visa = {
    "name_on_card": "Luke Skywalker",
    "credit_card_number": "4111110000000211",
    "expiry_date": "10/21",
    "cvv": "123",
    "pin": "sty"
}

cred_card_mastercard= {
    "name_on_card": "Han Solo",
    "credit_card_number": "5100000000000511",
    "expiry_date": "11/20",
    "cvv": "123",
    "pin": "sty"
}

cred_card_amex = {
    "name_on_card": "Qui-Gon Jinn",
    "credit_card_number": "340000000000611",
    "expiry_date": "09/20",
    "cvv": "1234",
    "pin": "sty"
}

awards_cc_visa = {
    "name_on_card": "Luke Skywalker",
    "cc_number": "4111110000000211",
    "expiry_month": 10,
    "expiry_year": 2021,
    "cvv_1": 1,
    "cvv_2": 2,
    "cvv_3": 3,
    "pin": "sty"
}

awards_cc_amex = {
    "name_on_card": "Qui-Gon Jinn",
    "credit_card_number": "340000000000611",
    "expiry_month": "09",
    "expiry_year": 2020,
    "cvv_1": "1",
    "cvv_2": "2",
    "cvv_3": "3",
    "cvv_4": "4",
    "pin": "sty"
}

current_date = datetime.date.today().strftime("%d-%B-%Y")
current_date = current_date.split('-')
payment_options = ['bank_transfer', 'visa', 'amex', 'mastercard']
if current_date[0] >= 17 and current_date[1] == 'June':
    payment_options = ['visa', 'amex', 'mastercard']

payment_option = random.SystemRandom().choice(payment_options)
data = cred_card_visa if payment_option == 'visa' else \
    cred_card_mastercard if payment_option == 'mastercard' else \
    cred_card_amex if payment_option == 'amex' else None


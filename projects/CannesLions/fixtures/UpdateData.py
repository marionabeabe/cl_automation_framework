import strgen
import os

user = os.getenv('username')
random_string = str(strgen.StringGenerator("[\d\w]{6}").render())

update_pass_data = {
    'job_title': 'Product Owner',
    'email': 'test_automation_' + random_string + '@13.com',
    'company_name': 'Abacus Company',
    'address_1': '55 Star Wars Road',
    'address_2': 'Boba Fett Building',
    'city': 'London',
    'postcode': 'RG32 BH5',
    'country': 'United Kingdom',
    'comp_activity': 'Media Owner',
    'company_type': 'Freelance / Individual',
    'mobile': '+449000000001',
}

companion_details = {
    'first_name': 'Darth',
    'last_name': 'Vader'
}

fpath = 'C://Users/{}/automation_framework\projects\CannesLions\\fixtures\C' \
       'apture1.PNG'.format(user)
replace_delegate = {
    'title': 'Mr',
    'first_name': 'Jean-Luc',
    'last_name': 'Pickard',
    'job_title': 'Senior Manager',
    'email': 'jean.luc.pickaed' + random_string + '@startrek.com',
    'company': 'Star Trek Enterprise',
    'address1': '21 Star Trek Colony',
    'address2': 'Spock Building',
    'city': 'London',
    'postcode': 'SHO1 SH2',
    'country': 'United Kingdom',
    'comp_activity': 'Media Owner',
    'company_type': 'Entertainment',
    'mobile': '90909090',
    'path': fpath,
    'dob': '12/01/1990'
}

visa_details = {
    'dob': '10/11/1988',
    'place_of_birth': 'Redruth',
    'nationality': 'British',
    'passport_number': 'HY4567890',
    'place_of_issue': 'London',
    'passport_issue_date': '04/06/2015',
    'passport_expiry_date': '03/06/2025',
    'visa_start_date': '1 June 2019',
    'visa_end_date': '31 July 2019',
    'embarkation': 'United Kingdom'
}
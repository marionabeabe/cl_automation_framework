import strgen
import os

user = os.getenv('username')
random_string = str(strgen.StringGenerator("[\d\w]{6}").render())

company_data = {
    'add_new_company': 'Add new company',
    'company_name': 'Ascential Events',
    'address_1': '33 Kingsway',
    'address_2': 'Strand Underpass',
    'pobox': 'WC2B',
    'city': 'London',
    'postcode': 'WC2B 6UF',
    'country': 'United Kingdom',
    'email': 'abc@bbc.com'
}

entries_company_data = {
    'add_new_company': 'Add new company',
    'company_name': 'Ascential Events',
    'address_1': '33 Kingsway',
    'address_2': 'Strand Underpass',
    'pobox': 'WC2B',
    'city': 'London',
    'postcode': 'WC2B 6UF',
    'country': 'UNITED KINGDOM',
    'email': 'abc@bbc.com',
    'vat_number': 'GB12345678'
}
fpath = 'C://Users/{}/automation_framework\projects\CannesLions\\fixtures\C' \
       'apture1.PNG'.format(user)
someother_delegate = {
    'title': 'Mr',
    'first_name': 'Master',
    'last_name': 'Yoda',
    'job_title': 'Senior Manager',
    'email': 'test_automation@13.com',
    'company': 'Test Awesome Company',
    'address1': '55 Star Wars Road',
    'address2': 'Boba Fett Building',
    'city': 'London',
    'postcode': 'UHO1 UH2',
    'country': 'United Kingdom',
    'comp_activity': 'Media Owner',
    'company_type': 'Entertainment',
    'mobile': '23456789',
    'dob': '12/10/1990',
    'path': fpath
}

contractor_data = {
    'title': 'Mr',
    'first_name': 'Master',
    'last_name': 'Yoda',
    'job_title': 'Senior Manager',
    'email': 'test_automation@13.com',
    'company': 'Test Awesome Company',
    'address1': '55 Star Wars Road',
    'address2': 'Boba Fett Building',
    'city': 'London',
    'postcode': 'UHO1 UH2',
    'country': 'United Kingdom',
    'comp_activity': 'Media Owner',
    'company_type': 'Entertainment',
    'mobile': '23456789',
    'dob': '05/08/1986',
    'nationality': 'American',
    'city_of_birth': 'Washington DC',
    'country_of_birth': 'United States',
}

senior_officer_data = {
    'officer_full_name': 'Captain Jack',
    'officer_position': 'Captain'
}

billing_address_data = {
    'company_name': 'Ebbsfleet Tech',
    'address_1': 'Ebbsfleet Buildings',
    'address_2': 'Ebbsfleet Road',
    'pobox': 'EB10',
    'city': 'Ebbsfleet',
    'postcode': 'EB10 1HS',
    'country': 'UNITED KINGDOM'
}


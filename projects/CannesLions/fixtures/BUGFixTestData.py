awards_and_categories = {
    'award': 'Print & Publishing',
    'category': ['A05 Automotive',
                 ],
    'entries': {
        'Print & Publishing': 'A05 Automotive'
    },
    'selector': 'A05 Automotive'
}

bug_fix_campaign_data = {
    'title': 'Travel to Mars',
    'brand': 'Mars Rover',
    'product': 'MRF-01',
    'sector': 'Travel',
    'sub_sector': 'Transport'
}

new_campaign = {
    'campaign_title': 'Apple Campaign',
    'entry_title': 'Make Apple Innovative'
}


ipl_1 = {
    'title': 'IPL 1',
    'brand': 'CSK',
    'product': 'MSD',
    'sector': 'Travel',
    'sub_sector': 'Transport'
}

ipl_2 = {
    'title': 'IPL 2',
    'brand': 'RCB',
    'product': 'VK',
    'sector': 'Leisure',
    'sub_sector': 'Sports'
}
campaign_data = {
    'title': 'Vat Tests',
    'brand': 'BMW',
    'product': 'ACAS',
    'sector': 'Automotive',
    'sub_sector': 'Vehicles'
}

gb_with_out_vat_company_data = {
    'add_new_company': 'Add new company',
    'company_name': 'Ascential Events',
    'address_1': '33 Kingsway',
    'address_2': 'Strand Underpass',
    'pobox': 'WC2B',
    'city': 'London',
    'postcode': 'WC2B 6UF',
    'country': 'UNITED KINGDOM',
    'email': 'abc@bbc.com'
}

gb_with_vat_company_data = {
    'add_new_company': 'Add new company',
    'company_name': 'Ascential Events',
    'address_1': '33 Kingsway',
    'address_2': 'Strand Underpass',
    'pobox': 'WC2B',
    'city': 'London',
    'postcode': 'WC2B 6UF',
    'country': 'UNITED KINGDOM',
    'email': 'abc@bbc.com',
    'vat_number': 'GB333289454'
}

country_company_data = {
    'add_new_company': 'Add new company',
    'company_name': 'Ascential Events',
    'address_1': '33 Kingsway',
    'address_2': 'Strand Underpass',
    'pobox': 'WC2B',
    'city': 'London',
    'postcode': 'WC2B 6UF',
    'country': 'UNITED KINGDOM',
    'email': 'abc@bbc.com'
}

eu_with_vat_company_data = {
    'add_new_company': 'Add new company',
    'company_name': 'Ascential Events',
    'address_1': '33 Kingsway',
    'address_2': 'Strand Underpass',
    'pobox': 'WC2B',
    'city': 'London',
    'postcode': 'WC2B 6UF',
    'country': 'UNITED KINGDOM',
    'email': 'abc@bbc.com',
    'vat_number': 'FR01353534506'
}

eu_with_out_vat_company_data = {
    'add_new_company': 'Add new company',
    'company_name': 'Ascential Events',
    'address_1': '33 Kingsway',
    'address_2': 'Strand Underpass',
    'pobox': 'WC2B',
    'city': 'London',
    'postcode': 'WC2B 6UF',
    'country': 'UNITED KINGDOM',
    'email': 'abc@bbc.com',
}

us_company_data = {
    'add_new_company': 'Add new company',
    'company_name': 'Ascential Events',
    'address_1': '33 Kingsway',
    'address_2': 'Strand Underpass',
    'pobox': 'WC2B',
    'city': 'London',
    'postcode': 'WC2B 6UF',
    'country': 'UNITED KINGDOM',
    'email': 'abc@bbc.com'
}

japan_company_data = {
    'add_new_company': 'Add new company',
    'company_name': 'Ascential Events',
    'address_1': '33 Kingsway',
    'address_2': 'Strand Underpass',
    'pobox': 'WC2B',
    'city': 'London',
    'postcode': 'WC2B 6UF',
    'country': 'UNITED KINGDOM',
    'email': 'abc@bbc.com',
}

entries_company_data = {
    'add_new_company': 'Add new company',
    'company_name': 'Ascential Events',
    'address_1': '33 Kingsway',
    'address_2': 'Strand Underpass',
    'pobox': 'WC2B',
    'city': 'London',
    'postcode': 'WC2B 6UF',
    'country': 'UNITED KINGDOM',
    'email': 'abc@bbc.com',
    'vat_number': 'GB12345678'
}
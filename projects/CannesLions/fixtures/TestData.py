import os
import datetime
import pytest
import strgen
random_string = str(strgen.StringGenerator("[\d\w]{6}").render())
now = datetime.datetime.now()
formatted_date = now.strftime("%Y-%m-%d-%H-%M")
test_env = pytest.config.getoption('LAB')
#test_env = os.getenv('TEST_ENV', 'uat')
cust = 'customer_uat' if test_env == 'uat' \
    else 'customer_staging' if test_env == 'staging' \
    else 'customer_integration' if test_env == 'integration' \
    else 'customer_prod'

customer = os.getenv('customer', cust)
Environments = {
    "dev": "",
    "uat": "https://cannes-uat.lionsfestivals.com/",
    "staging": "https://cannes-staging.lionsfestivals.com/",
    "prod": "https://www.canneslions.com",
    "integration": "https://cannes-integration.lionsfestivals.com/",
}

Awards_Environments = {
    "dev": "",
    "uat": "https://awards-uat.lionsfestivals.com",
    "staging": "http://awards-staging.canneslions.com",
    "prod": "https://www.canneslions.com",
    "integration": "https://awards-integration.lionsfestivals.com",
}

Accounts = {
    "admin_uat": {
        "username": "admin@test.com",
        "password": "Password007",
        "person_id": ""
    },
    "customer_uat": {
        "username":
            "new_cust_tests_CL_Automation_2019-04-08-09-39JZdg5r@cl.com",
        "password": "Automation123",
        "person_id": "7df5b3e0-d959-e911-80e2-005056ad6a25"
    },
    "customer_staging": {
        "username":
            "new_cust_tests_CL_Automation_2019-05-22-10-01q5svTh@cl.com",
        "password": "Automation123",
        "person_id": ""
    },
    "customer_integration": {
        "username":
            "new_cust_tests_CL_Automation_2019-05-28-14-117zTQMv@cl.com",
        "password": "Automation123",
        "person_id": ""
    },
    "customer_prod": {
        "username":
            "change",
        "password": "Automation123",
        "person_id": ""
    }
    }

test_data = {
    "url": Environments[test_env],
    "username": Accounts[customer]['username'],
    "password": Accounts[customer]['password'],
    "person_id": Accounts[customer]['person_id'],
    "env": test_env,
    "title": "Mr",
    "comp_activity": "Production",
    "country": "UNITED KINGDOM",
    "job_role": "Technology",
    "company_type": "Photography"
}

delegate_data = {
    "url": Environments[test_env],
    "title": "Mr",
    "first_name": "Luke",
    "last_name": "Skywalker",
    "contact": "Luke Skywalker",
    "email": "CL_Automation_" + formatted_date + random_string + "@cl.com",
    "password": "Automation123",
    "job_title": "Product Manager",
    "mobile": "+44327442878",
    "company_name": "Awesome Company",
    "address1": "Awesome Building",
    "address2": "Awesome Road",
    "city": "London",
    "postcode": "WC23 1UHO",
    "country": "United Kingdom",
    "company_role": "Middle Manager",
    "company_activity": "Production",
    "job_role": "Middle Manager",
    "company_type": "Photography",
    "new_first_name": "Jonty",
    "new_last_name": "Rhodes",
    "new_email": "jonty.rhodes@abc.com",

}

entries_data = {
    "url": Awards_Environments[test_env],
    "title": "Mr",
    "first_name": "Luke",
    "last_name": "Skywalker",
    "contact": "Luke Skywalker",
    "email": "CL_Automation_" + formatted_date + random_string + "@cl.com",
    "password": "Automation123",
    "job_title": "Product Manager",
    "mobile": "+44327442878",
    "company_name": "Awesome Company",
    "address1": "Awesome Building",
    "address2": "Awesome Road",
    "city": "London",
    "postcode": "WC23 1UHO",
    "country": "United Kingdom",
    "company_role": "Middle Manager",
    "company_activity": "Production",
    "company_type": "Photography",
    "job_role": "Middle Manager",
    "country2": "UNITED KINGDOM",
    "comp_activity": "Production",
    "new_first_name": "Jonty",
    "new_last_name": "Rhodes",
    "new_email": "jonty.rhodes@abc.com",

}

update_delegate_data = {
    "url": Environments[test_env],
    "title": "Prof",
    "first_name": "Vin",
    "last_name": "Diesel",
    'password': "Automation123",
    "job_title": "Fast and Furious Driver",
    "mobile": "+44327442878",
    "company_name": "Furious Company",
    "address1": "Fast Building",
    "address2": "Fast Road",
    "city": "Manchester",
    "postcode": "FAST11 1UHO",
    "country": "United Kingdom",
    "company_role": "Middle Manager",
    "company_activity": "Production",
    "company_type": "Photography",
    "new_password": "Automation124",
    "marketing": {"cl": "checked", "dl":"unchecked"}

}

upgrade_pass_data = {
    "url": Environments[test_env],
    "title": "Mr",
    "first_name": "Luke",
    "last_name": "Skywalker",
    "email": "test_automation_" + formatted_date + "@cl.com",
    "password": "Automation123",
    "job_title": "Product Manager",
    "mobile": "+44327442878",
    "company_name": "Awesome Company",
    "address1": "Awesome Building",
    "address2": "Awesome Road",
    "city": "London",
    "postcode": "WC23 1UHO",
    "country": "United Kingdom",
    "company_role": "Middle Manager",
    "company_activity": "Production",
    "company_type": "Photography"
}

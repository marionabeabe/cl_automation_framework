gb_with_out_tin = {
    'add_new_company': 'Add new company',
    'company_name': 'Ascential Events',
    'address_1': '33 Kingsway',
    'address_2': 'Strand Underpass',
    'pobox': 'WC2B',
    'city': 'London',
    'postcode': 'WC2B 6UF',
    'country': 'United Kingdom',
    'email': 'abc@bbc.com'
}

gb_with_tin = {
    'add_new_company': 'Add new company',
    'company_name': 'Ascential Events',
    'address_1': '33 Kingsway',
    'address_2': 'Strand Underpass',
    'pobox': 'WC2B',
    'city': 'London',
    'postcode': 'WC2B 6UF',
    'country': 'United Kingdom',
    'email': 'abc@bbc.com',
    'tax_number': 'GB333289454'
}

france_with_tin = {
    'add_new_company': 'Add new company',
    'company_name': 'Ascential Events',
    'address_1': '33 Kingsway',
    'address_2': 'Strand Underpass',
    'pobox': 'WC2B',
    'city': 'London',
    'postcode': 'WC2B 6UF',
    'country': 'France',
    'email': 'abc@bbc.com',
    'tax_number': 'FR01353534506'
}

france_with_out_tin = {
    'add_new_company': 'Add new company',
    'company_name': 'Ascential Events',
    'address_1': '33 Kingsway',
    'address_2': 'Strand Underpass',
    'pobox': 'WC2B',
    'city': 'London',
    'postcode': 'WC2B 6UF',
    'country': 'France',
    'email': 'abc@bbc.com'
}

russia_with_out_tin = {
    'add_new_company': 'Add new company',
    'company_name': 'Ascential Events',
    'address_1': '33 Kingsway',
    'address_2': 'Strand Underpass',
    'pobox': 'WC2B',
    'city': 'London',
    'postcode': 'WC2B 6UF',
    'country': 'Russian Federation',
    'email': 'abc@bbc.com'
}

us_with_out_tin = {
    'add_new_company': 'Add new company',
    'company_name': 'Ascential Events',
    'address_1': '33 Kingsway',
    'address_2': 'Strand Underpass',
    'pobox': 'WC2B',
    'city': 'London',
    'postcode': 'WC2B 6UF',
    'country': 'United States',
    'email': 'abc@bbc.com'
}
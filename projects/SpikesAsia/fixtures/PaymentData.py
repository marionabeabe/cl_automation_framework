cred_card_visa = {
    "name_on_card": "Luke Skywalker",
    "credit_card_number": "4444333322221111",
    "expiry_month": "10",
    "expiry_year": "2020",
    "cvv": "737",
    "pin": "sty"
}

cred_card_mastercard = {
    "name_on_card": "Han Solo",
    "credit_card_number": "5555444433331111",
    "expiry_month": "10",
    "expiry_year": "2020",
    "cvv": "737",
    "pin": "sty"
}

cred_card_amex = {
    "name_on_card": "Qui-Gon Jinn",
    "credit_card_number": "370000000000002",
    "expiry_month": "10",
    "expiry_year": "2020",
    "cvv": "7373",
    "pin": "sty"
}

awards_cc_visa = {
    "name_on_card": "Luke Skywalker",
    "cc_number": "4444333322221111",
    "expiry_month": 10,
    "expiry_year": 2020,
    "cvv_1": 7,
    "cvv_2": 3,
    "cvv_3": 7,
    "pin": "sty"
}

awards_cc_mc = {
    "name_on_card": "Luke Skywalker",
    "cc_number": "5555444433331111",
    "expiry_month": 10,
    "expiry_year": 2020,
    "cvv_1": 7,
    "cvv_2": 3,
    "cvv_3": 7,
    "pin": "sty"
}

awards_cc_amex = {
    "name_on_card": "Qui-Gon Jinn",
    "credit_card_number": "370000000000002",
    "expiry_month": "10",
    "expiry_year": 2020,
    "cvv_1": "7",
    "cvv_2": "3",
    "cvv_3": "7",
    "cvv_4": "3",
    "pin": "sty"
}
# -*- coding: utf-8 -*-

awards_data = {
    'award': 'Print & Publishing',
    'category': ' Retail',
    'entries': {
        'Design': ['A06 Retail']
    },
    'selector': 'A06 Retail'
}

two_awards_seven_categories = {
    'award': ['Design', 'Direct'],
    'category': ['A01 Creation of a new Brand Identity',
                 'B04 Posters', 'D04 Live Events',
                 'A03 Healthcare', 'B01 Mailing',
                 'C02 Use of Mobile'
                 ],
    'entries': {
        'Design': ['A01 Creation of a new Brand Identity',
                   'B04 Posters', 'D04 Live Events'],
        'Direct': ['A03 Healthcare', 'B01 Mailing',
                   'C02 Use of Mobile'],

    }
}

six_awards_1 = {
    'award': [
        'Brand Experience & Activation',
        'Creative eCommerce',
        'Digital',
        'Film Craft',
        'Innovation',
    ],
    'category': [
            'A01 Creation of a New Brand Identity',
            'A02 Rebrand / Refresh of an Existing Brand',
            'A03 Design-driven Effectiveness',
            'B01 Posters',
    ],
    'entries': {
        'Brand Experience & Activation': [
            # max 1 from A anf total of  3
            'A07 Travel / Leisure',
            'B03 Use of Broadcast',
            'C05 Customer Retail / In-Store Experience',
            # 'D05 Digital Installations',
        ],
        'Creative eCommerce': [
            # max 1 from A and max total of 3
            'A01 Food / Drink',
            # 'A02 Other FMCG',
            # 'A03 Healthcare',
            # 'A04 Consumer Durables',
            # 'A05 Automotive',
            # 'A06 Retail',
            # 'A07 Travel',
            # 'A08 Leisure',
            # 'A09 Media / Entertainment',
            # 'A10 Consumer Services / Business to Business',
            # 'A11 Not-for-profit / charity / government',
            # 'A12 Corporate Social Responsibility (CSR) / Corporate Image',
            'B03 Payment Solutions',
            'B04 Personalised Campaigns',
            # 'B05 Predictive Data Targeting'
        ],

        'Digital': [
            # max 1 from A
            'A01 Food / Drink',
            # 'A02 Other FMCG',
            # 'A03 Healthcare',
            # 'A04 Consumer Durables',
            # 'A05 Automotive',
            # 'A06 Retail',
            # 'A07 Travel / Leisure',
            # 'A08 Media / Entertainment',
            # 'A09 Consumer Services / Business to Business',
            # 'A10 Not-for-profit / Charity / Government',
            # 'A11 Corporate Social Responsibility (CSR) / Corporate Image',
            'B01 Websites',
            'B02 Microsites',
            'B03 Web Service / App',
            'C01 Online Ad',
            'D01 Social Video',
            # 'D02 Interactive Video',
            'D03 Webisodes / Series',
            'D04 Brand / Product Video',
            'E01 Social Business & Commerce',
            'E02 Social Purpose',
            'E03 Innovative Use of Social or Community',
            'E04 Real-time Response',
            'E05 Influencer / Talent',
            'E06 Co-Creation & User Generated Content',
            'E07 Content Placement',
            'E08 Community Building & Management',
            'E09 Social Data & Insight',
            'E10 Emerging Platforms',
            'F01 Branded Games',
            'G01 Tangible Tech',
            'G02 Spatial Tech',
            'G03 Digital Billboard',
            'H01 Integrated Multi-Platform Campaign (Online & Offline)'
        ],
        'Film Craft': [
            'A01 Direction',
            'A02 Script',
            'A03 Casting',
            'A04 Production Design / Art Direction',
            'A05 Cinematography',
            'A06 Editing',
            'A07 Use of Original Music',
            'A08 Use of Licensed or Adapted Music',
            'A09 Sound Design',
            'A10 Animation',
            'A11 Visual Effects',
            'A12 Innovation in Production'
        ],
        'Innovation': [
            # only 1
            'A01 Innovative Technology',
            # 'A02 Applied Innovation',
            # 'A03 Scalable Innovation',
            # 'A04 Early Stage Technology',
            # 'A05 Business Transformation',
            # 'A06 Product Innovation',
        ],

    }
}

six_awards_2 = {
    'award': [
        'Direct',
        'Design',
        'Entertainment',
        'Glass: The Award For Change'
    ],
    'category': [],
    'entries': {
        'Direct': [
            # max 1 from A total of 3
            'A01 Food / Drink',
            # 'A02 Other FMCG',
            # 'A03 Healthcare',
            # 'A04 Consumer Durables',
            # 'A05 Automotive',
            # 'A06 Retail',
            # 'A07 Travel',
            # 'A08 Leisure',
            # 'A09 Media / Entertainment',
            # 'A10 Consumer Services / Business to Business',
            # 'A11 Not-for-profit / charity / government',
            # 'A12 Corporate Social Responsibility (CSR) / Corporate Image',
            # 'B01 Mailing',
            # 'B02 Use of Ambient Media: Small Scale',
            # 'B03 Use of Ambient Media: Large Scale',
            # 'B04 Use of Broadcast',
            # 'B05 Use of Print / Outdoor',
            'C01 Use of Digital Platforms',
            'D01 Data Strategy',
            # 'C02 Data-driven Targeting',
            # 'C03 Use of Real-time Data',
            # 'C04 New Realities & Voice-activation',
            # 'C05 Digital Installations & Interactive Screens',
            # 'C06 Use of Other Technology',

            # 'D02 Use of Mobile',
            # 'D03 Use of Social Platforms',
            # 'D04 Teal-time Response',
            # 'D05 Co-creation & User Generated Content',
            # 'E01 Acquisition & Retention',
            # 'E02 Art Direction / Design',
            # 'E03 Experience Design',
            # 'E04 Launch / Re-launch',
            # 'E05 Personalised Campaigns',
            # 'F01 Local Brand',
            # 'F02 Challenger Brand',
            # 'F03 Single-market Campaign',
            # 'F04 Social Behaviour & Cultural Insight',
            # 'F05 Breakthrough on a Budget'
        ],
        'Design': [
            # max 1 from A
            'A01 Creation of a new Brand Identity',
            # 'A02 Rebrand / Refresh of an existing Brand',
            'B01 Publications & Brand Collateral',
            'B02 Promotional Item Design',
            'B03 Self-promotion',
            'B04 Posters',
            'B05 Books',
            'C01 Digital & Interactive Design',
            'D01 Retail Environment & Experience Design',
            'D02 Retail Installation & Experience',
            'D03 Point of Sales, Consumer Touchpoints & In-store Collateral',
            # max 3 from D
            # 'D04 Live Events',
            # 'D05 Spatial & Sculptural Exhibitions and Experiences',
            # 'D06 Wayfinding & Signage',
            # max 1 from E
            'E01 Food / Drink',
            # 'E02 Other FMCG',
            # 'E03 Beauty / Healthcare',
            # 'E04 Consumer Durables',
            'F01 Consumer Products',
            'F02 Environmental / Social Impact',
            'F03 Brand Communication',
            'F04 Innovation & Solution',
            'G01 Typography',
            'G02 Illustration',
            'G03 Logo Design',
            'G04 Sound Design',
            'G05 Motion Graphics Design & Animation',
            'G06 Photography / Curation of Images',
            'G07 Copywriting',

        ],
        'Entertainment': [
            'A01 Fiction & Non-Fiction Film: Up to 5 minutes',
            'A02 Fiction & Non-Fiction Film: 5-30 minutes',
            'A03 Fiction & Non-Fiction Film: Over 30 minutes',
            'A04 New Realities',
            'A05 Audio Content',
            'A06 Co-Creation & User Generated Content',
            'A07 Use of Talent',
            'A08 Live Brand Experience',
            'A09 Branded Games',
            'A10 Use of Digital & Social',
            'A11 Excellence in Brand or Product Integration into '
            'Existing Content',
            'A12 Excellence in Audience Engagement & Distribution Strategy',
            'A13 Excellence in Partnerships',
            'A14 Innovation in Entertainment',
            'B01 Sports: Film, Series & Audio',
            'B02 Sports: Live Experience',
            'B03 Sports: Digital, Social & Emerging Tech',
            'B04 Excellence in Brand Integration & Sponsorship / Partnership',
            'B05 Excellence in Audience Targeting or Distribution Strategy',
            'B06 Sports for Good',
        ],

        # 'Glass: The Award For Change': [
        #     'A01 Glass'
        # ],
    }
}

six_awards_3 = {
    'award': [
        'Digital Craft',
        'Integrated',
        'Media',
        'Mobile',
        'Music'

    ],
    'category': [],
    'entries': {
        'Integrated': [
            'A01 Integrated'
        ],
        'Media': [
            # one from A and max of 3
            'A01 Food / Drink',
            # 'A02 Other FMCG',
            # 'A03 Healthcare',
            # 'A04 Consumer Durables',
            # 'A05 Automotive',
            # 'A06 Retail',
            # 'A07 Travel',
            # 'A08 Leisure',
            # 'A09 Media / Entertainment',
            # 'A10 Consumer Services / Business to Business',
            # 'A11 Not-for-profit / Charity / Government',
            # 'A12 Corporate Social Responsibility (CSR) / Corporate Image',
            'B01 Use of TV & Other Screens',
            # 'B02 Use of Audio Platforms',
            # 'B03 Use of Print / Outdoor',
            # 'B04 Use of Ambient Media: Small Scale',
            # 'B05 Use of Ambient Media: Large Scale',
            # 'B06 Use of Events',
            # 'B07 Use of Stunts',
            # 'B08 Use of Digital Platforms',
            # 'B09 Use of Mobile',
            # 'B10 Use of Social Platforms',
            # 'B11 Use of Technology',
            # 'C01 Use of Data Driven Insight',
            # 'C02 Use of Real-time Data',
            # 'C03 Data-driven Targeting',
            # 'D01 Use of Brand or Product Integration into a Programme or '
            # 'Platform',
            # 'D02 Use of Branded Content Created for Digital or Social',
            # 'E01 Excellence in Media Insights & Strategy',
            # 'E02 Excellence in Media Planning',
            # 'E03 Excellence in Media Execution',
            # 'E04 Use of Integrated Media',
            # 'F01 Local Brand',
            # 'F02 Challenger Brand',
            # 'F03 Single-market Campaign',
            # 'F04 Social Behaviour & Cultural Insight',
            'G01 Local Brand'
            ],
        'Mobile': [
            'A01 Activation by Location',
            'A02 AR',
            'A03 New Realities & Voice Activation',
            'A04 Wearable Technology',
            'A05 Data / Insight',
            'A06 Innovative use of Technology',
            'A07 Use of Advanced Learning Technologies',
            'B01 Mobile Websites',
            'B02 Mobile Apps',
            'B03 Social for Mobile',
            'B04 Mobile Games',
            'B05 Mobile Advertising',
            'C01 Integrated Mobile Campaigns',
            'C02 Messaging Campaign',

        ],
        'Music': [
            'A01 Artist in Partnership with a Brand or a Cause',
            'A02 Use of Social / Digital Platform',
            'A03 Fan Engagement/Community Building',
            'A04 Use of Licensed or Adapted Music',
            'A05 Use of Original Composition',
            'A06 Use of Music Streaming Platform / Video Hosting Service / App',
            'A07 Use of Music Technology or Innovation',
            'A08 Music Live Experience',
            'A09 Excellence in Music Video',
            'A10 Excellence in Audience Engagement & Distribution Strategy',
            'A11 Excellence in Brand / Music Sponsorship or Partnership'
        ],
        'Digital Craft': [
            'A01 UI',
            'A02 Digital Illustration',
            'A03 Video / Moving Image',
            'A04 Digital Image Design',
            'A05 Music / Sound Design',
            'A06 Overall Aesthetic Design',
            'B01 UX & Journey Design',
            'B02 Native & Built-in Feature Integration',
            'B03 Experience Design: Multi-platform',
            'B04 Overall Functional Design',
            'C01 Curation of Data',
            'C02 Data Storytelling',
            'C03 Data Visualisation',
            'D01 New Realities & Voice Activation',
            'D02 Innovative Use of Technology',
            'D03 Technological Achievement in Digital Craft',
            'D04 Advanced Learning Technologies'
        ],


    }
}

six_awards_4 = {
    'award': [
        'Film',
        'Outdoor',
        'PR',
        'Print & Outdoor Craft',
        'Print & Publishing',
        'Radio & Audio'
    ],
    'category': [],
    'entries': {
        'Film': [
            'A05 Automotive',
            'B06 Retail',
            'C01 Viral Film',
            'C02 Branded Content & Entertainment Film',
            'C03 Screens & Events',
            'C04 Micro-film',
            'D01 Use of Film',
            'E01 Local Brand',
            'E02 Challenger Brand',
            'E03 Single Market Campaign',
            'E04 Social Behaviour & Cultural Insight',
            'E05 Breakthrough on a Budget',
        ],
        'Outdoor': [
            # max 1 from A
            'A01 Food / Drink',
            # 'A02 Other FMCG',
            # 'A03 Healthcare',
            # 'A04 Consumer Durables',
            # 'A05 Automotive',
            'B03 Healthcare',
            'C01 Animated Digital Screens',
            'C02 Interactive Digital Screens',
            'C03 Dynamic Digital Screens',
            'D01 Standard Sites',
            'D02 Ambient Outdoor',
            'D03 Technology',
            'E01 Non-Standard Indoor Advertising',
            'E02 Small Scale Special Solutions',
            'E03 Special Build',
            'E04 Live Advertising & Events',
            'E05 Interactive Outdoor Experiences',
            'E06 Transit',
            'F01 Integrated Campaign Led by Outdoor'
        ],
        'PR': [
            # max of 3
            'A01 Food / Drink',
            # 'A02 Other FMCG',
            # 'A03 Healthcare',
            # 'A04 Consumer Durables',
            # 'A05 Automotive',
            # 'A06 Retail',
            # 'A07 Travel',
            'B05 Media Relations',
            'C07 Use of Technology',
            # 'D01 PR Excellence in Effectiveness'
            ],
        'Print & Outdoor Craft': [
            'A01 Art Direction',
            'A02 Copywriting',
            'A03 Illustration',
            'A04 Photography',
            'A05 Typography'
        ],
        'Print & Publishing': [
            # max 1 from A
            'A05 Automotive',
            'B01 Innovative Use of Print',
            'C01 Original Print & Publishing'

        ],
        'Radio & Audio': [
            # max 1 from A
            'A04 Consumer Durables',
            'B01 Use of Radio & Audio as a Medium',
            'B02 Use of Audio Technology',
            'B03 Branded Content / Programming',
            'C01 Use of Music',
            'C02 Sound Design',
            'C03 Script',
            'C04 Casting & Performance',
            'D01 Local Brand',
            'D02 Challenger Brand',
            'D03 Single Market Campaign',
            'D04 Social Behaviour & Cultural Insight',
            'D05 Breakthrough on a Budget',
        ]
    }
}

health_awards = {
    'award': ['Health & Wellness'],
    'category': [],
    'entries': {
        'Health & Wellness': [
            'Brand Experience & Activation',
            'Branded Content & Entertainment: Digital & Social',
            'Branded Content & Entertainment: Film, TV and Online Film Content',
            'Branded Content & Entertainment: Live Experience',
            'Creative Data: Creative Data Enhancement',
            'Creative Data: Use of Real-Time Data',
            'Creative Data: Data Visualisation',
            'Creative Data: Creative Data Collection & Research',
            'Digital Craft: Interface & Navigation (UI)',
            'Digital Craft: User Experience (UX)',
            'Digital: Platforms',
            'Digital: Social',
            'Direct',
            'Film: Cinema, TV and Digital Film Content',
            'Film Craft: Animation & Visual Effects',
            'Film Craft: Production Design / Art Direction',
            'Film Craft: Cinematography',
            'Film Craft: Direction',
            'Film Craft: Script',
            'Film Craft: Use of Music / Sound Design',
            'Integrated Campaign',
            'Mobile',
            'Outdoor',
            'Ambient Outdoor',
            'PR',
            'Print & Publishing',
            'Print Collateral',
            'Industry Craft: Art Direction',
            'Industry Craft: Copywriting',
            'Industry Craft: Illustration',
            'Industry Craft: Photography',
            'Industry Craft: Typography',
            'Radio & Audio',
            'Use of Technology',
            'Product Innovation'
        ]}

}

pharma_awards = {

}


extra_chars_categories = [
    #'A03 Motion Graphics Design & Animation',
    'B02 Use of Mobile',
    'D06 360 Integrated Brand Experience',
    'A04 Collaborative Creative Effectiveness',
    'B01 Local Brand',
    'B02 Challenger Brand',
    'B04 Social Behaviour & Cultural Insight',
    'C03 Retail Promotions & In-store Integration',
    'E04 Business Transformation',
    'F02 Challenger Brand',
    'A03 Consumer Durables',
    'A06 Media/ Entertainment',
    'A07 Consumer Services / Business to Business',
    'A08 Not-for-profit / Charity / Government',
    'C04 Long-term Strategy',
    'D02 Collaboration',
    'E02 Business Transformation',
    'C06 IA Application',
    'D06 IA Application',
    'E03 Other FMCG & Consumer Durables',
    'A11 Not-for-profit / Charity / Government',
    'E02 Art Direction / Design',
    'E03 Experience Design',
    'F01 Local Brand',
    'F02 Challenger Brand',
    'D01 Sports: Content',
    'A05 Gaming',
    'B01 Sports Live Experience',
    'C01 Fan Engagement',
    'C02 Influencer & Co-creation',
    'C03 Social',
    'E02 Excellence in Brand Storytelling',
    'E03 Excellence in Audience Targeting or Distribution Strategy',
    'E08 Excellence by Challenger Brand for Sport',
    'A11 Not-for-profit / Charity/ Government',
    'B11 Not-for-profit / Charity / Government',
    'F02 Challenger Brand',
    'A11 Not-for-profit / Charity / Government',
    'B01 Community Building & Management',
    'B05 Brand Storytelling',
    'B06 Innovative Use of Community',
    'B07 New Realities & Voice-activation',
    'D01 Content Placement',
    'D02 Social Film',
    'D04 Co-Creation & User Generated Content',
    'D05 Social Games',
    'E01 Multi-platform Social Campaign',
    'F02 Challenger Brand',
    'A06 Product Innovation',
    'A11 Not-for-profit / Charity / Government',
    'B03 Use of Print / Outdoor',
    'F02 Challenger Brand',
    'A07 Mobile Payment Solutions',
    'A09 Advanced Learning Technologies / Voice-activation',
    'A11 Not-for-profit / Charity / Government',
    'B11 Not-for-profit / Charity / Government',
    'D02 Mass Distributed Promotional Items & Printed Media',
    'D03 Limited Run Promotional Items & Printed Media',
    'A11 Not-for-profit / Charity / Government',
    'B02 Publications for Good for Charity / Not-for-profit',
    'A11 Not-for-profit / Charity / Government',
    'E07 Multi-market Campaign',
    'F01 Local Brand',
    'F02 Challenger Brand',
    'A11 Not-for-profit / Charity / Government',
    'C02 Use of Audio Technology / Voice -Activation',
    'C04 Native Advertising',
]


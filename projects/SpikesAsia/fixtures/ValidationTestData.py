import datetime
import strgen
import pytest

random_string = str(strgen.StringGenerator("[\d\w]{6}").render())


now = datetime.datetime.now()
formatted_date = now.strftime("%Y-%m-%d-%H-%M")
test_env = pytest.config.getoption('LAB')
empty_credentials = {
    'url': 'https://spikesasia-{}.lionsfestivals.com/'.format(test_env),
    'username': ' ',
    'password': ' '
}

invalid_credentials = {
        'url': 'https://spikesasia-{}.lionsfestivals.com/'.format(test_env),
        "username": "invalid_test_automation_2018-11-12-12-39@cl.com",
        "password": "Automation123invalid"
    }

create_account_data = {
    "url": 'https://spikesasia-{}.lionsfestivals.com/'.format(test_env),
    "title": "Mr",
    "first_name": "Luke",
    "last_name": "Skywalker",
    "email": "test_automation_" + formatted_date + random_string + "@sa.com",
    "password": "Automation123",
    "job_title": "Product Manager",
    "mobile": "+44327442878",
    "company_name": "Awesome Company",
    "address1": "Awesome Building",
    "address2": "Awesome Road",
    "city": "London",
    "postcode": "WC23 1UHO",
    "country": "United Kingdom",
    "company_role": "Middle Manager",
    "job_role": "Middle Manager",
    "company_activity": "Production",
    "company_type": "Photography"
}

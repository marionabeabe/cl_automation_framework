import os
import datetime
import pytest
import strgen

random_string = str(strgen.StringGenerator("[\d\w]{6}").render())
user = os.getenv('username')
now = datetime.datetime.now()
formatted_date = now.strftime("%Y-%m-%d-%H-%M-%S")
test_env = pytest.config.getoption('LAB')
cust = 'customer_uat' if test_env == 'uat' \
    else 'customer_staging' if test_env == 'staging' \
    else 'customer_integration' if test_env == 'integration' \
    else 'customer_prod'

customer = os.getenv('customer', cust)
Environments = {
    "dev": "",
    "uat": "https://spikesasia-uat.lionsfestivals.com/",
    "staging": "https://spikesasia-staging.lionsfestivals.com/",
    "prod": "https://www.spikesasia.com",
    "integration": "https://spikesasia-integration.lionsfestivals.com/"
}

Awards_Environments = {
    "dev": "",
    "uat": "https://awards-uat.spikes.asia",
    "staging": "https://awards-staging.spikes.asia",
    "prod": "https://www.spikesasia.com",
    "integration": "https://awards-integration.spikes.asia",
}

Accounts = {
    "admin_uat": {
        "username": "admin@test.com",
        "password": "Password007",
        "person_id": ""
    },
    "customer_uat": {
        "username": "new_cust_tests_SA_Automation_2019-03-13-14-10@sa.com",
        "password": "Automation123",
        "person_id": "10B0D0D5-9945-E911-80E1-005056AD6A25"
    },
    "customer_staging": {
        "username":
            "new_cust_tests_Automation_2019-05-20-16-06-16HfjCJ9@sa.com",
        "password": "Automation123",
        "person_id": "095733dd-107b-e911-80ec-005056ad86db"
    },
    "customer_integration": {
        "username":
            "new_cust_tests_Automation_2019-05-28-16-44-52uz1H_T@sa.com",
        "password": "Automation123",
        "person_id": ""
    },
    "customer_prod": {
        "username":
            "change",
        "password": "Automation123",
        "person_id": ""
    }
}

test_data = {
    "url": Environments[test_env],
    "username": Accounts[customer]['username'],
    "password": Accounts[customer]['password'],
    "person_id": Accounts[customer]['person_id'],
    "env": test_env,
    "title": "Mr",
    "comp_activity": "Production",
    "country": "UNITED KINGDOM",
    "job_role": "Technology",
    "company_type": "Photography"

}

delegate_data = {
    "url": Environments[test_env],
    "title": "Mr",
    "first_name": "John",
    "last_name": "Pickard",
    "email": "_Automation_" + formatted_date + random_string + "@sa.com",
    "password": "Automation123",
    "job_title": "Product Manager",
    "mobile": "+44327442878",
    "company_name": "SA Enterprise Company",
    "address1": "Spikes Building",
    "address2": "SpikesRoad",
    "city": "London",
    "postcode": "WC23 1UHO",
    "country": "United Kingdom",
    "company_role": "Middle Manager",
    "company_activity": "Production",
    "job_role": "Middle Manager",
    "company_type": "Photography"

}

entries_data = {
    "url": Awards_Environments[test_env],
    "title": "Mr",
    "first_name": "Luke",
    "last_name": "Skywalker",
    "email": "_Automation_" + formatted_date + random_string + "@sa.com",
    "password": "Automation123",
    "job_title": "Product Manager",
    "mobile": "+44327442878",
    "company_name": "Awesome Company",
    "address1": "Awesome Building",
    "address2": "Awesome Road",
    "city": "London",
    "postcode": "WC23 1UHO",
    "country": "United Kingdom",
    "company_role": "Middle Manager",
    "company_activity": "Production",
    "company_type": "Photography",
    "job_role": "Middle Manager",
    "country2": "UNITED KINGDOM",
    "comp_activity": "Production",

}

company_data = {
    'add_new_company': 'Add new company',
    'company_name': 'Ascential Events',
    'address_1': '33 Kingsway',
    'address_2': 'Strand Underpass',
    'pobox': 'WC2B',
    'city': 'London',
    'postcode': 'WC2B 6UF',
    'country': 'United Kingdom',
    'email': 'abc@bbc.com'
}

entries_company_data = {
    'add_new_company': 'Add new company',
    'company_name': 'Ascential Events',
    'address_1': '33 Kingsway',
    'address_2': 'Strand Underpass',
    'pobox': 'WC2B',
    'city': 'London',
    'postcode': 'WC2B 6UF',
    'country': 'UNITED KINGDOM',
    'email': 'abc@bbc.com',
    'vat_number': 'GB12345678'
}
fpath = 'C://Users/{}/automation_framework\projects\CannesLions\\fixtures\C' \
       'apture1.PNG'.format(user)

someother_delegate = {
    'title': 'Mr',
    'first_name': 'Master',
    'last_name': 'Yoda',
    'job_title': 'Senior Manager',
    'email': 'test_automation@13.com',
    'company': 'Test Awesome Company',
    'address1': '55 Star Wars Road',
    'address2': 'Boba Fett Building',
    'city': 'London',
    'postcode': 'UHO1 UH2',
    'country': 'United Kingdom',
    'comp_activity': 'Media Owner',
    'company_type': 'Entertainment',
    'mobile': '23456789',
    'dob': '12/10/1995',
    'path': fpath,
    'college_name': 'University of Singapore'
}

update_delegate_data = {
    "url": Environments[test_env],
    "title": "Prof",
    "first_name": "Vin",
    "last_name": "Diesel",
    'password': "Automation123",
    "job_title": "Fast and Furious Driver",
    "mobile": "+44327442878",
    "company_name": "Furious Company",
    "address1": "Fast Building",
    "address2": "Fast Road",
    "city": "Manchester",
    "postcode": "FAST11 1UHO",
    "country": "United Kingdom",
    "company_role": "Middle Manager",
    "company_activity": "Production",
    "company_type": "Photography",
    "new_password": "Automation124",
    "marketing": {"cl": "checked", "dl":"unchecked"}

}

media_data = {
    'web_address': 'www.google.com',
    'page_impressions': 'Under 50K',
    'unique_users': 'Between 50K and 100K',
    'file_name': 'Capture1.PNG',
    'id_path': fpath
}

ysc_data = {
    'competition': 'Digital',
    'title': 'Dr',
    'first_name': 'Robert',
    'last_name': 'Downey',
    'job_title': 'Iron Man',
    'email': 'robert.downey@ironman.com',
    'company': 'Test Awesome Company',
    'address1': '55 Star Wars Road',
    'address2': 'Boba Fett Building',
    'city': 'London',
    'postcode': 'UHO1 UH2',
    'country': 'United Kingdom',
    'comp_activity': 'Media Owner',
    'company_type': 'Entertainment',
    'mobile': '23456789',
    'dob': '05/08/1996',
    'path': fpath
}
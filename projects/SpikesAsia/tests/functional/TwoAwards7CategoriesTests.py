import platform

from utils.TestUtils import Expects
from TestDriver import TestDriver


from projects.CannesLions.pages.OrderPage import OrderPage
from projects.CannesLions.fixtures.OrderData import \
    senior_officer_data, billing_address_data, entries_company_data
from projects.SpikesAsia.fixtures.AwardsCategoriesData import \
    two_awards_seven_categories
from projects.SpikesAsia.fixtures.PaymentData import awards_cc_visa
from projects.SpikesAsia.fixtures.TestData import entries_data
from projects.SpikesAsia.fixtures.CampaignData import campaign_data
from projects.SpikesAsia.pages.AccountPage import SAAccountPage
from projects.DubaiLynx.pages.CampaignPage import CampaignPage
from projects.DubaiLynx.pages.OrderPage import OrderPage as DLOrderPage
from projects.DubaiLynx.fixtures.CampaignData import categories_data, \
    linux_file_paths


linux_path = '/var/lib/jenkins/workspace/files/File2.jpg'


class SAMultipleAwardsTests(TestDriver):

    def test_sl_aw1_1_create_cl_account_and_update_details(self):
        obj = SAAccountPage(self.driver)
        entries_data['email'] = "SA_Multiple_Award_" + entries_data['email']
        res = obj.create_account(entries_data, site='sa_awards')
        self.assert_and_log('login', res)

    @Expects(['login'])
    def test_sl_aw1_2_create_campaign_with_one_entry(self):
        if platform.system() == 'Linux':
            categories_data['media_upload'] = linux_file_paths
        obj = CampaignPage(self.driver)
        (campaign, res) = obj.create_campaign(campaign_data, brand='sa')
        assert res
        assert obj.create_entry(campaign, two_awards_seven_categories['award'],
                                two_awards_seven_categories['category'],
                                entries=None,
                                brand='sa')
        res = obj.fill_campaign_details(
            categories_data, campaign,
            two_awards_seven_categories['entries'], brand='sa')
        self.assert_and_log('single_fill', res)

    @Expects(['single_fill'])
    def test_sl_aw1_3_process_order(self):
        obj = OrderPage(self.driver)
        obj.go_to_payment_page()
        assert obj.add_company_details('invoice', entries_company_data)
        assert obj.add_senior_officer(senior_officer_data)
        assert obj.add_company_details('billing', billing_address_data)

        obj1 = DLOrderPage(self.driver)
        entries = obj1.process_entries(two_awards_seven_categories['entries'],
                                       brand='sa')
        price = obj1.process_totals(entries, brand='sa')
        assert obj.process_entries_payment(
            'credit_card', total=price, data=awards_cc_visa, card_type='visa',
            festival='sa')

import platform

from utils.TestUtils import Expects
from TestDriver import TestDriver

from projects.CannesLions.pages.OrderPage import OrderPage
from projects.SpikesAsia.pages.AccountPage import SAAccountPage
from projects.SpikesAsia.pages.PassPage import SAPassPage
from projects.SpikesAsia.fixtures.TestData import delegate_data, company_data, \
    someother_delegate, ysc_data, update_delegate_data, test_data, media_data
from projects.SpikesAsia.fixtures.PaymentData import cred_card_mastercard, \
    cred_card_amex, cred_card_visa

linux_path = '/var/lib/jenkins/workspace/files/Capture1.PNG'


class SASpecialPassesTests(TestDriver):

    def test_sp1_create_account_login(self):
        obj = SAAccountPage(self.driver)
        delegate_data['email'] = "SA_Special_Passes_" + delegate_data['email']
        res = obj.create_account(delegate_data, site='spikes_asia')
        self.assert_and_log('new_account', res)

    @Expects(['new_account'])
    def test_sp2_buy_press_pass(self):
        obj = SAPassPage(self.driver)
        obj.add_pass_from_list_view(delegate_data, pass_type='press')
        obj = OrderPage(self.driver)
        if platform.system() == 'Linux':
            media_data['id_path'] = linux_path
        res = obj.process_special_passes(
            person="myself", pass_type="press", data=media_data,
            extra_form=True)
        self.assert_and_log('press', res)

    @Expects(['press'])
    def test_sp3_check_pass_count_increase(self):
        obj = SAAccountPage(self.driver)
        assert obj.check_pass_count(pending_count=0, ready_count=1)

    @Expects(['new_account'])
    def test_sp4_buy_competitor_pass(self):
        card_type = 'mc'
        obj = SAPassPage(self.driver)
        obj.add_pass_from_list_view(delegate_data, pass_type='competitor')
        obj = OrderPage(self.driver)
        if platform.system() == 'Linux':
            ysc_data['path'] = linux_path
        (res, card_fee) = obj.process_order(
            person="someone", total=680, account='old', data=ysc_data,
            delegate='competitor', festival='spikes_asia', card_fees=card_type)
        assert res
        assert obj.add_invoice(company_data, festival='sa')
        res = obj.pay_for_order(
            pay_by='credit_card', card_fee=card_fee,
            card=card_type, data=cred_card_mastercard)
        self.assert_and_log('competitor', res)

    @Expects(['competitor'])
    def test_sp5_check_pass_count_increase(self):
        obj = SAAccountPage(self.driver)
        assert obj.check_pass_count(pending_count=0, ready_count=2)

    @Expects(['new_account'])
    def test_sp6_buy_representative_pass(self):
        obj = SAPassPage(self.driver)
        obj.add_pass_from_list_view(delegate_data, pass_type='representative')
        obj = OrderPage(self.driver)
        if platform.system() == 'Linux':
            someother_delegate['path'] = linux_path
        res = obj.process_special_passes(
            person="someone", pass_type="representative", account='old',
            data=someother_delegate, extra_form=True)
        self.assert_and_log('competitor', res)

    @Expects(['press'])
    def test_sp7_check_pass_count_increase(self):
        obj = SAAccountPage(self.driver)
        assert obj.check_pass_count(pending_count=0, ready_count=3)

from TestDriver import TestDriver
from projects.SpikesAsia.fixtures.ValidationTestData import \
    empty_credentials, invalid_credentials, create_account_data
from projects.CannesLions.pages.AccountPage import AccountPage
from utils.TestUtils import Expects


class SAAccountValidationTests1(TestDriver):

    def test_av1_v2_log_into_portal_with_empty_username_and_password(self):
        obj = AccountPage(self.driver)
        res = obj.check_account_validations(empty_credentials, skip='both',
                                            domain='spikes_asia')
        self.assert_and_log('landing', res)

    @Expects(['landing'])
    def test_av1_v3_log_into_portal_with_password_only(self):
        obj = AccountPage(self.driver)
        assert obj.check_account_validations(invalid_credentials, skip='email')

    @Expects(['landing'])
    def test_av1_v4_log_into_portal_with_username_only(self):
        obj = AccountPage(self.driver)
        assert obj.check_account_validations(invalid_credentials,
                                             skip='password')

    @Expects(['landing'])
    def test_av1_v5_log_into_portal_with_invalid_credentials(self):
        obj = AccountPage(self.driver)
        assert obj.check_account_validations(invalid_credentials,
                                             skip='invalid')

    @Expects(['landing'])
    def test_av1_v6_check_all_fields_validations_for_create_account(self):
        obj = AccountPage(self.driver)
        assert obj.check_account_creation_validations("all_1",
                                                      create_account_data)

    @Expects(['landing'])
    def test_av1_v7_check_per_field_validations_for_create_account_page_1(self):
        obj = AccountPage(self.driver)
        assert obj.check_account_creation_validations("first_name",
                                                      create_account_data)
        assert obj.check_account_creation_validations("last_name",
                                                      create_account_data)
        assert obj.check_account_creation_validations("email",
                                                      create_account_data)
        assert obj.check_account_creation_validations("password",
                                                      create_account_data)
        assert obj.check_account_creation_validations("job_title",
                                                      create_account_data)
        assert obj.check_account_creation_validations("mobile",
                                                      create_account_data)

    @Expects(['landing'])
    def test_av1_v8_check_per_field_validations_for_create_account_page_2(self):
        obj = AccountPage(self.driver)
        assert obj.check_account_creation_validations("company_name",
                                                      create_account_data)
        assert obj.check_account_creation_validations("address1",
                                                      create_account_data)
        assert obj.check_account_creation_validations("city",
                                                      create_account_data)
        assert obj.check_account_creation_validations("postcode",
                                                      create_account_data)
        assert obj.check_account_creation_validations("country",
                                                      create_account_data)

        # assert obj.check_account_creation_validations("company_activity",
        #                                               create_account_data)
        assert obj.check_account_creation_validations("company_type",
                                                      create_account_data)



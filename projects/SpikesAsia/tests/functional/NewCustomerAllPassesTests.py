import platform
import strgen

from utils.TestUtils import Expects
from TestDriver import TestDriver

from projects.SpikesAsia.pages.AccountPage import SAAccountPage
from projects.SpikesAsia.pages.PassPage import SAPassPage
from projects.SpikesAsia.fixtures.TestData import delegate_data, company_data, \
    someother_delegate
from projects.CannesLions.pages.OrderPage import OrderPage as SAOrderPage
from projects.SpikesAsia.fixtures.PaymentData import cred_card_mastercard, \
    cred_card_amex, cred_card_visa

linux_path = '/var/lib/jenkins/workspace/files/Capture1.PNG'
passes = ['Spikes Student', 'Account Leadership Academy', 'Digital Academy',
          'Marketers Academy', 'Strategy & Effectiveness Academy']
passes_dict = {
    'Spikes Student': 'spikes_student',
    'Account Leadership Academy': 'account_leadership_academy',
    'Digital Academy': 'digital_academy',
    'Marketers Academy': 'marketers_academy',
    'Strategy & Effectiveness Academy': 'strategy_effectiveness_academy'
}


class NewCustomerTests(TestDriver):

    def test_n1_create_account_login(self):
        obj = SAAccountPage(self.driver)
        delegate_data['email'] = "new_cust_tests" + delegate_data['email']
        res = obj.create_account(delegate_data, site='spikes_asia')
        self.assert_and_log('new_account', res)

    @Expects(['new_account'])
    def test_n2_add_pass_using_mastercard(self):
        card_type = 'mc'
        obj = SAPassPage(self.driver)
        (res, price_total) = obj.add_single_pass("Spikes Delegate")
        assert res
        obj2 = SAOrderPage(self.driver)
        (res, card_fee) = obj2.process_order("myself", price_total,
                                             festival='spikes_asia',
                                             card_fees=card_type)
        assert res
        assert obj2.add_invoice(company_data, festival='sa')
        res = obj2.pay_for_order(pay_by='credit_card', card_fee=card_fee,
                                 card=card_type, data=cred_card_mastercard)
        self.assert_and_log('pass1', res)

    @Expects(['pass1'])
    def test_n3_add_media_academy_pass_using_visa(self):
        someother_delegate['email'] = \
            str(strgen.StringGenerator("[\d\w]{6}").render()) + \
            someother_delegate['email']
        card_type = 'visa'
        obj = SAPassPage(self.driver)
        (res, price_total) = obj.add_single_pass("Media Academy")
        assert res
        obj2 = SAOrderPage(self.driver)
        if platform.system() == 'Linux':
            someother_delegate['path'] = linux_path
        (res, card_fee) = obj2.process_order(
            "someone", price_total, delegate='media_academy', account='old',
            data=someother_delegate, festival='spikes_asia',
            card_fees=card_type)
        assert res
        assert obj2.add_invoice(company_data, festival='sa')
        res = obj2.pay_for_order(pay_by='credit_card', card_fee=card_fee,
                                 card=card_type, data=cred_card_visa)
        self.assert_and_log('pass2', res)

    @Expects(['pass2'])
    def test_n4_add_young_spikes_pass_using_amex(self):
        someother_delegate['email'] = \
            str(strgen.StringGenerator("[\d\w]{6}").render()) + \
            someother_delegate['email']
        card_type = 'amex'
        obj = SAPassPage(self.driver)
        (res, price_total) = obj.add_single_pass("Young Spikes")
        assert res
        obj2 = SAOrderPage(self.driver)
        (res, card_fee) = obj2.process_order(
            "someone", price_total, delegate='young_spikes', account='old',
            data=someother_delegate, festival='spikes_asia',
            card_fees=card_type)
        assert res
        assert obj2.add_invoice(company_data, festival='sa')
        res = obj2.pay_for_order(pay_by='credit_card', card_fee=card_fee,
                                 card=card_type, data=cred_card_amex)
        self.assert_and_log('pass3', res)

    @Expects(['pass3'])
    def test_n5_add_spikes_student_pass_using_amex(self):
        someother_delegate['email'] = \
            str(strgen.StringGenerator("[\d\w]{6}").render()) + \
            someother_delegate['email']
        card_type = 'amex'
        obj = SAPassPage(self.driver)
        (res, price_total) = obj.add_single_pass("Spikes Student")
        assert res
        obj2 = SAOrderPage(self.driver)
        (res, card_fee) = obj2.process_order(
            "someone", price_total, delegate='spikes_student', account='old',
            data=someother_delegate, festival='spikes_asia',
            card_fees=card_type)
        assert res
        assert obj2.add_invoice(company_data, festival='sa')
        res = obj2.pay_for_order(pay_by='credit_card', card_fee=card_fee,
                                 card=card_type, data=cred_card_amex)
        self.assert_and_log('pass4', res)

    @Expects(['pass4'])
    def test_n6_add_account_leadership_pass_using_mc(self):
        someother_delegate['email'] = \
            str(strgen.StringGenerator("[\d\w]{6}").render()) + \
            someother_delegate['email']
        card_type = 'mc'
        obj = SAPassPage(self.driver)
        (res, price_total) = obj.add_single_pass("Account Leadership Academy")
        assert res
        obj2 = SAOrderPage(self.driver)
        (res, card_fee) = obj2.process_order(
            "someone", price_total, delegate='account_leadership_academy',
            account='old',
            data=someother_delegate, festival='spikes_asia',
            card_fees=card_type)
        assert res
        assert obj2.add_invoice(company_data, festival='sa')
        res = obj2.pay_for_order(pay_by='bank_transfer', festival='sa')
        self.assert_and_log('pass5', res)

    @Expects(['pass5'])
    def test_n7_add_digital_academy_pass_using_mc(self):
        someother_delegate['email'] = \
            str(strgen.StringGenerator("[\d\w]{6}").render()) + \
            someother_delegate['email']
        card_type = 'mc'
        obj = SAPassPage(self.driver)
        (res, price_total) = obj.add_single_pass("Digital Academy")
        assert res
        obj2 = SAOrderPage(self.driver)
        (res, card_fee) = obj2.process_order(
            "someone", price_total, delegate='digital_academy', account='old',
            data=someother_delegate, festival='spikes_asia',
            card_fees=card_type)
        assert res
        assert obj2.add_invoice(company_data, festival='sa')
        res = obj2.pay_for_order(pay_by='credit_card', card_fee=card_fee,
                                 card=card_type, data=cred_card_mastercard)
        self.assert_and_log('pass6', res)

    @Expects(['pass6'])
    def test_n8_add_marketers_academy_pass_using_mc(self):
        someother_delegate['email'] = \
            str(strgen.StringGenerator("[\d\w]{6}").render()) + \
            someother_delegate['email']
        card_type = 'mc'
        obj = SAPassPage(self.driver)
        (res, price_total) = obj.add_single_pass("Marketers Academy")
        assert res
        obj2 = SAOrderPage(self.driver)
        (res, card_fee) = obj2.process_order(
            "someone", price_total, delegate='marketers_academy', account='old',
            data=someother_delegate, festival='spikes_asia',
            card_fees=card_type)
        assert res
        assert obj2.add_invoice(company_data, festival='sa')
        res = obj2.pay_for_order(pay_by='credit_card', card_fee=card_fee,
                                 card=card_type, data=cred_card_mastercard)
        self.assert_and_log('pass7', res)

    @Expects(['pass7'])
    def test_n9_add_strategy_pass_using_mc(self):
        someother_delegate['email'] = \
            str(strgen.StringGenerator("[\d\w]{6}").render()) + \
            someother_delegate['email']
        card_type = 'mc'
        obj = SAPassPage(self.driver)
        (res, price_total) = \
            obj.add_single_pass("Strategy & Effectiveness Academy")
        assert res
        obj2 = SAOrderPage(self.driver)
        (res, card_fee) = obj2.process_order(
            "someone", price_total, delegate='strategy_effectiveness_academy',
            account='old', data=someother_delegate, festival='spikes_asia',
            card_fees=card_type)
        assert res
        assert obj2.add_invoice(company_data, festival='sa')
        res = obj2.pay_for_order(pay_by='credit_card', card_fee=card_fee,
                                 card=card_type, data=cred_card_mastercard)
        self.assert_and_log('pass3', res)


    # @Expects(['pass3'])
    # def test_n5_buy_spikes_rest_of_passes_with_bank_transfer(self):
    #     obj = SAPassPage(self.driver)
    #     obj2 = SAOrderPage(self.driver)
    #     for onepass in passes:
    #         (res, price_total) = obj.add_single_pass(onepass)
    #         assert res
    #         someother_delegate['email'] = \
    #             str(strgen.StringGenerator("[\d\w]{6}").render()) + \
    #             someother_delegate['email']
    #         assert obj2.process_order(
    #             "someone", price_total, delegate=passes_dict[onepass],
    #             account='old', data=someother_delegate, festival='spikes_asia')
    #         assert obj2.add_invoice(company_data, festival='sa')
    #         assert obj2.pay_for_order(pay_by='bank_transfer', festival = 'sa')
    #         print "Added " + onepass + " Pass Successfully"



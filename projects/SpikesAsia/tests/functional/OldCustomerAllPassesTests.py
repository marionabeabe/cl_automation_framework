import platform
import strgen
import pytest

from utils.TestUtils import Expects
from TestDriver import TestDriver

from projects.SpikesAsia.pages.AccountPage import SAAccountPage
from projects.SpikesAsia.pages.PassPage import SAPassPage
from projects.SpikesAsia.fixtures.TestData import delegate_data, company_data, \
    someother_delegate, test_data
from projects.CannesLions.pages.OrderPage import OrderPage as SAOrderPage
from projects.SpikesAsia.fixtures.PaymentData import cred_card_mastercard, \
    cred_card_amex, cred_card_visa
from projects.CannesLions.pages.APICalls import APICalls

linux_path = '/var/lib/jenkins/workspace/files/Capture1.PNG'
passes = ['Spikes Student', 'Account Leadership Academy', 'Digital Academy',
          'Marketers Academy', 'Strategy & Effectiveness Academy']
passes_dict = {
    'Spikes Student': 'spikes_student',
    'Account Leadership Academy': 'account_leadership_academy',
    'Digital Academy': 'digital_academy',
    'Marketers Academy': 'marketers_academy',
    'Strategy & Effectiveness Academy': 'strategy_effectiveness_academy'
}


class OldCustomerTests(TestDriver):

    def test_login_into_old_account(self):
        obj = SAAccountPage(self.driver)
        res = obj.log_into_account(test_data)
        self.assert_and_log('old_account', res)
        api = APICalls(self)
        assert api.api_remove_all_passes(
            festival='sa', person_id=test_data["person_id"],
            env=test_data['env'])

    @Expects(['old_account'])
    def test_o2_add_pass_using_mastercard(self):
        card_type = 'mc'
        obj = SAPassPage(self.driver)
        (res, price_total) = obj.add_single_pass("Spikes Delegate")
        assert res
        obj2 = SAOrderPage(self.driver)
        test_env = pytest.config.getoption('LAB')
        if test_env == 'uat':
            (res, card_fee) = obj2.process_order(
                "myself", price_total, festival='spikes_asia',
                card_fees=card_type)
            assert res
        else:
            (res, card_fee) = obj2.process_order(
                "someone", price_total, delegate='spikes_delegate',
                account='old', data=someother_delegate, festival='spikes_asia',
                card_fees=card_type)
            assert res
        assert obj2.add_invoice(company_data, festival='sa')
        res = obj2.pay_for_order(pay_by='credit_card', card_fee=card_fee,
                                 card=card_type, data=cred_card_mastercard)
        self.assert_and_log('pass1', res)

    @Expects(['pass1'])
    def test_o3_add_pass_using_visa(self):
        someother_delegate['email'] = \
            str(strgen.StringGenerator("[\d\w]{6}").render()) + \
            someother_delegate['email']
        card_type = 'visa'
        obj = SAPassPage(self.driver)
        (res, price_total) = obj.add_single_pass("Media Academy")
        assert res
        obj2 = SAOrderPage(self.driver)
        if platform.system() == 'Linux':
            someother_delegate['path'] = linux_path
        (res, card_fee) = obj2.process_order(
            "someone", price_total, delegate='media_academy', account='old',
            data=someother_delegate, festival='spikes_asia',
            card_fees=card_type)
        assert res
        assert obj2.add_invoice(company_data, festival='sa')
        res = obj2.pay_for_order(pay_by='credit_card', card_fee=card_fee,
                                 card=card_type, data=cred_card_visa)
        self.assert_and_log('pass2', res)

    @Expects(['pass2'])
    def test_o4_add_pass_using_amex(self):
        someother_delegate['email'] = \
            str(strgen.StringGenerator("[\d\w]{6}").render()) + \
            someother_delegate['email']
        card_type = 'amex'
        obj = SAPassPage(self.driver)
        (res, price_total) = obj.add_single_pass("Young Spikes")
        assert res
        obj2 = SAOrderPage(self.driver)
        (res, card_fee) = obj2.process_order(
            "someone", price_total, delegate='young_spikes', account='old',
            data=someother_delegate, festival='spikes_asia',
            card_fees=card_type)
        assert res
        assert obj2.add_invoice(company_data, festival='sa')
        res = obj2.pay_for_order(pay_by='credit_card', card_fee=card_fee,
                                 card=card_type, data=cred_card_amex)
        self.assert_and_log('pass3', res)

    @Expects(['pass3'])
    def test_o5_buy_spikes_rest_of_passes_with_bank_transfer(self):
        obj = SAPassPage(self.driver)
        obj2 = SAOrderPage(self.driver)
        for onepass in passes:
            someother_delegate['email'] = \
                str(strgen.StringGenerator("[\d\w]{6}").render()) + \
                someother_delegate['email']
            (res, price_total) = obj.add_single_pass(onepass)
            someother_delegate['email'] = 'test_automation@13.com'
            assert res
            assert obj2.process_order(
                "someone", price_total, delegate=passes_dict[onepass],
                account='old', data=someother_delegate, festival='spikes_asia')
            assert obj2.add_invoice(company_data, festival='sa')
            assert obj2.pay_for_order(pay_by='bank_transfer', festival='sa')
            print "Added " + onepass + " Pass Successfully"



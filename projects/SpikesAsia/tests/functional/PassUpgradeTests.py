import platform

from utils.TestUtils import Expects
from TestDriver import TestDriver

from projects.SpikesAsia.pages.AccountPage import SAAccountPage
from projects.SpikesAsia.pages.PassPage import SAPassPage
from projects.SpikesAsia.fixtures.TestData import delegate_data, company_data, \
    someother_delegate
from projects.CannesLions.pages.OrderPage import OrderPage as SAOrderPage
from projects.CannesLions.pages.PassManagerPage import PassManagerPage
from projects.CannesLions.fixtures.UpdateData import update_pass_data, \
    replace_delegate, visa_details, companion_details
from projects.SpikesAsia.fixtures.PaymentData import cred_card_mastercard, \
    cred_card_amex, cred_card_visa

linux_path = '/var/lib/jenkins/workspace/files/Capture1.PNG'


class PassUpgradeTests(TestDriver):

    def test_pu1_create_account_login(self):
        obj = SAAccountPage(self.driver)
        delegate_data['email'] = "pass_upgrade_tests" + delegate_data['email']
        res = obj.create_account(delegate_data, site='spikes_asia')
        self.assert_and_log('new_account', res)

    @Expects(['new_account'])
    def test_pu2_add_pass_using_mastercard(self):
        card_type = 'mc'
        obj = SAPassPage(self.driver)
        (res, price_total) = obj.add_single_pass("Spikes Delegate")
        assert res
        obj2 = SAOrderPage(self.driver)
        (res, card_fee) = obj2.process_order("myself", price_total,
                                             festival='spikes_asia',
                                             card_fees=card_type)
        assert res
        assert obj2.add_invoice(company_data, festival='sa')
        res = obj2.pay_for_order(pay_by='credit_card', card_fee=card_fee,
                                 card=card_type, data=cred_card_mastercard)
        self.assert_and_log('pass1', res)

    @Expects(['new_account'])
    def test_pu3_add_pass_using_amex(self):
        if platform.system() == 'Linux':
            someother_delegate['path'] = linux_path
        card_type = 'amex'
        obj = SAPassPage(self.driver)
        (res, price_total) = obj.add_single_pass("Young Spikes")
        assert res
        obj2 = SAOrderPage(self.driver)
        (res, card_fee) = obj2.process_order(
            "someone", price_total, delegate='young_spikes', account='old',
            data=someother_delegate, festival='spikes_asia',
            card_fees=card_type)
        assert res
        assert obj2.add_invoice(company_data, festival='sa')
        res = obj2.pay_for_order(pay_by='credit_card', card_fee=card_fee,
                                 card=card_type, data=cred_card_amex)
        self.assert_and_log('pass1', res)

    @Expects(['pass1'])
    def test_pu4_change_delegate_details(self):
        obj = PassManagerPage(self.driver)
        res = obj.upgrade_booked_pass(
            'Young Spikes', 'delegate', update_pass_data)
        self.assert_and_log('pass1', res)

    @Expects(['pass1'])
    def test_pu5_upgrade_pass(self):
        card_type = 'amex'
        obj = PassManagerPage(self.driver)
        (res, total) = obj.upgrade_booked_pass(
            pass_type='Young Spikes', upgrade='pass_type',
            data=companion_details, upgrade_pass="Digital Academy",
            festival='spikes_asia', vat_free=True)
        assert res
        card_fee = obj.process_card_fee(
            festival='spikes_asia', card_type=card_type, total=total)
        obj = SAOrderPage(self.driver)
        res = obj.pay_for_order(pay_by='credit_card', card_fee=card_fee,
                                card=card_type, data=cred_card_amex)
        self.assert_and_log('upgrade', res)

    @Expects(['pass1'])
    def test_pu6_replace_delegate(self):
        if platform.system() == 'Linux':
            replace_delegate['path'] = linux_path
        obj = PassManagerPage(self.driver)
        assert obj.upgrade_booked_pass(
            'Digital Academy', 'replace', replace_delegate,
            person="someone", account="old", vat_free=True,
            delegate='digital_academy')
        obj = SAOrderPage(self.driver)
        res = obj.pay_for_order(pay_by='free')
        self.assert_and_log('replace', res)

    @Expects(['replace'])
    def test_pu7_request_visa_letter(self):
        obj = PassManagerPage(self.driver)
        res = obj.upgrade_booked_pass(
            'Digital Academy', 'visa', visa_details,
            festival='spikes_asia')
        self.assert_and_log('visa', res)

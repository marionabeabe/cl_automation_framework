import platform

from utils.TestUtils import Expects
from TestDriver import TestDriver

from projects.SpikesAsia.pages.AccountPage import SAAccountPage
from projects.SpikesAsia.pages.PassPage import SAPassPage
from projects.SpikesAsia.fixtures.TestData import delegate_data, company_data, \
    someother_delegate, update_delegate_data, test_data
from projects.CannesLions.pages.OrderPage import OrderPage as SAOrderPage
from projects.SpikesAsia.fixtures.PaymentData import cred_card_mastercard, \
    cred_card_visa

linux_path = '/var/lib/jenkins/workspace/files/Capture1.PNG'


class SAAccountSanityTests(TestDriver):

    def test_s1_create_account_login(self):
        obj = SAAccountPage(self.driver)
        delegate_data['email'] = "SA_Account_" + delegate_data['email']
        res = obj.create_account(delegate_data, site='spikes_asia')
        self.assert_and_log('new_account', res)

    @Expects(['new_account'])
    def test_s2_update_account_details(self):
        obj = SAAccountPage(self.driver)
        assert obj.update_account_details('profile', update_delegate_data)

    @Expects(['new_account'])
    def test_s3_update_marketing_preferences(self):
        obj = SAAccountPage(self.driver)
        update_delegate_data['marketing'] = {
            'cl': 'true', 'eb': 'false', 'dl': 'true', 'sa': 'false',
            'ae': 'true'
        }
        assert obj.update_account_details('marketing', update_delegate_data)

    @Expects(['new_account'])
    def test_s4_update_password(self):
        obj = SAAccountPage(self.driver)
        assert obj.update_account_details('password', update_delegate_data)

    @Expects(['new_account'])
    def test_s5_add_pass_using_mastercard(self):
        card_type = 'mc'
        obj = SAPassPage(self.driver)
        (res, price_total) = obj.add_single_pass("Spikes Delegate")
        assert res
        obj2 = SAOrderPage(self.driver)
        (res, card_fee) = obj2.process_order("myself", price_total,
                                             festival='spikes_asia',
                                             card_fees=card_type)
        assert res
        assert obj2.add_invoice(company_data, festival='sa')
        res = obj2.pay_for_order(pay_by='credit_card', card_fee=card_fee,
                                 card=card_type, data=cred_card_mastercard)
        self.assert_and_log('pass1', res)

    @Expects(['pass1'])
    def test_s6_check_count_increase(self):
        obj = SAAccountPage(self.driver)
        assert obj.check_pass_count(pending_count=0, ready_count=1)

    # @Expects(['new_pass'])
    # def test_s7_check_registration_invoices(self):
    #     obj = SAAccountPage(self.driver)
    #     assert obj.check_registration_invoices(1)

    @Expects(['pass1'])
    def test_s8_add_pass_using_visa(self):
        card_type = 'visa'
        obj = SAPassPage(self.driver)
        (res, price_total) = obj.add_single_pass("Media Academy")
        assert res
        obj2 = SAOrderPage(self.driver)
        if platform.system() == 'Linux':
            someother_delegate['path'] = linux_path
        (res, card_fee) = obj2.process_order(
            "someone", price_total, delegate='media_academy', account='old',
            data=someother_delegate, festival='spikes_asia',
            card_fees=card_type)
        assert res
        assert obj2.add_invoice(company_data, festival='sa')
        res = obj2.pay_for_order(pay_by='credit_card', card_fee=card_fee,
                                 card=card_type, data=cred_card_visa)
        self.assert_and_log('pass2', res)

    @Expects(['pass2'])
    def test_s9_check_count_increase(self):
        obj = SAAccountPage(self.driver)
        assert obj.check_pass_count(pending_count=0, ready_count=2)

    # @Expects(['new_pass'])
    # def test_ss10_check_registration_invoices(self):
    #     obj = SAAccountPage(self.driver)
    #     assert obj.check_registration_invoices(1)

    @Expects(['pass2'])
    def test_ss11_logout(self):
        obj = SAAccountPage(self.driver)
        assert obj.logout(test_data)




cc_data = {
    'card_holder': 'John Walker',
    'type': {
        'Visa': {
            'cc_number': '4005550000000001',
            'security': 123,
            'month': '05',
            'year': '2021'
        },
        'Mastercard': {
            'cc_number': '5123456789012346',
            'security': 123,
            'month': '05',
            'year': '2021'
        },
        'Amex': {
            'cc_number': '345678901234564',
            'security': 1234,
            'month': '05',
            'year': '2021'

        },
    },
    'billing_name': 'John Walker',
    'address1': 'Adventure Road',
    'address2': 'Advent Building',
    'city': 'Manchester',
    'postcode': 'MK12 F45',
    'country': 'UNITED KINGDOM'
}

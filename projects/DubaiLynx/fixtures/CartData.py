data = {
    'senior_officer': 'Captain Kirk',
    'position': 'Captain'
}

company_data = {
    'company': 'BBC Documentary',
    'address1': 'BBC Buildings',
    'address2': 'Broad Lane',
    'postcode': 'SE3 6YH',
    'city': 'London',
    'country': 'UNITED KINGDOM',
    'vat_number': 'GB12345678',
    'email': 'rob.william@bbc.co.uk'
}

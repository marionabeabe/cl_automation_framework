import os
import datetime
import strgen

random_string = str(strgen.StringGenerator("[\d\w]{6}").render())

now = datetime.datetime.now()
formatted_date = now.strftime("%Y-%m-%d-%H-%M")
test_env = os.getenv('TEST_ENV', 'uat')
customer = os.getenv('customer', 'test_uat')
Environments = {
    "uat": "https://uat.dubailynx.com/",
    "staging": "",
    "prod": "https://www.canneslions.com",
}

Accounts = {
    "admin_uat": {
        "username": "admin@test.com",
        "password": "Password007"
    },
    "test_uat": {
        "username": "DL_Automation_2018-11-30-09-14@dl.com",
        "password": "Automation123"
    }
}
test_data = {
    "url": Environments[test_env],
    "username": Accounts[customer]['username'],
    "password": Accounts[customer]['password'],
    "title": "Mr",
    "comp_activity": "Production",
    "country": "UNITED KINGDOM",
    "job_role": "Technology",
    "company_type": "Photography"
}

delegate_data = {
    "url": Environments[test_env],
    "title": "Mr",
    "first_name": "Bugs",
    "last_name": "Bunny",
    "email": "DL_Automation_" + formatted_date + random_string + "@dl.com",
    "password": "Automation123",
    "job_title": "Product Manager",
    "mobile": "+44327442878",
    "company_name": "Cartoon Network",
    "address1": "Build Studios",
    "address2": "Coyote Road",
    "city": "London",
    "postcode": "BUGS11 BU1NY",
    "country": "United Kingdom",
    "company_role": "Middle Manager",
    "company_activity": "Production",
    "company_type": "Photography",
    "comp_activity": "Production",
    "job_role": "Technology",
    "country2": "UNITED KINGDOM",

}
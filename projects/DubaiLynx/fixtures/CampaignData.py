import os
user = os.getenv('username')

campaign_data = {
    'title': 'Making Roads Safer',
    'brand': 'BMW',
    'product': 'ACAS',
    'sector': 'Automotive',
    'sub_sector': 'Vehicles'
}

health_campaign_data = {
    'title': 'Making Health Better',
    'brand': 'NHS',
    'product': 'lot',
    'sector': 'Healthcare',
    'sub_sector': 'OTC drugs'
}

campaign_2_data = {
    'title': 'Making Roads Safer 2',
    'brand': 'BMW',
    'product': 'ACAS',
    'sector': 'Automotive',
    'sub_sector': 'Vehicles'
}

categories_data = {
    'company': {
        'company_name': 'Space International',
        'company_activity': 'N/A',
        # TODO:
        # if the comp activity is agency three more fields populate
        'company_type': 'Associations',
        'agency_network': 'ALL OTHER COMPANIES',
        'address1': 'Aeronautical Buildings',
        'address2': 'Aero Road',
        'address3': 'Manhattan',
        'city': 'London',
        'postcode': 'TR15 1PE',
        'country': 'UNITED KINGDOM'
    },
    'contact': {
        'name': 'Mike Ross',
        'title': 'Associate',
        'mobile': '1234567890',
        'email': 'mikeross@pd.com',
        'client_name': 'Rachael Zane',
        'client_title': 'Paralegal',
        'client_mobile': '1234567990',
        'client_email': 'rachael.zane@pd.com',
        'client_city': 'LONDON',
        'client_country': 'UNITED KINGDOM'
    },
    'media_info': {
        'element': 'Test Element',
        'location': 'BBC',
        'airings': 5,
        'start_date': '03/05/2018',
        'end_date': '06/08/2018',
        'country': 'United Kingdom',
        'url': 'https://www.google.com',
        'url_type': 'Website',
        'cl_country': 'EUROPE'
    },
    'explanation': {
        'why_work': 'Hey this is my work',
        'background': 'Hey this is me',
        'creative_idea': 'Hey this is my idea',
        'strategy': 'Hey this is my strategy',
        'execution': 'Hey this my execution',
        'results': 'Hey these are my results',
        'conf_info': 'Hey this is info',
        'what_happens_in_film': 'Hey this what happens in film',
        'context_info': 'Hey this is my context',
        'why_relevant': 'It doesnt matter',
        'the_results': 'Distinction',
        'outcome': 'This is my outcome',
        'script': 'This is my script',
        'casting': 'This is my casting',
        'design': 'This is my design',
        'cinematography': 'This is my cinematography',
        'about_the_edit': 'This is my edit',
        'music_track': 'This is my track',
        'sound_design': 'This is my sound design',
        'animation': 'This is the animation',
        'visual_effects': 'This is the visual effect',
        'achievements': 'These are my achievements',
        'full_radio_advert': 'This is a full advert',
        'radio_advert': 'This is a radio_advert'
    },
    'creative_team': {
        'first_name': 'Harvey',
        'last_name': 'Specter',
        'name': 'Harvey Specter',
        'company': 'Pearson Harvey',
        'position': 'Managing Director',
        'team_role': 'Manager',
        'email': 'harvey.specter@ph.com'
    },
    'media_upload': {
        'file': 'C:/Users/{}/automation_framework/proj'
                'ects/UploadFiles/File.jpg'.format(user),
        '3_min_video': 'C:/Users/{}/automation_framework/proj'
                'ects/UploadFiles/three_mins.mp4'.format(user),
        'big_file': 'C:/Users/{}/automation_framework/proj'
                'ects/UploadFiles/BigFile.jpg'.format(user),
        'video': 'C:/Users/{}/automation_framework/proj'
                 'ects/UploadFiles/sample_video.mp4'.format(user),
        'mp3': 'C:/Users/{}/automation_framework/proj'
                 'ects/UploadFiles/sample_mp3.mp3'.format(user),
        'length': 60,
        'pdf': 'C:/Users/{}/automation_framework/proj'
                 'ects/UploadFiles/sample_pdf.pdf'.format(user)


    }

}

linux_file_paths = {
    'file': '/var/lib/jenkins/workspace/files/File2.jpg',
    '3_min_video': '/var/lib/jenkins/workspace/files/less_than_three_mins.mov',
    'big_file': '/var/lib/jenkins/workspace/files/BigFile.jpg',
    'video': '/var/lib/jenkins/workspace/files/sample_video.mp4',
    'mp3': '/var/lib/jenkins/workspace/files/sample_mp3.mp3',
    'pdf': '/var/lib/jenkins/workspace/files/sample_pdf.pdf',
    'length': 60
}
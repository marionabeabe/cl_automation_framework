from BasePage import BasePage

loc = {
    "add_to_cart": "xpath=//span[text()='Add to cart']",
    "cart_button": "xpath=//span[contains(text(), 'My Cart')]",
    "in_cart": "xpath=//td[contains(text(), 'In cart')]",
    "proceed_to_checkout": "xpath=//input[@value='Proceed to checkout']",
    "entry_count": "xpath=//div[contains(@class, 'c-my-cart__entries')]//span"
                   "[contains(@class, 'js-cart-count')]",
    "total_price":
        "xpath=//span[contains(@class, 'c-my-cart__entries-amount')]//span["
        "contains(@class, 'js-cart-total-price')]",
    "pay_by_bank": "xpath=//input[@id='banktransfer_submit']",
    "pay_by_card": "xpath=//input[@id='creditcard_submit']",
    "agree_terms": "xpath=//label[@id='for_i_agree']",
    "senior_officer": "xpath=//input[@id='senior_officer_name']",
    "position": "xpath=//input[@id='senior_officer_position']"

}


class CartPage(BasePage):

    def __init__(self, driver):
        super(CartPage, self).__init__(driver=driver)

    def add_to_cart(self):
        #self.press(loc["add_to_cart"])
        self.wait_in_seconds(2)
        self.press(loc["cart_button"])
        count = int(self.find_by_locator(loc["entry_count"]).text)
        price = int(self.find_by_locator(loc["total_price"]).text)
        self.wait_until_appear(loc["proceed_to_checkout"])
        self.press(loc["proceed_to_checkout"])
        self.wait_until_appear(loc["pay_by_bank"])
        res = self.is_element_visible(loc["pay_by_bank"])
        return {'res': res, 'count': count, 'price': price}

    def process_order(self, data):
        self.press(loc["agree_terms"])
        self.type(loc["senior_officer"], data['senior_officer'])
        self.type(loc["position"], data['position'])
        return True

    def calculate_totals_vat(self):
        pass


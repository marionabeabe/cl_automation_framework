from projects.CannesLions.pages.AccountPage import AccountPage

loc = {
    "select_system": "xpath=//h3[text()='Select your system:']",
    "system_generic": "xpath=//a[text()='{}']",
    "register_heading": "xpath=//h1[text()='Buy Passes']",
    "login_button": "xpath=//button[contains(@class, 'c-btn--primary')]",
    "create_campaign": "xpath=//span[text()='Create New Campaign']",
    "brochure_menu_generic": "xpath=//span[text()='{}']",
    "update_details": "xpath=//h3[text()='Update Your Details']",
    "title": "xpath=//select[@id='title_id']",
    "job_role": "xpath=//select[@id='job_role_id']",
    "country": "xpath=//select[@id='countrycode']",
    "company_activity": "xpath=//select[@id='cotype']",
    "change_details_button":
        "xpath=//form[@name='registrationForm']//input[@type='submit']",
    "company_type": "xpath=//select[@id='company_subtype_id']",
    "create_entry": "xpath=//a[text()='Create an entry']",
    "close": "xpath=//a[text()='Close']"

}


class DLAccountPage(AccountPage):

    def __init__(self, driver):
        super(DLAccountPage, self).__init__(driver=driver)

    def load_landing_page(self, data, system):
        self.driver.get(data['url'])
        self.wait_until_appear(loc["select_system"])
        if 'uat' in data['url']:
            if system == 'register':
                self.press(loc["system_generic"].format("Register"))
            elif system == 'awards':
                self.press(loc["system_generic"].format("Awards"))
            elif system == 'brochure':
                self.press(loc["system_generic"].format("Brochure Site"))
            else:
                print "you are in prod"
        return True

    def log_into_dl_account(self, data, system, account=None):
        site = 'dubai'
        assert self.load_landing_page(data, system)
        assert self.log_into_account(data, site, system, account)
        return True

    def create_dl_account(self, data, system):
        site = 'dubai'
        print data['email']
        assert self.load_landing_page(data, system)
        self.create_account(data, site, system)
        assert self.update_details(data)
        return True

    def update_details(self, data):
        self.wait_in_seconds(5)
        self.wait_until_appear(loc["update_details"], timeout=30)
        self.select(loc["title"], data['title'])
        self.select(loc["job_role"], data['job_role'])
        self.select(loc["country"], data['country2'])
        self.select(loc["company_activity"], data['comp_activity'])
        self.wait_until_appear(loc["company_type"])
        self.select(loc["company_type"], data['company_type'])
        if self.is_element_visible(loc["close"]):
            self.press(loc["close"])
        self.press(loc["change_details_button"])
        self.wait_until_appear(loc["create_entry"])
        self.press(loc["create_entry"])
        self.wait_until_appear(loc["create_campaign"])
        return self.is_element_visible(loc["create_campaign"])
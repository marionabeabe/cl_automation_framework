# -*- coding: utf-8 -*-
from BasePage import BasePage
import datetime

loc = {
    "pay_by_bank": "xpath=//input[@id='banktransfer_submit']",
    "pay_by_card": "xpath=//input[@id='creditcard_submit']",
    "bank_transfer_instructions":
        "xpath=//div[text()='Bank Transfer Instructions']",
    "invoice_info": "xpath=//div[text()='Invoice Information']",
    "company_name": "xpath=//input[@id='invoice_company_company_name']",
    "address1": "xpath=//input[@id='invoice_company_address1']",
    "address2": "xpath=//input[@id='invoice_company_address2']",
    "postcode": "xpath=//input[@id='invoice_company_address3']",
    "city": "xpath=//input[@id='invoice_company_city']",
    "country": "xpath=//select[@id='invoice_company_countrycode']",
    "email1": "xpath=//input[@id='invoice_company_email']",
    "confirm_payment": "xpath=//input[contains(@class, 'c-btn--primary')]",
    "sub_total_ui": "xpath=//td[@id='display_amount_notax']",
    "vat_ui": "xpath=//td[@id='display_amount_tax']",
    "total_ui": "xpath=//td[@id='display_amount']",
    "check_invoice_info": "xpath=//h3[text()='Check Invoice Information']",
    "check_invoice_details": "xpath=//*[text()='{}']",
    "confirm_invoice_info": "xpath=//a[text()='Confirm']",
    "payment_confirmation": "xpath=//h3[text()='Transaction Summary']",
    "next": "xpath=//input[@value='Next']",
    "card_details_heading": "xpath=//div[text()='Card Details']",
    "card_holder_name": "xpath=//input[@id='cardholder_name']",
    "cc_type": "xpath=//select[@id='cctype']",
    "cc_number": "xpath=//input[@id='ccnumber']",
    "security": "xpath=//input[@id='securitycode']",
    "month": "xpath=//select[@id='month']",
    "year": "xpath=//select[@id='year']",
    "billing_name": "xpath=//input[@id='billing_company_company_name']",
    "billing_address1": "xpath=//input[@id='billing_company_address1']",
    "billing_address2": "xpath=//input[@id='billing_company_address2']",
    "billing_city": "xpath=//input[@id='billing_company_city']",
    "billing_postcode": "xpath=//input[@id='billing_company_postcode']",
    "billing_country": "xpath=//select[@id='billing_company_countrycode']",
    "my_dashboard": "xpath=//span[contains(text(), 'My Dashboard')]",
    "new_campaign": "xpath=//span[text()='Create New Campaign']",
    "vat_number": "xpath=//input[@id='invoice_company_vat_number']",
    "cl_awards_total":
        "xpath=//td[text()='Total:']/..//td[contains(@class,'_price')]",
    "cl_awards_vat":
        "xpath=//span[@id='spTaxAmount']",
    "cl_awards_net":
        "xpath=//td[text()='Net Total:']/..//td[contains(@class,'_price')]",
    "sa_awards_net": "xpath=//span[@id='amountTotal']",
}

dl_prices = {
    'Entertainment': 650,
    'Creative Effectiveness': 650,
    'Design': 445,
    'Direct': 445,
    'Film':	445,
    'Film Craft': 445,
    'Healthcare': 445,
    'Innovation': 445,
    'Digital': 750,
    'Integrated': 445,
    'Media': 445,
    'Mobile': 445,
    'Outdoor':	445,
    'PR': 445,
    'Print & Outdoor Craft': 445,
    'Print & Publishing	': 445,
    'Brand Experience & Activation': 445,
    'Radio & Audio': 445,
    'Glass': 445

}

cl_prices = {
    'Film Lions': 865,
    'Print & Publishing Lions': 555,
    'Outdoor Lions': 555,
    'Radio & Audio Lions': 555,
    'Social & Influencer Lions': 555,
    'Direct Lions': 555,
    'Media Lions': 555,
    'Brand Experience & Activation Lions': 555,
    'Titanium Lions': 1525,
    'Design Lions': 555,
    'PR Lions': 555,
    'Film Craft Lions': 865,
    'Mobile Lions': 555,
    'Creative Effectiveness Lions': 1525,
    'Entertainment Lions': 865,
    'Innovation Lions' :555,
    'Pharma Lions': 555,
    'Health & Wellness Lions': 555,
    'Product Design Lions': 555,
    'Creative Data Lions': 555,
    'Glass: The Lion For Change': 555,
    'Digital Craft Lions': 555,
    'Entertainment Lions for Music': 555,
    'Industry Craft Lions': 555,
    'Sustainable Development Goals Lions': 555,
    'Creative eCommerce Lions': 555,
    'Sport': 555,
    'Creative Strategy': 555
}

sa_prices = {
    'Brand Experience & Activation': 645,
    'Creative eCommerce': 645,
    'Creative Effectiveness': 815,
    'Design': 525,
    'Digital': 645,
    'Direct': 525,
    'Digital Craft': 525,
    'Entertainment': 645,
    'Film': 645,
    'Film Craft': 645,
    'Glass: The Award For Change': 525,
    'Healthcare': 525,
    'Innovation': 645,
    'Integrated': 815,
    'Media': 525,
    'Mobile': 525,
    'Music': 525,
    'Outdoor': 525,
    'PR': 525,
    'Print & Outdoor Craft': 525,
    'Print & Publishing': 525,
    'Radio & Audio': 525,
}

cl_tier_1 = ['Titanium', 'Creative Effectiveness']
cl_tier_2 = ['Film', 'Entertainment', 'Film Craft']
cl_tier_3 = [
    'Print & Publishing', 'Outdoor', 'Radio & Audio', 'Social & Influencer',
    'Direct', 'Media', 'Brand Experience & Activation', 'Design', 'PR',
    'Mobile', 'Innovation', 'Pharma', 'Health & Wellness',
    'Product Design', 'Creative Data', 'Glass: The Lion For Change',
    'Entertainment Lions for Music', 'Entertainment Lions For Sport',
    'Industry Craft', 'Digital Craft', 'Sustainable Development Goals',
    'Creative eCommerce', 'Creative Strategy']

dl_tier_1 = ['Digital']
dl_tier_2 = ['Entertainment', 'Creative Effectiveness']
dl_tier_3 = [
    'Design', 'Direct', 'Film', 'Film Craft', 'Healthcare',
    'Innovation', 'Digital', 'Integrated', 'Media', 'Mobile', 'Outdoor', 'PR',
    'Print & Outdoor Craft', 'Print & Publishing',
    'Brand Experience & Activation', 'Radio & Audio', 'Glass']

sa_tier_1 = ['Creative Effectiveness', 'Integrated']
sa_tier_2 = ['Brand Experience & Activation', 'Creative eCommerce', 'Digital',
             'Entertainment', 'Film', 'Film Craft', 'Innovation']
sa_tier_3 = ['Design', 'Digital', 'Direct', 'Health', 'Media', 'Mobile',
             'Music', 'Outdoor', 'PR', 'Print & Outdoor Craft',
             'Print & Publishing', 'Radio & Audio',
             'Glass: The Award for Change', 'Digital Craft']

eu_countries = []


class OrderPage(BasePage):

    def add_invoicing_company(self, data, brand=None):
        assert self.is_element_visible(loc["invoice_info"])
        self.type(loc["company_name"], data['company'])
        self.type(loc["address1"], data['address1'])
        self.type(loc["address2"], data['address2'])
        self.type(loc["postcode"], data['postcode'])
        self.type(loc["city"], data['city'])
        self.select(loc["country"], data['country'])
        if brand == 'cl':
            self.type(loc["vat_number"], data['vat_number'])
        self.type(loc["email1"], data['email'])

    def process_totals(self, entries, brand=None, vat_free=None):
        tier_1_price = 750
        tier_2_price = 650
        tier_3_price = 445
        if brand == 'cl':
            tier_1_price = 1525
            tier_2_price = 865
            tier_3_price = 555
            cl_late_fee = 0
            current_date = datetime.date.today().strftime("%d-%B-%Y")
            current_date = current_date.split('-')
            if current_date[1] == 'March' and int(current_date[0]) >= 14:
                print "Late Fees 1 apply"
                cl_late_fee = 180  # 14 March
            elif current_date[1] == 'March' and int(current_date[0]) >= 28:
                print "Late Fees 2 apply"
                cl_late_fee = 260  # 28 March
            elif current_date[1] == 'April' and int(current_date[0]) <= 10:
                print "Late Fees 2 apply"
                cl_late_fee = 260  # 28 March
            elif current_date[1] == 'April' and int(current_date[0]) > 11 or \
                    current_date[1] == 'May' or current_date[1] == 'June':
                print "Late Fees 3 apply"
                cl_late_fee = 360  # 11 April
            print "current late fee: " + str(cl_late_fee)
            tier_1_price = 1525 + cl_late_fee
            tier_2_price = 865 + cl_late_fee
            tier_3_price = 555 + cl_late_fee
        elif brand == 'sa':
            sa_late_fee = 75
            tier_1_price = 815
            tier_2_price = 645
            tier_3_price = 525
            current_date = datetime.date.today().strftime("%d-%B-%Y")
            current_date = current_date.split('-')
            if current_date[1] in ['July', 'August', 'September'] and \
                    int(current_date[0]) >= 13:
                print "Late Fees 1 apply"
                tier_1_price = 815 + sa_late_fee
                tier_2_price = 645 + sa_late_fee
                tier_3_price = 525 + sa_late_fee
        total_price_of_tier_1_entries = tier_1_price * entries['tier_1']
        total_price_of_tier_2_entries = tier_2_price * entries['tier_2']
        total_price_of_tier_3_entries = tier_3_price * entries['tier_3']
        price = total_price_of_tier_1_entries + \
            total_price_of_tier_2_entries + \
            total_price_of_tier_3_entries
        print "Sub Total Price method : " + str(price)
        if brand == 'cl' and vat_free is None:
            vat = 0.2 * price
        elif brand == 'cl' and vat_free is True:
            vat = 0
        elif brand == 'sa':
            vat = 0
            sa_late_fee = 75
        else:
            vat = 0.05 * price
        print "Total Vat method: " + str(vat)
        current_date = datetime.date.today().strftime("%d-%B-%Y")
        current_date = current_date.split('-')
        dl_late_fee = 0
        if current_date[1] == 'February' and int(current_date[0]) > 21:
            print "Late Fees 1 apply"
            dl_late_fee = 150

        if brand == 'cl' and cl_late_fee != 0:
            price = price
            if brand == 'cl' and vat_free is None:
                vat = 0.2 * price
            elif brand == 'cl' and vat_free is True:
                vat = 0
            total = vat + price
        else:
            price = price + dl_late_fee
            total = vat + price
        print "Total Price method : " + str(total)
        if brand == 'cl':
            sub_total_from_ui = float(self.find_by_locator(
                loc["cl_awards_net"]).text.encode("ascii", "ignore"))
            if vat_free is not True:
                vat_from_ui = float(self.find_by_locator(
                    loc["cl_awards_vat"]).text.encode("ascii", "ignore"))
            total_from_ui = float(self.find_by_locator(
                loc["cl_awards_total"]).text.encode("ascii", "ignore"))
        elif brand == 'sa':
            total_from_ui = float(self.find_by_locator(
                loc["sa_awards_net"]).text)
            assert float(price) == total_from_ui
            return price
        else:
            sub_total_from_ui = float(((self.find_by_locator(
                loc["sub_total_ui"]).text).split(' ')[1]).replace(',', ''))
            vat_from_ui = float(((self.find_by_locator(
                loc["vat_ui"]).text).split(' ')[1]).replace(',', ''))
            total_from_ui = float(((self.find_by_locator(
                loc["total_ui"]).text).split(' ')[1]).replace(',', ''))
        print "Sub Total Price UI: " + str(sub_total_from_ui)
        print "Total Price UI: " + str(total_from_ui)
        print "Total VAT UI: " + str(vat_from_ui)
        assert float(price) == sub_total_from_ui
        assert vat == vat_from_ui
        assert total == total_from_ui
        return total

    def process_entries(self, entries, brand=None):
        tier_1_awards = 0
        tier_2_awards = 0
        tier_3_awards = 0
        tier_1_categories = 0
        tier_2_categories = 0
        tier_3_categories = 0
        for entry in entries:
            if brand == 'cl':
                if entry in cl_tier_1:
                    print entry + " is tier 1"
                    tier_1_awards += 1
                    new_tier_1_categories = len(entries[entry])
                    tier_1_categories = tier_1_categories + \
                                        new_tier_1_categories
                elif entry in cl_tier_2:
                    print entry + " is tier 2"
                    tier_2_awards += 1
                    new_tier_2_categories = len(entries[entry])
                    tier_2_categories = tier_2_categories + \
                                        new_tier_2_categories

                elif entry in cl_tier_3:
                    print entry + " is tier 3"
                    tier_3_awards += 1
                    new_tier_3_categories = len(entries[entry])
                    tier_3_categories = tier_3_categories + \
                                        new_tier_3_categories
            elif brand == 'sa':
                if entry in sa_tier_1:
                    print entry + " is tier 1"
                    tier_1_awards += 1
                    new_tier_1_categories = len(entries[entry])
                    tier_1_categories = tier_1_categories + \
                                        new_tier_1_categories
                elif entry in sa_tier_2:
                    print entry + " is tier 2"
                    tier_2_awards += 1
                    new_tier_2_categories = len(entries[entry])
                    tier_2_categories = tier_2_categories + \
                                        new_tier_2_categories
                elif entry in sa_tier_3:
                    print entry + " is tier 3"
                    tier_3_awards += 1
                    new_tier_3_categories = len(entries[entry])
                    tier_3_categories = tier_3_categories + \
                                        new_tier_3_categories

            else:
                if entry in dl_tier_1:
                    print entry + " is tier 1"
                    tier_1_awards += 1
                    new_tier_1_categories = len(entries[entry])
                    tier_1_categories = tier_1_categories + \
                                        new_tier_1_categories
                elif entry in dl_tier_2:
                    print entry + " is tier 2"
                    tier_2_awards += 1
                    new_tier_2_categories = len(entries[entry])
                    tier_2_categories = tier_2_categories + \
                                        new_tier_2_categories

                elif entry in dl_tier_3:
                    print entry + " is tier 3"
                    tier_3_awards += 1
                    new_tier_3_categories = len(entries[entry])
                    tier_3_categories = tier_3_categories + \
                                        new_tier_3_categories

        print "Tier 1 categories " + str(tier_1_categories)
        print "Tier 2 categories " + str(tier_2_categories)
        print "Tier 3 categories " + str(tier_3_categories)
        final_entries = {
            'tier_1': tier_1_categories,
            'tier_2': tier_2_categories,
            'tier_3': tier_3_categories
        }
        return final_entries

    def process_payment(self, pay_by, invoice_data, entries,
                        credit_card_data=None, cc_type=None, brand=None):
        if pay_by == 'bank_transfer':
            self.press(loc["pay_by_bank"])
            self.wait_until_appear(loc["bank_transfer_instructions"])
            assert self.is_element_visible(loc["bank_transfer_instructions"])
        elif pay_by == 'credit_card':
            self.press(loc["pay_by_card"])

        self.add_invoicing_company(invoice_data, brand=brand)
        self.process_totals(entries, brand=brand)
        if pay_by == 'credit_card':
            self.press(loc["next"])
        else:
            self.press(loc["confirm_payment"])
        assert self.is_element_visible(loc["check_invoice_info"])
        del invoice_data['email']
        del invoice_data['vat_number']
        for key in invoice_data.keys():
            assert self.is_element_visible(
                loc["check_invoice_details"].format(invoice_data[key]))
        self.press(loc["confirm_invoice_info"])
        if pay_by == 'credit_card':
            data = credit_card_data['type'][cc_type]
            self.type(loc["card_holder_name"], credit_card_data['card_holder'])
            self.select(loc["cc_type"], cc_type)
            self.type(loc["cc_number"], data["cc_number"])
            self.type(loc["security"], data['security'])
            self.select(loc["month"], data['month'])
            self.select(loc["year"], data['year'])
            self.type(loc["billing_name"], credit_card_data['billing_name'])
            self.type(loc["billing_address1"], credit_card_data['address1'])
            self.type(loc["billing_address2"], credit_card_data['address2'])
            self.type(loc["billing_city"], credit_card_data['city'])
            self.type(loc["billing_postcode"], credit_card_data['postcode'])
            self.select(loc["billing_country"], credit_card_data['country'])
            self.press(loc["confirm_payment"])
        self.wait_until_appear(loc["payment_confirmation"], timeout=100)
        assert self.is_element_visible(loc["payment_confirmation"])
        self.press(loc["my_dashboard"])
        self.wait_until_appear(loc["new_campaign"])
        return True


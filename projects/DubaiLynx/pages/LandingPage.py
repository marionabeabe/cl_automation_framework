from projects.CannesLions.pages.AccountPage import AccountPage
loc = {
    "select_system": "xpath=//h3[text()='Select your system:']",
    "system_generic": "xpath=//a[text()='{}']",
    "register_heading": "xpath=//h1[text()='Buy Passes']",
    "login_button": "xpath=//button[contains(@class, 'c-btn--primary')]",

}


class LandingPage(AccountPage):

    def __init__(self, driver):
        super(LandingPage, self).__init__(driver=driver)

    def load_landing_page(self, data, system):
        self.driver.get(data['url'])
        self.wait_until_appear(loc["select_system"])
        if 'uat' in data['url']:
            print system
            if system == 'register':
                self.press(loc["system_generic"].format("Register"))
                self.wait_until_appear(loc["register_heading"])
                return self.is_element_visible(loc["register_heading"])
            elif system == 'awards':
                self.press(loc["system_generic"].format("Awards"))
                self.wait_until_appear(loc["login_button"])
                return self.is_element_visible(loc["register_heading"])
            elif system == 'brochure':
                self.press(loc["system_generic"].format("Brochure Site"))

from BasePage import BasePage
import re
from selenium.common.exceptions import NoSuchElementException
from projects.SpikesAsia.fixtures.AwardsCategoriesData import \
    extra_chars_categories

loc = {
    "create_campaign": "xpath=//span[text()='Create New Campaign']",
    "create_campaign_three_dots": "xpath=//div[text()='Add campaign']",
    "entry_heading": "xpath=//h3[text()='Entry Title']",
    "entry_title": "xpath=//input[@name='title']",
    "brand_heading": "xpath=//input[@name='clientcontactcompanyname']",
    "product": "xpath=//input[@name='product']",
    "sector": "xpath=//select[@id='sector']",
    "sub_sector": "xpath=//select[@name='sector_sub_id']",
    "save_button": "xpath=//button[@name='submit']",
    "save_and_choose_categories":
        "xpath=//input[@value='Save and Choose Categories']",
    "create_entries_button": "xpath=//input[@value='Create Entries']",
    "back_button": "xpath=//input[@value='Back']",
    "campaign_title": "xpath=//div[contains(text(), '{}')]",
    "add_entry_to_campaign": '''xpath=//div[text()="Add entry to '{}'"]''',
    "awards_heading": "xpath=//h3[text()='Choose Awards & Categories']",
    "cl_awards_heading": "xpath=//h3[contains(text(), 'Choose')]",
    "awards_options": "xpath=//label[contains(text(), '{}')]",
    "categories_heading": "xpath=//h3[text()='Choose Awards & Categories']",
    "category_options": "xpath=//*[text()='{}']",
    "edit_category": "xpath=//div[contains(text(),'{}')]/../../..//a[contains(@href, '{}')]",
    "edit_mult_category":
        "xpath=//th[contains(text(), '{}')]/../../..//td[contains("
        "@data-title,'{}')]/..//a[contains(@href, '{}')]",
    "mult_three_dots": "xpath=//th[contains(text(), '{}')]/../../..//td[contains("
                       "@data-title,'{}')]/..//span[contains("
                       "@class,'c-dropdown__header')]",
    "categories_generic_heading": "xpath=//h3[contains(text(), '{}')]",
    "add_company_button": "xpath=//a[contains(@class, 'c-btn--primary')]//span[text()='Add company']",
    "company_name": "xpath=//input[@name='company_name']",
    "company_activity": "xpath=//select[@name='cotype']",
    "company_type": "xpath=//select[@id='awards_company_subtype_id']",
    "address1": "xpath=//input[@name='address1']",
    "address2": "xpath=//input[@name='address2']",
    "address3": "xpath=//input[@name='address3']",
    "city": "xpath=//input[@name='city']",
    "postcode": "xpath=//input[@name='postcode']",
    "country": "xpath=//select[@name='countrycode']",
    "entrant_company": "xpath=//strong[text()='Entrant Company']",
    "idea_creation": "xpath=//strong[text()='Idea Creation']",
    "pr_prod_media": "xpath=//strong[text()='{}']",
    "no_other_roles": "xpath=//h4[contains(text(), 'No Other Roles')]",
    "company_added": "xpath=//a[text()='{}']",
    "save_entry": "xpath=//a[contains(@href, 'entry')]",
    "save_media_entry": "xpath=//input[@value='Save']",
    "save_contact_entry": "xpath=//button[contains(text(),'Save')]",
    "written_save": "xpath=//button[@name='submit']",
    "category_check":
        "xpath=//a[contains(@href, '{}')]//span[contains(@class, '-checked')]",
    "multiple_category_check":
        "xpath=//td[@data-title='{}']/..//a[contains(@href, '{}')]//span["
        "contains(@class, '-checked')]",
    "multiple_category_check_for_spaces":
        "xpath=//td[contains(@data-title, '{}')]/..//a[contains(@href, '{}')]//span["
        "contains(@class, '-checked')]",
    "contact_name": "xpath=//input[@id='JudgingContactName']",
    "contact_title": "xpath=//input[@id='JudgingJobTitle']",
    "contact_mobile": "xpath=//input[@id='JudgingTelephone']",
    "contact_email": "xpath=//input[@id='JudgingEmail']",
    "client_name": "xpath=//input[@id='ClientContactName']",
    "client_title": "xpath=//input[@id='ClientContactJobTitle']",
    "client_mobile": "xpath=//input[@id='ClientContactTelephone']",
    "client_email": "xpath=//input[@id='ClientContactEmail']",
    "client_city": "xpath=//input[@id='ClientContactCity']",
    "client_country": "xpath=//select[@id='ClientContactCountrycode']",
    "media_placement":
        "xpath=//label[contains(text(), 'Placement 1 (required)')]",
    "media_element": "xpath=//input[@id='NumberTypeOfMedia_1']",
    "media_location": "xpath=//input[@id='MediaPlacement_1']",
    "media_airings": "xpath=//input[@id='ChannelUsageCount_1']",
    "media_startdate": "xpath=//input[@id='DateOf1stImplementation_1']",
    "media_end_date": "xpath=//input[@id='DateOf1stImplementationTo_1']",
    "media_country": "xpath=//input[@id='ChannelCountries_1']",
    "cl_media_country":
        "xpath"
        "=//select[@id='ChannelCountries_1']//option[contains(text(),'{}')]",
    "written_exp_why_work": "xpath=//textarea[@id='synopsis_72']",
    # idea, development process, innovation/tech, expectations
    "written_exp_background":
        "xpath=//label[contains(text(), 'Background')]/..//textarea",
    "written_exp_designed":
        "xpath=//label[contains(text(), 'designed')]/..//textarea",
    "written_exp_behaviour":
        "xpath=//label[contains(text(), 'behaviour')]/..//textarea",
    "written_exp_challenged":
        "xpath=//label[contains(text(), 'challenged')]/..//textarea",
    "written_exp_innovation":
        "xpath=//label[contains(text(), 'Innovation')]/..//textarea",
    "written_exp_what_happens_in_film":
        "xpath=//label[contains(text(), 'what happens in')]/..//textarea",
    "written_exp_creative_idea":
        "xpath=//label[contains(text(), 'creative idea')]/..//textarea",
    "written_exp_innovative_idea":
        "xpath=//label[contains(text(), 'idea')]/..//textarea",
    "written_exp_product_service":
        "xpath=//label[contains(text(), 'product/service')]/..//textarea",
    "written_exp_development":
        "xpath=//label[contains(text(), 'development')]/..//textarea",
    "written_exp_innovation1":
        "xpath=//label[contains(text(), 'innovation')]/..//textarea",
    "written_exp_expectations":
        "xpath=//label[contains(text(), 'expectations')]/..//textarea",
    "written_exp_full_film_script":
        "xpath=//label[contains(text(), 'full film script')]/..//textarea",
    "written_exp_strategy":
        "xpath=//label[contains(text(), 'strategy')]/..//textarea",
    "written_exp_information":
        "xpath=//label[contains(text(), 'Please provide')]/..//textarea",
    "written_context_information":
        "xpath=//label[contains(text(), 'Cultural/Context information "
        "for the jury. ')]/..//textarea",
    "written_interactive":
        "xpath=//label[contains(text(), 'interactive')]/..//textarea",
    "written_poster_billboard":
        "xpath=//label[contains(text(), 'poster')]/..//textarea",
    "written_translation":
        "xpath=//label[contains(text(), 'Translation')]/..//textarea",
    "written_publishing":
        "xpath=//label[contains(text(), ' medium')]/..//textarea",
    "written_exp_execution":
        "xpath=//label[contains(text(), 'execution')]/..//textarea",
    "written_exp_cinematography":
        "xpath=//label[contains(text(), 'cinematography')]/..//textarea",
    "written_exp_production_design":
        "xpath=//label[contains(text(), 'production design')]/..//textarea",
    "written_exp_results":
        "xpath=//label[contains(text(), 'results')]/..//textarea",
    "written_exp_insight":
        "xpath=//label[contains(text(), 'Insight')]/..//textarea",
    "written_exp_Cultural":
        "xpath=//label[contains(text(), 'Cultural')]/..//textarea",
    "written_exp_Creative":
        "xpath=//label[contains(text(), 'Creative')]/..//textarea",
    "written_pr_effectiveness":
        "xpath=//label[contains(text(), 'effectiveness of the')]/..//textarea",
    "written_exp_Outcome":
        "xpath=//label[contains(text(), 'Outcome')]/..//textarea",
    "written_exp_interpretation":
        "xpath=//label[contains(text(), 'Interpretation')]/..//textarea",
    "written_exp_about_the_edit":
        "xpath=//label[contains(text(), 'about the edit')]/..//textarea",
    "written_exp_music_track":
        "xpath=//label[contains(text(), 'music track')]/..//textarea",
    "written_exp_sound_design":
        "xpath=//label[contains(text(), 'sound design')]/..//textarea",
    "written_exp_animation":
        "xpath=//label[contains(text(), 'animation used')]/..//textarea",
    "written_exp_visual_effects":
        "xpath=//label[contains(text(), 'visual effects')]/..//textarea",
    "written_exp_achievements":
        "xpath=//label[contains(text(), 'achievements of the')]/..//textarea",
    "written_exp_casting_process":
        "xpath=//label[contains(text(), 'casting process')]/..//textarea",
    "written_why_relevant":
        "xpath=//label[contains(text(), "
        "'Why is this work relevant for')]/..//textarea",
    "written_the_results":
        "xpath=//label[contains(text(), 'the results')]/..//textarea",
    "written_creative_use":
        "xpath=//label[contains(text(), 'creative use')]/..//textarea",
    "written_the_budget":
        "xpath=//label[contains(text(), 'budget')]/..//textarea",
    "written_unwanted_star":
        "xpath=//label[contains(text(), '*')]/..//textarea",
    "written_outcome":
        "xpath=//label[contains(text(), 'Describe the outcome')]/..//textarea",
    "written_advancements":
        "xpath=//label[contains(text(), 'advancements')]/..//textarea",
    "written_cl_campaign_output":
        "xpath=//label[contains(text(), 'campaign output')]/..//textarea",
    "written_health_restrictions":
        "xpath=//label[contains(text(), 'restrictions')]/..//textarea",
    "written_health_target":
        "xpath=//label[contains(text(), 'target')]/..//textarea",
    "written_full_radio_advert":
        "xpath=//label[contains(text(), 'full radio advert')]/..//textarea",
    "written_medium_radio_audio":
        "xpath=//label[contains(text(), 'radio/audio')]/..//textarea",
    "written_what_happens_in_radio_advert":
        "xpath=//label["
        "contains(text(), 'what happens in the radio advert')]/..//textarea",
    "written_exp_conf_info":
        "xpath=//label[contains("
        "text(), 'information for the jury')]/..//textarea",
    "team_name": "xpath=//input[@id='name']",
    "cl_team_first_name": "xpath=//input[@name='firstname']",
    "cl_team_last_name": "xpath=//input[@name='lastname']",
    "team_company": "xpath=//input[@id='company']",
    "team_position": "xpath=//input[@id='position']",
    "team_role": "xpath=//input[@id='roleOnEntry']",
    "team_email": "xpath=//input[@id='email']",
    "team_values_assert": "xpath=//*[contains(text(), '{}')]",
    "media_upload_heading": "xpath=//h3[contains(text(),'Upload')]",
    "media_checkbox":
        "xpath=//*[contains(@class, 'e-form__field e-form__field--checkbox')]",
    "media_checkbox_continue": "xpath=//a[text()='Continue']",
    "media_upload_file": "xpath=//a[contains(text(), 'Upload New File')]",
    "special_media_upload_file": "xpath=//a[@data-fileextensionlist='{}']",
    "add_existing_media": "xpath=//a[contains(text(), 'Use existing media')]",
    "add_to_entry": "xpath=//span[text()='Add to entry']",
    "add_to_entry_specific_file": "xpath=//img[@alt='{}']/../../..//*["
                                  "contains(text(), 'Add to entry')]",
    "upload_area": "//*[contains(@id,'dropzone')]",
    "upload_area2": "xpath=//*[contains(@id,'dropzone')]",
    "remove_file": "xpath=//a[text()='Remove file']",
    "upload_length": "xpath=//input[@id='length']",
    "upload_file_button": "xpath=//button[text()='Upload']",
    "upload_complete": "xpath=//*[text()='Upload Complete']",
    "file_processing": "xpath=//li[text()='File processing.']",
    "view_media": "xpath=//a[text()='View media']",
    "upload_iframe": "//iframe[@id='fs-picker-frame']",
    "add_to_cart": "xpath=//span[text()='Add to cart']",
    "cat_add_to_cart":
        "xpath=//td[@data-title='{}']/..//span[text()='Add to cart']",
    "cat_add_to_cart_spaces":
        "xpath=//td[contains(@data-title,'{}')]/..//span[text()='Add to cart']",
    "add_url": "xpath=//label[contains(text(), 'Add URL')]",
    "input_url": "xpath=//input[@name='url_1']",
    "select_url_status": "xpath=//select[@name='urlType_1']",
    "award_and_category":
        "xpath=//h3[contains(text(), '{}')]/../../..//strong[text()='{}'"
        "]/../..//*[normalize-space()='{}']",
    "before_you_start": "xpath=//h2[text()='Before you get started...']",
    "yes_to_before": "xpath=//label[text()='Yes, I want this entry to include "
                     "multiple executions']",
    "no_to_before": "xpath=//label[text()='No, this entry is a standalone"
                    " piece of work']",
    "continue_button": "xpath=//button[text()='Continue']",
    "media_placement_company": "xpath=//h4[text()='{}']/../..//label[contains("
                       "text(), 'I confirm that no company fulfilled the Media "
                               "Placement Role')]",
    "confirm_button": "xpath=//input[@value='Confirm']",
    "health_sub_category": "xpath=//h4[contains(text(), '{}')]",
    "health_sub_category_category":
        "xpath=//h4[contains(text(), '{}')]/..//label[normalize-space()='{}']",
    "health_mult_edit":
        "xpath=//div[contains(text(), '{}')]/../../..//a["
        "contains(@href, '{}')]",
    "health_category_check":
        "xpath=//div[contains(@data-title,'{}')]/../../..//a[contains("
        "@href, '{}')]//span[contains(@class, '-checked')]",
    "optional_materials": "xpath=//span[text()='Optional Materials']",
    "health_add_to_cart":
        "xpath=//*[normalize-space()='{}']/../../..//span["
        "text()='Add to cart']",
    "limit_reached": "xpath=//h2[text()='Limit Reached']",
    "alert_ok": "xpath=//a[text()='Ok']",
    "close": "xpath=//a[text()='Close']",
    "upload_close": "xpath=//*[contains(text(),'You currently do not')]",
    "error_reference": "xpath=//p[contains(text(), 'Error Reference')]",
    "file_not_uploaded": "xpath=//div[contains(text(), 'You currently')]",
    "loading": "xpath=//strong[text()='Loading...']",
    "health_company_fill":
        "xpath=//th[contains(text(), 'Health & Wellness')]/../../../..//*["
        "@data-title='A01 OTC Oral Medicines - Brand Experience & Activation'"
        "]/../../..//a[contains(@href, 'company/default/entryid')]",
    "health_contact_fill":
        "xpath=//th[contains(text(), 'Health & Wellness')]/../../../..//*["
        "@data-title='A01 OTC Oral Medicines - Brand Experience & Activation'"
        "]/../../..//a[contains(@href, 'contacts/default/entryid')]",
    "alert_campaign_title": "xpath=//input[@id='campaignTitle']",
    "alert_campaign_entry": "xpath=//input[@id='entryTitle']",
    "alert_campaign_title_heading": "xpath=//div[contains(text(),'{}')]",
    "alert_campaign_entry_heading": "xpath=//div[contains(text(),'{}')]",
    "campaign_three_button": "xpath=//div[contains(text(),'{}')]/..//span["
                             "contains(@class,'dropdown__header')]",
    "delete_campaign":
        "xpath=//a[@data-title='{}']//div[contains(text(), 'Delete campaign')]",
    "edit_campaign":
        "xpath=//div[@data-title='{}']//div[contains(text(), 'Edit title')]",
    "delete_agree":
        "xpath=//label[contains(text(), 'I agree to delete this campaign')]",
    "delete_button": "xpath=//button[text()='Delete']",
    "client_less": "xpath=//label[contains(text(), 'This entry does not have')]"


}
health_sub_cats = [
    'OTC Medicines', 'OTC Applications', 'OTC Products/Devices',
    'Nutraceuticals', 'Health & Wellness', 'Brand-led Education & Awareness',
    'Public Education & Awareness', 'Fundraising & Advocacy',
    'Corporate Image & Communication', 'Services & Facilities', 'Insurance',
    'Animal Health']
cl_health_sub_cats = [
    'OTC Oral Medicines', 'OTC Applications',
    'OTC Products / Devices', 'Nutraceuticals', 'Health & Wellness Tech',
    'Brand-led Education & Awareness',
    'Non-profit / Foundation-led Education & Awareness',
    'Fundraising & Advocacy', 'Corporate Image & Communication',
    'Health Services & Facilities', 'Insurance', 'Animal Health']
cl_pharma_sub_cats = [
    'Regulated: Direct To Customer', 'Regulated: Direct To Patient',
    'Regulated: Healthcare Professional', 'Non-regulated: Direct To Customer',
    'Non-regulated: Direct To Patient',
    'Non-regulated: Healthcare Professional', 'Regulated', 'Non-regulated',
    'Regulated', 'Non-Regulated', 'Regulated', 'Non-Regulated', 'Regulated',
    'Non-Regulated'
]
cl_health_cats_mapping = {
    'OTC Oral Medicines': 'A01 OTC Oral Medicines',
    'OTC Applications': 'A02 OTC Applications',
    'OTC Products / Devices': 'A03 OTC Products / Devices',
    'Nutraceuticals': 'A04 Nutraceuticals',
    'Health & Wellness Tech': 'A05 Health & Wellness Tech',
    'Brand-led Education & Awareness': 'B01 Brand-led Education & Awareness',
    'Non-profit / Foundation-led Education & Awareness':
        'B02 Non-profit / Foundation-led Education & Awareness',
    'Fundraising & Advocacy': 'B03 Fundraising & Advocacy',
    'Corporate Image & Communication': 'C01 Corporate Image & Communication',
    'Health Services & Facilities': 'C02 Health Services & Facilities',
    'Insurance': 'C03 Insurance',
    'Animal Health': 'D01 Animal Health'}

health_cats_mapping = {'OTC Medicines': 'A01 OTC Medicines',
                       'OTC Applications': 'A02 OTC Applications',
                       'OTC Products/Devices': 'A03 OTC Products/Devices',
                       'Nutraceuticals': 'A04 Nutraceuticals',
                       'Health & Wellness': 'A05 Health & Wellness',
                       'Brand-led Education & Awareness':
                           'B01 Brand-led Education & Awareness',
                       'Public Education & Awareness':
                           'B02 Public Education & Awareness',
                       'Fundraising & Advocacy': 'B03 Fundraising & Advocacy',
                       'Corporate Image & Communication':
                           'C01 Corporate Image & Communication',
                       'Services & Facilities': 'C02 Services & Facilities',
                       'Insurance': 'C03 Insurance',
                       'Animal Health': 'D01 Animal Health'}

extra_alerts = [
                'Film', 'Film Craft', 'Outdoor',
                'Print & Outdoor Craft',
                'Print & Publishing', 'Radio & Audio',
                'Film: Cinema, TV and Digital Film Content',
                'Film Craft: Art Direction / Production Design',
                'Film Craft: Direction',
                'Film Craft: Script',
                'Film Craft: Use of Music / Sound Design',
                'Film Craft: Animation / Visual Effects',
                'Film Craft: Cinematography',
                'Standard Outdoor',
                'Standard Print',
                'Print & Outdoor Craft: Art Direction',
                'Print & Outdoor Craft: Copywriting',
                'Print & Outdoor Craft: Illustration',
                'Print & Outdoor Craft: Photography',
                'Print & Outdoor Craft: Typography',
                'Radio Craft: Script',
                'Radio Craft: Use of Music / Sound Design',
                ]

cl_extra_alerts = ['Design', 'Mobile', 'Direct', 'Outdoor', 'Digital Craft',
                   'Industry Craft', 'Film Craft',
                   'Sustainable Development Goals',
                   'Glass: The Lion For Change',
                   'Entertainment', 'Entertainment Lions For Sport',
                   'Innovation', 'Media', 'PR', 'Social & Influencer'
                   ]

sa_extra_alerts = ['Digital']

extra_space_categories = [
    'A07 New Realities', 'A02 Video / Moving Image', 'A04 Music / Sound Design',
    'A05 Overall Aesthetic Design (incl. UI)', 'A06 UX & Journey Design',
    'A07 Experience Design: Multi-platform', 'A08 Experience Design: Voice',
    'E03 Other FMCG & Consumer Durables',

                          ]


class CampaignPage(BasePage):

    def __init__(self, driver):
        super(CampaignPage, self).__init__(driver=driver)

    def create_campaign(self, data, brand=None, client_less=None):
        assert self.is_element_visible(loc["create_campaign"])
        if brand == 'cl' or brand == 'sa':
            try:
                self.wait_in_seconds(2)
                self.press(loc["close"])
            except NoSuchElementException:
                pass
        self.press(loc["create_campaign"])
        self.wait_until_appear(loc["entry_heading"])
        self.type(loc["entry_title"], data['title'])
        if client_less:
            self.press(loc["client_less"])
        else:
            self.type(loc["brand_heading"], data['brand'])
        self.type(loc["product"], data["product"])
        self.select(loc["sector"], data['sector'])
        self.select(loc["sub_sector"], data['sub_sector'])
        self.wait_in_seconds(2)
        self.press(loc["save_button"])
        self.wait_in_seconds(2)
        self.wait_until_appear(loc["campaign_title"].format(data['title']))
        return data['title'], self.is_element_visible(
            loc["add_entry_to_campaign"].format(data['title']))

    def modify_campaign(self, campaign, modify, data, fill_brand=None):
        self.press(loc["campaign_three_button"].format(campaign))
        self.wait_until_appear(loc["edit_campaign"].format(campaign))
        self.press(loc["edit_campaign"].format(campaign))
        self.wait_until_appear(loc["save_button"])
        if type(modify) is str:
            if modify == 'title':
                self.clear_and_type(loc["entry_title"], data['title'])
            elif modify == 'client_less':
                self.press(loc["client_less"])
                if fill_brand:
                    self.type(loc["brand_heading"], data['brand'])
            elif modify == 'product':
                self.clear_and_type(loc["product"], data['product'])
            elif modify == 'sector':
                self.select(loc["sector"], data['sector'])
            elif modify == 'sub_sector':
                self.select(loc["sub_sector"], data['sub_sector'])
        elif type(modify) is list:
            for change in modify:
                if change == 'title':
                    self.clear_and_type(loc["entry_title"], data['title'])
                elif change == 'client_less':
                    self.press(loc["client_less"])
                    if fill_brand:
                        self.type(loc["brand_heading"], data['brand'])
                elif change == 'product':
                    self.clear_and_type(loc["product"], data['product'])
                elif change == 'sector':
                    self.select(loc["sector"], data['sector'])
                elif change == 'sub_sector':
                    self.select(loc["sub_sector"], data['sub_sector'])
        self.press(loc["save_button"])
        self.wait_in_seconds(2)
        if modify == 'title' or 'title' in modify:
            return data['title']
        # self.wait_until_appear(loc["campaign_title"].format(data['title']))
        # return data['title'], self.is_element_visible(loc["create_campaign"])
        return True

    def delete_campaign(self, campaign):
        self.press(loc["campaign_three_button"].format(campaign))
        self.wait_until_appear(loc["delete_campaign"].format(campaign))
        self.wait_in_seconds(2)
        self.press(loc["delete_campaign"].format(campaign))
        self.wait_until_appear(loc["delete_agree"])
        assert self.is_element_visible(loc["delete_agree"])
        self.press(loc["delete_agree"])
        self.press(loc["delete_button"])
        self.wait_until_appear(loc["create_campaign"])
        self.wait_in_seconds(2)
        assert not self.is_element_visible(loc["add_entry_to_campaign"].format(
            campaign))
        return True

    def create_campaign_through_three_dots(self, award, category, yes_no=None,
                                           campaign=None, campaign_data=None):
        self.press(loc["mult_three_dots"].format(award, category))
        self.wait_until_appear(loc["create_campaign_three_dots"])
        assert self.is_element_visible(loc["create_campaign_three_dots"])
        self.wait_in_seconds(2)
        self.press(loc["create_campaign_three_dots"])
        self.wait_in_seconds(2)
        self.handle_extra_alerts(yes_no=yes_no, campaign=campaign,
                                 campaign_data=campaign_data)
        self.wait_until_appear(loc["alert_campaign_title_heading"].
                               format(campaign_data['campaign_title']))
        # assert self.is_element_visible(loc["alert_campaign_entry_heading"].
        #                                format(campaign_data['entry_title']))
        assert self.is_element_visible(loc["alert_campaign_title_heading"].
                                       format(campaign_data['campaign_title']))
        return True

    def handle_extra_alerts(self, yes_no=None, campaign=None, campaign_data=None):
        try:
            self.wait_in_seconds(1)
            if self.is_element_visible(loc["before_you_start"],
                                       screenshot=False):
                self.wait_in_seconds(2)
                print "Extra Alert Found"
                if yes_no:
                    self.press(loc["yes_to_before"])
                    if campaign:
                        self.wait_until_appear(loc["alert_campaign_entry"])
                        self.type(loc["alert_campaign_title"],
                                  campaign_data['campaign_title'])
                        self.type(loc["alert_campaign_entry"],
                                  campaign_data['entry_title'])
                else:
                    self.press(loc["no_to_before"])
                self.wait_in_seconds(1)
                self.press(loc["continue_button"])
                self.wait_until_appear(loc["create_campaign"])
        except NoSuchElementException:
            pass

    def create_entry(self, campaign, awards, categories, entries=None,
                     brand=None):
        self.press(loc["add_entry_to_campaign"].format(campaign))
        if brand == 'cl' or brand == 'sa':
            assert self.is_element_visible(loc["cl_awards_heading"])
        else:
            assert self.is_element_visible(loc["awards_heading"])
        if type(awards) is str:
            self.press(loc["awards_options"].format(awards))
        elif type(awards) is list:
            for award in awards:
                print "Selecting " + award + " Award"
                self.press(loc["awards_options"].format(award))
            self.wait_in_seconds(1)
            try:
                if self.is_element_visible(loc["limit_reached"]):
                    self.press(loc["alert_ok"])
            except NoSuchElementException:
                pass
        self.wait_until_appear(loc["save_and_choose_categories"])
        self.press(loc["save_and_choose_categories"])

        if brand == 'cl' or brand == 'sa':
            self.wait_in_seconds(3)
            try:
                if self.is_element_visible(loc["error_reference"]):
                    print self.find_by_locator(loc["error_reference"]).text
                    self.screen_shot()
                    return
            except NoSuchElementException:
                pass

            assert self.is_element_visible(loc["cl_awards_heading"])
        else:
            assert self.is_element_visible(loc["awards_heading"])
        regex = re.compile(r'(\b[a-zA-Z]\d{2}\b)')

        if brand == 'cl':
            health_sub_cats = cl_health_sub_cats
            health_cats_mapping = cl_health_cats_mapping

        if entries is not None:
            for award in sorted(entries.keys()):
                if 'M_' in award:
                    award = award.split('_')[1]
                if award in awards:
                    if award == 'Healthcare' or award == 'Health & Wellness':
                        for sub_cat in health_sub_cats:
                            print "Selecting: " + sub_cat
                            self.press(loc["health_sub_category"].format(
                                sub_cat))
                            for cat in entries[award]:
                                print "Selecting: " + sub_cat + " " + cat
                                self.press(
                                    loc["health_sub_category_category"].format(
                                        sub_cat, cat))

                    elif award == 'Glass' or award == 'Integrated' or \
                            award == 'Titanium' or \
                            award == 'Glass: The Lion For Change':
                        print "Default category is already  selected"
                    else:
                        # if award == 'Creative eCommerce':
                        #     award = 'M_' + award
                        for cat in entries[award]:
                            if 'M_' in award:
                                award = award.split('_')[1]
                            print award + " : " + cat
                            regex_pattern = regex.findall(cat)[0]
                            category = cat.strip(regex_pattern)
                            self.press(loc["award_and_category"].format(
                                award, regex_pattern, cat))
                            # #self.wait_in_seconds(0.5)
            self.press(loc["create_entries_button"])
            assert self.is_element_visible(
                loc["campaign_title"].format(campaign))
            return True
        if type(categories) is str:
            self.press(loc["category_options"].format(categories))
        elif type(categories) is list:
            for category in categories:
                cat = regex.findall(category)[0]
                category = category.strip(cat)
                print "Selecting" + category + " category"
                self.press(loc["category_options"].format(category))
        self.press(loc["create_entries_button"])
        assert self.is_element_visible(loc["campaign_title"].format(campaign))
        return True

    def fill_campaign_details(self, data, campaign, entries=None, award=None,
                              brand=None, selector=None, cat=None):
        data['company']['campaign'] = campaign
        data['contact']['campaign'] = campaign
        data['media_info']['campaign'] = campaign
        data['explanation']['campaign'] = campaign
        data['creative_team']['campaign'] = campaign
        data['media_upload']['campaign'] = campaign
        selector = selector

        def do_fill_company_details(award=award):
            print "Filling Company Details"
            if type(entries) is dict:
                category = None
                award = None
                for award in entries:
                    award = award
                    for cat in entries[award]:
                        category = cat
                        break
                    break
                if award == 'Health & Wellness':
                    self.wait_until_disappear(loc["loading"])
                    self.wait_until_appear(loc["health_company_fill"])
                    self.press(loc["health_company_fill"])
                else:
                    self.wait_in_seconds(2)
                    self.wait_until_appear(loc["edit_mult_category"].format(
                                    award, category, 'company/default/entryid'))
                    self.press(loc["edit_mult_category"].format(
                                    award, category, 'company/default/entryid'))
            else:
                award = award
                self.wait_until_appear(loc["edit_category"].format(
                    selector, 'company/default/entryid'))
                self.press(loc["edit_category"].format(
                    selector, 'company/default/entryid'))
            if award in extra_alerts or award in cl_extra_alerts \
                    or award in sa_extra_alerts:
                print award + " in extra alerts"
                handle_extra_alerts(cat='cl')
            if brand == 'cl' or brand == 'sa':
                handle_extra_alerts(cat='cl')
            self.wait_until_appear(loc["categories_generic_heading"].format(
                "Company Credits"))
            assert self.add_company(data['company'])
            self.wait_until_appear(
                loc["category_check"].format('company/default/entryid'))
            assert self.is_element_visible(
                loc["category_check"].format('company/default/entryid'))

        def do_fill_contact_details():
            print "Filling Contact Details"
            if type(entries) is dict:
                category = None
                award = None
                for award in entries:
                    award = award
                    for cat in entries[award]:
                        category = cat
                        break
                    break
                if award == 'Health & Wellness':
                    self.wait_until_disappear(loc["loading"])
                    self.wait_until_appear(loc["health_contact_fill"])
                    self.press(loc["health_contact_fill"])
                else:
                    self.wait_in_seconds(2)
                    self.wait_until_appear(loc["edit_mult_category"].format(
                                    award, category, 'contacts/default/entryid'))
                    self.press(loc["edit_mult_category"].format(
                                    award, category, 'contacts/default/entryid'))
            else:
                self.wait_in_seconds(2)
                self.wait_until_appear(loc["edit_category"].format(
                    selector, 'contacts/default/entryid'))
                self.press(loc["edit_category"].format(
                    selector, 'contacts/default/entryid'))
            # self.press(loc["edit_mult_category"].format(
            #                     award, category, 'contacts/default/entryid'))
            # self.wait_until_appear(loc["categories_generic_heading"].format(
            #     "Contacts"))
            assert self.add_contacts(data['contact'])
            self.wait_until_appear(
                loc["category_check"].format('contacts/default/entryid'))
            assert self.is_element_visible(
                loc["category_check"].format('contacts/default/entryid'))

        def handle_extra_alerts(cat=None, locator=None):
            try:
                self.wait_in_seconds(1)
                if self.is_element_visible(loc["before_you_start"],
                                           screenshot=False):
                    self.wait_in_seconds(2)
                    print "Extra Alert Found"
                    self.press(loc["no_to_before"])
                    self.wait_in_seconds(1)
                    self.press(loc["continue_button"])
                    self.wait_until_appear(
                        loc["campaign_title"].format(
                            data['media_info']['campaign']))
                    self.wait_in_seconds(1)
                    if cat == 'healthcare':
                        self.wait_until_appear(locator, timeout=120)
                        self.press(locator)
                    elif cat == 'cl':
                        # functionality changed no need click again
                        # self.wait_until_appear(loc["edit_category"].format(
                        #     selector,
                        #     'company/default/entryid'), timeout=120)
                        # self.press(loc["edit_category"].format(
                        #     selector, 'company/default/entryid'))
                        pass
                    else:
                        # self.wait_until_appear(loc["edit_mult_category"].format(
                        #     award, category, 'lion/manage'), timeout=120)
                        # self.press(loc["edit_mult_category"].format(
                        #     award, category, 'lion/manage'))
                        pass
            except NoSuchElementException:
                pass

        do_fill_company_details()
        do_fill_contact_details()
        # media
        if type(entries) is dict:
            total_awards = 0
            total_categories = 0
            for award in sorted(entries.keys()):
                total_awards += 1
                if award == 'Healthcare' or award == 'Health & Wellness':
                    if brand == 'cl':
                        health_sub_cats = cl_health_sub_cats
                        health_cats_mapping = cl_health_cats_mapping
                    for sub_cat in health_sub_cats:
                        for category in entries[award]:
                            total_categories += 1
                            full_cat = \
                                health_cats_mapping[sub_cat] + " - " + category
                            print "Filling Media for " + str(full_cat)
                            self.wait_in_seconds(1)
                            self.wait_until_disappear(loc["loading"])
                            self.press(loc["health_mult_edit"].format(
                                full_cat, 'lion/manage'))
                            if category in extra_alerts or \
                                    award in cl_extra_alerts:
                                handle_extra_alerts(
                                    cat='healthcareg',
                                    locator=loc["health_mult_edit"].format(
                                        full_cat, 'lion/manage'))
                            self.wait_until_appear(loc["categories_generic_"
                                                       "heading"].format(
                                "Media Info"))
                            self.add_media_info(data['media_info'], brand=brand)
                            self.wait_until_appear(
                                loc["health_category_check"].format(
                                    category, 'lion/manage'), timeout=120)
                else:
                    for category in entries[award]:
                        if 'M_' in award:
                            award = award.split('_')[1]
                        total_categories += 1
                        print "Filling Media for " + str(category)
                        self.wait_in_seconds(1)
                        regex = re.compile(r'(\b[a-zA-Z]\d{2}\b)')
                        regex_pattern = regex.findall(category)[0]
                        if category in extra_chars_categories:
                            print "Extra Spaces"
                            extra_length = len(category) + 1
                            category = category.ljust(extra_length)
                        self.wait_until_appear(loc["edit_mult_category"].format(
                            award, category, 'lion/manage'), timeout=10,
                            screenshot=False)

                        self.press(loc["edit_mult_category"].format(
                            award, category, 'lion/manage'))
                        if award in extra_alerts \
                                or award in cl_extra_alerts \
                                or award in sa_extra_alerts:
                            print award + " in extra alerts"
                            handle_extra_alerts()

                        self.wait_until_appear(
                            loc["categories_generic_heading"].format(
                                "Media Info"), screenshot=False)
                        self.add_media_info(data['media_info'], brand=brand)
                        if category in extra_space_categories:
                            self.wait_until_appear(loc[
                                "multiple_category_check_for_spaces"].format(
                                category, 'lion/manage'), timeout=10,
                                screenshot=False)
                        else:
                            self.wait_until_appear(
                                loc["multiple_category_check"].format(
                                    category, 'lion/manage'), timeout=10,
                                screenshot=False)
                    # self.wait_in_seconds(0.5)
                    # assert self.is_element_visible(
                    #     loc["multiple_category_check"].format(category,
                    #                                           'lion/manage'))
            print "Total Awards: " + str(total_awards)
            print "Total Categories: " + str(total_categories)
        else:
            self.press(loc["edit_category"].format(selector, 'lion/manage'))
            self.wait_until_appear(loc["categories_generic_heading"].format(
                "Media Info"))
            self.add_media_info(data['media_info'], brand=brand)
            self.wait_until_appear(
                loc["category_check"].format('lion/manage'), timeout=120)
            assert self.is_element_visible(
                loc["category_check"].format('lion/manage'))

        # written explanation
        if type(entries) is dict:
            for award in sorted(entries.keys()):
                if award == 'Healthcare' or award == 'Health & Wellness':
                    if brand == 'cl':
                        health_sub_cats = cl_health_sub_cats
                        health_cats_mapping = cl_health_cats_mapping
                    for sub_cat in health_sub_cats:
                        for category in entries[award]:
                            full_cat = \
                                health_cats_mapping[sub_cat] + " - " + category
                            print "Filling Written Exp for " + str(full_cat)
                            self.wait_in_seconds(1)
                            self.wait_until_disappear(loc["loading"])
                            self.wait_until_appear(
                                loc["health_mult_edit"].format(
                                    full_cat, 'synopsis/default'), timeout=10)
                            self.press(loc["health_mult_edit"].format(
                                full_cat, 'synopsis/default'))
                            # if category in extra_alerts:
                            #     handle_extra_alerts()
                            self.wait_until_appear(
                                loc["categories_generic_heading"].format(
                                    "The Written Explanation"))
                            self.add_written_explanation(data['explanation'])
                            self.wait_until_appear(
                                loc["health_category_check"].format(
                                    category, 'synopsis/default'))
                else:
                    for category in entries[award]:
                        if 'M_' in award:
                            award = award.split('_')[1]
                        print "Filling Written Explanation for " + str(category)
                        self.wait_in_seconds(1)
                        if category in extra_chars_categories:
                            print "Extra Spaces"
                            extra_length = len(category) + 1
                            category = category.ljust(extra_length)
                        self.wait_until_appear(loc["edit_mult_category"].format(
                                award, category, 'synopsis/default'),
                            timeout=10)
                        self.press(loc["edit_mult_category"].format(
                                award, category, 'synopsis/default'))

                        if award in extra_alerts:
                            handle_extra_alerts()
                        self.wait_until_appear(
                            loc["categories_generic_heading"].format(
                                "The Written Explanation"))
                        self.add_written_explanation(data['explanation'])
                        self.wait_in_seconds(1)
                        if category in extra_space_categories:
                            self.wait_until_appear(loc[
                                "multiple_category_check_for_spaces"].format(
                                 category, 'synopsis/default'))
                        else:
                            self.wait_until_appear(
                                loc["multiple_category_check"].format(
                                 category, 'synopsis/default'))
                            assert self.is_element_visible(
                                loc["multiple_category_check"].format(
                                    category, 'synopsis/default'))

        else:
            self.wait_until_appear(loc["edit_category"].format(
                selector, 'synopsis/default'))
            self.press(loc["edit_category"].format(selector,'synopsis/default'))
            self.wait_until_appear(loc["categories_generic_heading"].format(
                "The Written Explanation"))
            self.add_written_explanation(data['explanation'])
            self.wait_until_appear(
                loc["category_check"].format('synopsis/default'))
            assert self.is_element_visible(
                loc["category_check"].format('synopsis/default'))

        # creative team
        if type(entries) is dict:
            for award in sorted(entries.keys()):
                if award == 'Healthcare' or award == 'Health & Wellness':
                    if brand == 'cl':
                        health_sub_cats = cl_health_sub_cats
                        health_cats_mapping = cl_health_cats_mapping
                    for sub_cat in health_sub_cats:
                        for category in entries[award]:
                            full_cat = \
                                health_cats_mapping[sub_cat] + " - " + category
                            print "Filling Creative Team for " + str(full_cat)
                            self.wait_until_disappear(loc["loading"])
                            self.wait_until_appear(loc["health_mult_edit"].
                                format(full_cat, 'creativeteam/default'),
                                                   timeout=10)
                            self.press(loc["health_mult_edit"].format(
                                full_cat, 'creativeteam/default'))
                            self.wait_until_appear(
                                loc["categories_generic_heading"].format(
                                    "Creative Team"))
                            self.add_creative_team(data['creative_team'],
                                                   brand=brand)
                            self.wait_until_appear(
                                loc["health_category_check"].format(
                                    category, 'creativeteam/default'))
                            assert self.is_element_visible(
                                loc["health_category_check"].format(
                                    category, 'creativeteam/default'))
                else:
                    for category in entries[award]:
                        if 'M_' in award:
                            award = award.split('_')[1]
                        print "Filling Creative Team for " + str(category)
                        self.wait_in_seconds(1)
                        if category in extra_chars_categories:
                            print "Extra Spaces"
                            extra_length = len(category) + 1
                            category = category.ljust(extra_length)
                        self.wait_until_appear(loc["edit_mult_category"].format(
                            award, category, 'creativeteam/default'),
                            timeout=10)
                        self.press(loc["edit_mult_category"].format(
                            award, category, 'creativeteam/default'))
                        if award in extra_alerts:
                            handle_extra_alerts()
                        self.wait_until_appear(
                            loc["categories_generic_heading"].format(
                                "Creative Team"))
                        self.add_creative_team(data['creative_team'],
                                               brand=brand)
                        self.wait_in_seconds(1)
                        if category in extra_space_categories:
                            self.wait_until_appear(loc[
                                "multiple_category_check_for_spaces"].format(
                                  category, 'creativeteam/default'))
                            assert self.is_element_visible(
                                loc["multiple_category_check_for_spaces"
                                ].format(category, 'creativeteam/default'))
                        else:
                            self.wait_until_appear(
                                loc["multiple_category_check"].format(
                                 category, 'creativeteam/default'))
                            assert self.is_element_visible(
                                loc["multiple_category_check"].format(
                                    category, 'creativeteam/default'))

        else:
            self.wait_until_appear(loc["edit_category"].format(
                selector, 'creativeteam/default'))
            self.press(loc["edit_category"].format(
                selector, 'creativeteam/default'))
            self.wait_until_appear(loc["categories_generic_heading"].format(
                "Creative Team"))
            self.add_creative_team(data['creative_team'], brand=brand)
            self.wait_until_appear(
                loc["category_check"].format('creativeteam/default'))
            assert self.is_element_visible(
                loc["category_check"].format('creativeteam/default'))
        # uploads
        if type(entries) is dict:
            for award in sorted(entries.keys()):
                count = 0
                if award == 'Healthcare' or award == 'Health & Wellness':
                    if brand == 'cl':
                        health_sub_cats = cl_health_sub_cats
                        health_cats_mapping = cl_health_cats_mapping
                    for sub_cat in health_sub_cats:
                        for category in entries[award]:
                            full_cat = \
                                health_cats_mapping[sub_cat] + " - " + category
                            print "Filling Uploads for " + str(full_cat)
                            self.wait_in_seconds(1)
                            if category in extra_chars_categories:
                                print "Extra Spaces"
                                extra_length = len(category) + 1
                                category = category.ljust(extra_length)
                            self.wait_until_disappear(loc["loading"])
                            self.wait_until_appear(
                                loc["health_mult_edit"].format(
                                    full_cat, 'media/default'), timeout=120)
                            self.press(loc["health_mult_edit"].format(
                                full_cat, 'media/default'))
                            self.wait_until_appear(loc["media_upload_heading"])
                            if category == 'Entertainment: Film, TV and ' \
                                           'Online Film Content':
                                count = 0
                            if category == 'Film: Cinema, TV and Digital ' \
                                           'Film Content':
                                count = 0
                            elif category == 'Radio & Audio':
                                count = 0
                            self.add_media_upload(
                                data['media_upload'], count, category, award,
                                brand=brand)
                            self.wait_in_seconds(1)
                            count += 1
                            self.wait_until_appear(
                                loc["health_category_check"].format(
                                    category, 'media/default'))
                            assert self.is_element_visible(
                                loc["health_category_check"].format(
                                    category, 'media/default'))

                            assert self.is_element_visible(
                                loc["health_add_to_cart"].format(full_cat))
                            self.press(loc["health_add_to_cart"].format(
                                full_cat))

                else:
                    for category in entries[award]:
                        if 'M_' in award:
                            award = award.split('_')[1]
                        self.wait_in_seconds(1)
                        print "Filling Uploads for " + str(category)
                        self.wait_in_seconds(1)

                        # if category == 'A02 Video / Moving Image':
                        #     print "Extra Spaces"
                        #     extra_length = len(category) + 1
                        #     category = category.ljust(extra_length)
                        self.wait_until_appear(loc["edit_mult_category"].format(
                                award, category, 'media/default'), timeout=120)
                        self.press(loc["edit_mult_category"].format(
                                award, category, 'media/default'))
                        reset_count_categories = [
                            'B03 Use of Broadcast', 'E01 Launch / Re-launch',
                            'A03 Brand or Product Integration into Music '
                            'Content', 'A09 Audio Content', 'A03 Audio Content',
                            'A10 Excellence in Brand Integration & '
                            'Sponsorships / Partnerships for Branded Content',
                            'A04 Digital', 'D02 Social Film ',
                            'D03 Social Film Series', 'D01 Social Video',
                            'A09 Excellence in Music Video',
                            'A05 Audio Content'

                        ]
                        if award in extra_alerts:
                            handle_extra_alerts()
                        self.wait_until_appear(loc["media_upload_heading"])
                        if category == \
                                'A05 Co-Creation & User Generated Content':
                            count = 0
                            self.add_media_upload(
                                data['media_upload'], count, category, award,
                                brand=brand)
                        elif category in reset_count_categories:
                            count = 0
                            self.add_media_upload(
                                data['media_upload'], count, category, award,
                                brand=brand)
                        elif award == 'Film' and category == \
                                'A14 Public Sector':
                            self.add_media_upload(data['media_upload'], count,
                                                  category, award, brand=brand)
                        else:
                            self.add_media_upload(
                                data['media_upload'], count, category, award,
                                brand=brand)
                        self.wait_in_seconds(1)
                        count += 1
                        # if category == 'A02 Video / Moving Image':
                        #     print "Extra Spaces"
                        #     extra_length = len(category) + 1
                        #     category = category.ljust(extra_length)
                        if category in extra_space_categories or \
                                category in extra_chars_categories:
                            self.wait_until_appear(loc[
                                "multiple_category_check_for_spaces"].format(
                                 category, 'media/default'))
                            assert self.is_element_visible(
                                loc["multiple_category_check_for_spaces"].format(
                                    category, 'media/default'))
                            assert self.is_element_visible(
                                loc["cat_add_to_cart_spaces"].format(category))
                            self.press(loc["cat_add_to_cart_spaces"].format(
                                category))

                        else:
                            self.wait_until_appear(
                                loc["multiple_category_check"].format(
                                 category, 'media/default'))
                            assert self.is_element_visible(
                                loc["multiple_category_check"].format(
                                 category, 'media/default'))

                            assert self.is_element_visible(
                                loc["cat_add_to_cart"].format(category))
                            self.press(loc["cat_add_to_cart"].format(category))

        else:
            self.wait_until_appear(loc["edit_category"].format(
                selector, 'media/default'), timeout=120)
            self.press(loc["edit_category"].format(selector, 'media/default'))
            self.wait_until_appear(loc["media_upload_heading"])
            self.add_media_upload(data['media_upload'], brand=brand)
            self.wait_until_appear(loc["category_check"].format(
                'media/default'))
            assert self.is_element_visible(
                loc["category_check"].format('media/default'))

            assert self.is_element_visible(loc["add_to_cart"])
            self.press(loc["add_to_cart"])
            self.wait_in_seconds(2)
        return True

    def add_company(self, data):
        self.wait_until_appear(loc["add_company_button"])
        self.press(loc["add_company_button"])
        self.wait_until_appear(loc["save_button"])
        self.type(loc["company_name"], data['company_name'])
        if data['company_activity'] == 'N/A':
            self.select(loc["company_activity"], data['company_activity'])
        else:
            try:
                self.select(loc["company_activity"], data['company_activity'])
                self.select(loc["company_activity"], data['company_activity'])
            except NoSuchElementException:
                pass
            self.wait_until_appear(loc["company_type"])
            self.select(loc["company_type"], data['company_type'])
        self.type(loc["address1"], data['address1'])
        self.type(loc["address2"], data['address2'])
        self.type(loc["city"], data['city'])
        self.type(loc["postcode"], data['postcode'])
        self.select(loc["country"], data['country'])
        self.press(loc["entrant_company"])
        self.press(loc["idea_creation"])
        extras = ['Media Placement', 'PR',
                  'Production']
        for extra in extras:
            self.press(loc["pr_prod_media"].format(extra))
        self.press(loc["save_button"])
        self.wait_until_appear(loc["company_added"].format(
            data['company_name']))
        self.press(loc["save_entry"])
        self.wait_until_appear(loc["campaign_title"].format(data['campaign']))
        assert self.is_element_visible(loc["campaign_title"].format(
            data['campaign']))
        return True

    def add_contacts(self, data):
        self.wait_in_seconds(2)
        self.type(loc["contact_name"], data['name'])
        self.type(loc["contact_title"], data['title'])
        self.clear_and_type(loc["contact_mobile"], data['mobile'])
        self.type(loc["contact_email"], data['email'])
        self.type(loc["client_name"], data['client_name'])
        self.type(loc["client_title"], data['client_title'])
        self.clear_and_type(loc["client_mobile"], data['client_mobile'])
        self.type(loc["client_email"], data['client_email'])
        self.type(loc["client_city"], data['client_city'])
        self.select(loc["client_country"], data['client_country'])
        self.press(loc["save_contact_entry"])
        self.wait_until_appear(loc["campaign_title"].format(
            data['campaign']), timeout=60)
        self.wait_in_seconds(2)
        return True

    def add_media_info(self, data, brand=None):
        self.press(loc["media_placement"])
        self.wait_until_appear(loc["media_element"], timeout=20)
        self.type(loc["media_element"], data['element'], 0.5)
        self.type(loc["media_location"], data['location'], 0.5)
        self.type(loc["media_airings"], data['airings'], 0.5)
        self.type(loc["media_startdate"], data['start_date'], 0.5)
        self.type(loc["media_end_date"], data['end_date'], 0.5)
        if brand == 'cl' or brand == 'sa':
            if type(data['cl_country']) is str:
                self.press(loc["cl_media_country"].format(data['cl_country']))
            elif type(data['cl_country']) is list:
                for country in data['cl_country']:
                    self.press(loc["cl_media_country"].format(country))
        else:
            self.type(loc["media_country"], data['country'], 0.5)
        try:
            if self.is_element_visible(loc["add_url"], screenshot=False):
                self.press(loc["add_url"])
                self.wait_until_appear(loc["input_url"])
                self.type(loc["input_url"], data['url'], 0.5)
                self.select(loc["select_url_status"], data['url_type'])
        except NoSuchElementException:
            pass
        self.press(loc["save_media_entry"])
        self.wait_until_appear(loc["campaign_title"].format(
            data['campaign']), timeout=60)
        return True

    def add_written_explanation(self, data):
        locs = [loc["written_why_relevant"], loc["written_the_results"],
                loc["written_exp_why_work"], loc["written_outcome"],
                loc["written_context_information"]
                ]
        for loce in locs:
            # TODO implement this to remove all trys
            pass
        try:
            if self.is_element_visible(loc["written_why_relevant"],
                                       screenshot=False):
                self.type(loc["written_why_relevant"], data['why_relevant'])
        except NoSuchElementException:
            pass
        try:
            if self.is_element_visible(loc["written_the_results"],
                                       screenshot=False):
                self.type(loc["written_the_results"], data['the_results'])
        except NoSuchElementException:
            pass
        try:
            if self.is_element_visible(loc["written_exp_why_work"],
                                       screenshot=False):
                self.type(loc["written_exp_why_work"], data['why_work'])
        except NoSuchElementException:
            pass
        try:
            if self.is_element_visible(loc["written_outcome"],
                                       screenshot=False):
                self.type(loc["written_outcome"], data['outcome'])
        except NoSuchElementException:
            pass
        try:
            if self.is_element_visible(loc["written_unwanted_star"],
                                       screenshot=False):
                self.type(loc["written_unwanted_star"], data['outcome'])
        except NoSuchElementException:
            pass
        try:
            if self.is_element_visible(loc["written_context_information"],
                                       screenshot=False):
                self.type(loc["written_context_information"],
                          data['context_info'])
        except NoSuchElementException:
            pass
        try:
            if self.is_element_visible(loc["written_exp_what_happens_in_film"],
                                       screenshot=False):
                self.type(loc["written_exp_what_happens_in_film"],
                          data['what_happens_in_film'])
        except NoSuchElementException:
            pass
        try:
            if self.is_element_visible(loc["written_exp_background"],
                                       screenshot=False):
                self.type(loc["written_exp_background"], data['background'])
        except NoSuchElementException:
            pass
        try:
            if self.is_element_visible(loc["written_exp_creative_idea"],
                                       screenshot=False):
                self.type(
                    loc["written_exp_creative_idea"], data['creative_idea'])
        except NoSuchElementException:
            pass
        try:
            if self.is_element_visible(loc["written_exp_strategy"],
                                       screenshot=False):
                self.type(loc["written_exp_strategy"], data['strategy'])
        except NoSuchElementException:
            pass
        try:
            if self.is_element_visible(loc["written_exp_execution"],
                                       screenshot=False):
                self.type(loc["written_exp_execution"], data['execution'])
        except NoSuchElementException:
            pass
        try:
            if self.is_element_visible(loc["written_exp_results"],
                                       screenshot=False):
                self.type(loc["written_exp_results"], data['results'])
        except NoSuchElementException:
            pass
        try:
            if self.is_element_visible(loc["written_exp_conf_info"],
                                       screenshot=False):
                self.type(loc["written_exp_conf_info"], data['conf_info'])
        except NoSuchElementException:
            pass
        try:
            if self.is_element_visible(loc["written_exp_full_film_script"],
                                       screenshot=False):
                self.type(loc["written_exp_full_film_script"], data['script'])
        except NoSuchElementException:
            pass
        try:
            if self.is_element_visible(loc["written_exp_casting_process"],
                                       screenshot=False):
                self.type(loc["written_exp_casting_process"], data['casting'])
        except NoSuchElementException:
            pass
        try:
            if self.is_element_visible(loc["written_exp_production_design"],
                                       screenshot=False):
                self.type(loc["written_exp_production_design"], data['design'])
        except NoSuchElementException:
            pass
        try:
            if self.is_element_visible(loc["written_exp_cinematography"],
                                       screenshot=False):
                self.type(loc["written_exp_cinematography"],
                          data['cinematography'])
        except NoSuchElementException:
            pass
        try:
            if self.is_element_visible(loc["written_exp_about_the_edit"],
                                       screenshot=False):
                self.type(loc["written_exp_about_the_edit"],
                          data['about_the_edit'])
        except NoSuchElementException:
            pass
        try:
            if self.is_element_visible(loc["written_exp_music_track"],
                                       screenshot=False):
                self.type(loc["written_exp_music_track"], data['music_track'])
        except NoSuchElementException:
            pass
        try:
            if self.is_element_visible(loc["written_exp_sound_design"],
                                       screenshot=False):
                self.type(loc["written_exp_sound_design"],
                          data['sound_design'])
        except NoSuchElementException:
            pass
        try:
            if self.is_element_visible(loc["written_exp_animation"],
                                       screenshot=False):
                self.type(loc["written_exp_animation"], data['animation'])
        except NoSuchElementException:
            pass
        try:
            if self.is_element_visible(loc["written_exp_visual_effects"],
                                       screenshot=False):
                self.type(loc["written_exp_visual_effects"],
                          data['visual_effects'])
        except NoSuchElementException:
            pass
        try:
            if self.is_element_visible(loc["written_exp_achievements"],
                                       screenshot=False):
                self.type(loc["written_exp_achievements"],
                          data['achievements'])
        except NoSuchElementException:
            pass
        try:
            if self.is_element_visible(loc["written_full_radio_advert"],
                                       screenshot=False):
                self.type(loc["written_full_radio_advert"],
                          data['full_radio_advert'])
        except NoSuchElementException:
            pass
        try:
            if self.is_element_visible(
                    loc["written_what_happens_in_radio_advert"],
                    screenshot=False):
                self.type(loc["written_what_happens_in_radio_advert"],
                          data['radio_advert'])
        except NoSuchElementException:
            pass
        try:
            if self.is_element_visible(
                    loc["written_health_restrictions"], screenshot=False):
                self.type(loc["written_health_restrictions"], data['results'])
        except NoSuchElementException:
            pass
        try:
            if self.is_element_visible(loc["written_health_target"],
                                       screenshot=False):
                self.type(loc["written_health_target"], data['results'])
        except NoSuchElementException:
            pass
        try:
            if self.is_element_visible(loc["written_cl_campaign_output"],
                                       screenshot=False):
                self.type(loc["written_cl_campaign_output"], data['outcome'])
        except NoSuchElementException:
            pass
        try:
            if self.is_element_visible(loc["written_interactive"],
                                       screenshot=False):
                self.type(loc["written_interactive"], data['outcome'])
        except NoSuchElementException:
            pass
        try:
            if self.is_element_visible(loc["written_publishing"],
                                       screenshot=False):
                self.type(loc["written_publishing"], data['outcome'])
        except NoSuchElementException:
            pass
        try:
            if self.is_element_visible(loc["written_translation"],
                                       screenshot=False):
                self.type(loc["written_translation"], data['outcome'])
        except NoSuchElementException:
            pass
        try:
            if self.is_element_visible(loc["written_medium_radio_audio"],
                                       screenshot=False):
                self.type(loc["written_medium_radio_audio"], data['outcome'])
        except NoSuchElementException:
            pass
        try:
            if self.is_element_visible(loc["written_exp_information"],
                                       screenshot=False):
                self.type(loc["written_exp_information"], data['outcome'])
        except NoSuchElementException:
            pass
        try:
            if self.is_element_visible(loc["written_poster_billboard"],
                                       screenshot=False):
                self.type(loc["written_poster_billboard"], data['outcome'])
        except NoSuchElementException:
            pass
        try:
            if self.is_element_visible(loc["written_exp_innovation"],
                                       screenshot=False):
                self.type(loc["written_exp_innovation"], data['outcome'])
        except NoSuchElementException:
            pass
        try:
            if self.is_element_visible(loc["written_pr_effectiveness"],
                                       screenshot=False):
                self.type(loc["written_pr_effectiveness"], data['results'])
        except NoSuchElementException:
            pass
        try:
            if self.is_element_visible(loc["written_the_budget"],
                                       screenshot=False):
                self.type(loc["written_the_budget"], data['outcome'])
        except NoSuchElementException:
            pass
        try:
            if self.is_element_visible(loc["written_advancements"],
                                       screenshot=False):
                self.type(loc["written_advancements"], data['outcome'])
        except NoSuchElementException:
            pass
        try:
            if self.is_element_visible(loc["written_exp_innovative_idea"],
                                       screenshot=False):
                self.type(loc["written_exp_innovative_idea"], data['results'])
        except NoSuchElementException:
            pass
        try:
            if self.is_element_visible(loc["written_exp_development"],
                                       screenshot=False):
                self.type(loc["written_exp_development"], data['results'])
        except NoSuchElementException:
            pass
        try:
            if self.is_element_visible(loc["written_exp_innovation1"],
                                       screenshot=False):
                self.type(loc["written_exp_innovation1"], data['results'])
        except NoSuchElementException:
            pass
        try:
            if self.is_element_visible(loc["written_exp_expectations"],
                                       screenshot=False):
                self.type(loc["written_exp_expectations"], data['results'])
        except NoSuchElementException:
            pass

        try:
            if self.is_element_visible(loc["written_creative_use"],
                                       screenshot=False):
                self.type(loc["written_creative_use"], data['results'])
        except NoSuchElementException:
            pass
        try:
            if self.is_element_visible(loc["written_exp_insight"],
                                       screenshot=False):
                self.type(loc["written_exp_insight"], data['results'])
        except NoSuchElementException:
            pass
        try:
            if self.is_element_visible(loc["written_exp_Cultural"],
                                       screenshot=False):
                self.type(loc["written_exp_Cultural"], data['results'])
        except NoSuchElementException:
            pass
        try:
            if self.is_element_visible(loc["written_exp_Creative"],
                                       screenshot=False):
                self.type(loc["written_exp_Creative"], data['results'])
        except NoSuchElementException:
            pass
        try:
            if self.is_element_visible(loc["written_exp_Outcome"],
                                       screenshot=False):
                self.type(loc["written_exp_Outcome"], data['results'])
        except NoSuchElementException:
            pass
        try:
            if self.is_element_visible(loc["written_exp_interpretation"],
                                       screenshot=False):
                self.type(loc["written_exp_interpretation"], data['results'])
        except NoSuchElementException:
            pass
        try:
            if self.is_element_visible(loc["written_exp_product_service"],
                                       screenshot=False):
                self.type(loc["written_exp_product_service"], data['results'])
        except NoSuchElementException:
            pass
        try:
            if self.is_element_visible(loc["written_exp_challenged"],
                                       screenshot=False):
                self.type(loc["written_exp_challenged"], data['results'])
        except NoSuchElementException:
            pass
        try:
            if self.is_element_visible(loc["written_exp_designed"],
                                       screenshot=False):
                self.type(loc["written_exp_designed"], data['results'])
        except NoSuchElementException:
            pass
        try:
            if self.is_element_visible(loc["written_exp_behaviour"],
                                       screenshot=False):
                self.type(loc["written_exp_behaviour"], data['results'])
        except NoSuchElementException:
            pass

        self.press(loc["written_save"])
        self.wait_until_appear(loc["campaign_title"].format(
            data['campaign']), timeout=60)
        return True

    def add_creative_team(self, data, brand=None):
        if brand == 'cl' or brand == 'sa':
            self.type(loc["cl_team_first_name"], data['first_name'])
            self.type(loc["cl_team_last_name"], data['last_name'])
        else:
            self.type(loc["team_name"], data['name'])
        self.type(loc["team_company"], data['company'])
        self.type(loc["team_position"], data['position'])
        self.type(loc["team_role"], data['team_role'])
        self.type(loc["team_email"], data['email'])
        self.press(loc["save_contact_entry"])
        assert self.is_element_visible(
            loc["team_values_assert"].format(data['name']))
        assert self.is_element_visible(
            loc["team_values_assert"].format(data['company']))
        assert self.is_element_visible(
            loc["team_values_assert"].format(data['position']))
        assert self.is_element_visible(
            loc["team_values_assert"].format(data['team_role']))
        self.press(loc["save_entry"])
        self.wait_until_appear(loc["campaign_title"].format(
            data['campaign']), timeout=60)
        return True

    def add_media_upload(self, data, count=None, category=None, award=None,
                         brand=None):
        video_categories = ['A01 Direction',
                            'A01 Cinema & Theatrical: Fiction & Non-Fiction',
                            'A02 TV & Broadcast: Fiction & Non-Fiction',
                            'A03 Online: Fiction & Non-Fiction',
                            'A04 Series: Fiction & Non-Fiction',
                            'Film: Cinema, TV and Digital Film Content',
                            'Entertainment: Film, TV and Online Film Content',
                            'D01 Social Video', 'A09 Excellence in Music Video'
                            ]
        mp3_categories = ['A02 Durable Consumer Goods',
                          'B01 Use of Music or Sound Design',
                          'B02 Script',
                          'B03 Casting & Performance',
                          'C01 Use of Radio / Audio as a Medium',
                          'C02 Use of Audio Technology',
                          'C03 Use of Branded Content / Programming / Podcasts',
                          'Radio & Audio',
                          'A09 Audio Content',
                          'A03 Audio Content',
                          'A05 Audio Content'
                          ]
        three_min_awards = ['Entertainment', 'Entertainment Lions for Music',
                            'Film', 'Entertainment Lions For Sport',
                            'Film Craft']
        picture_categories = [
            'A03 Brand or Product Integration into Music Content',
            'A10 Excellence in Brand Integration & Sponsorships / '
            'Partnerships for Branded Content', 'A04 Digital']
        if award == 'Healthcare':
            pass
            if category == 'Standard Outdoor':
                self.press(loc["optional_materials"])
        else:
            self.wait_until_appear(loc["media_checkbox"])
            self.press(loc["media_checkbox"])
            self.press(loc["media_checkbox_continue"])
        self.wait_until_appear(loc["categories_generic_heading"].format(
             "Upload Media"))

        if category == 'D02 Social Film' or \
                category == 'D03 Social Film Series' or \
                category == 'A03 Brand or Product Integration into ' \
                            'Music Content' or \
                category == 'D01 Use of Original Composition':
            print "Special Category Video and JPG are needed"
            self.press(loc["special_media_upload_file"].format('mov,mp4'))
            self.wait_until_appear(loc["upload_area2"])
            self.wait_in_seconds(3)
            self.drop_files(loc["upload_area"], data['3_min_video'])
            self.wait_until_appear(loc["remove_file"], timeout=120)
            self.press(loc["upload_file_button"])
            self.wait_until_appear(loc["upload_complete"])
            assert self.is_element_visible(loc["upload_complete"])
            assert self.is_element_visible(loc["media_upload_file"])
            print "Mp4 upload complete"
            self.wait_in_seconds(7)
            self.press(loc["special_media_upload_file"].format('jpg'))
            self.wait_until_appear(loc["upload_area2"])
            self.wait_in_seconds(2)
            self.drop_files(loc["upload_area"], data['file'])
            self.wait_until_appear(loc["remove_file"])
            self.press(loc["upload_file_button"])
            self.wait_until_appear(loc["upload_complete"])
            assert self.is_element_visible(loc["upload_complete"])
            assert self.is_element_visible(loc["media_upload_file"])
            self.wait_until_appear(loc["view_media"])
            self.wait_in_seconds(7)

            self.press(loc["save_entry"])
            self.wait_until_appear(loc["campaign_title"].format(
                data['campaign']), timeout=60)
            return True

        if count >= 1:
            print "Re-using Existing Media"
            self.press(loc["add_existing_media"])
            try:
                while self.is_element_visible(loc["file_not_uploaded"], screenshot=False) is True:
                    print "The file is uploaded but not processed completely"
                    self.wait_in_seconds(5)
                    self.reload()
            except NoSuchElementException:
                pass
            if award == 'Creative Strategy':
                file = 'sample_pdf.pdf'
                self.press(loc["add_to_entry_specific_file"].format(file))
            else:
                self.press(loc["add_to_entry"])
            if award == 'Healthcare':
                if category == 'Standard Outdoor':
                    self.press(loc["optional_materials"])
            self.wait_until_appear(loc["view_media"])
            self.press(loc["save_entry"])
            self.wait_until_appear(loc["campaign_title"].format(
                data['campaign']), timeout=60)
            return True
        if category == 'A14 Public Sector' and award == 'Film':
            self.type(loc["upload_length"], data['length'])
            self.press(loc["confirm_button"])
        if category == 'A05 Automotive' and award == 'Film':
            self.type(loc["upload_length"], data['length'])
            self.press(loc["confirm_button"])
        self.press(loc["media_upload_file"])
        file = None
        if brand == 'cl' or brand == 'sa':
            self.wait_until_appear(loc["upload_area2"])
        else:
            self.switch_to_iframe(loc["upload_iframe"])
            self.wait_in_seconds(3)
        if category in video_categories and award != 'Film Craft':
            print "mp4 file is required"
            self.drop_files(loc["upload_area"], data['video'])
        elif award == 'Creative Strategy':
            print "pdf file is required"
            self.drop_files(loc["upload_area"], data['pdf'])
        elif award == 'Radio & Audio' or category in mp3_categories:
            print "mp3 file is required"
            self.drop_files(loc["upload_area"], data['mp3'])
        elif (brand == 'cl' or brand == 'sa') and award in three_min_awards:
            if category in mp3_categories:
                print "mp3 is required"
                self.drop_files(loc["upload_area"], data['mp3'])
            elif category in picture_categories:
                print "jpg is needed"
                self.drop_files(loc["upload_area"], data['file'])
            else:
                print "max 3 min video is required"
                file = 'small_video'
                self.drop_files(loc["upload_area"], data['3_min_video'])
        elif brand == 'cl' and award in three_min_awards:
            print "max 3 min video is required"
            self.drop_files(loc["upload_area"], data['3_min_video'])
        elif category == 'A14 Public Sector' and award == 'Film':
            print "mp4 file is required"
            self.drop_files(loc["upload_area"], data['video'])
        else:
            print "jpg file is required"
            self.drop_files(loc["upload_area"], data['file'])
        self.wait_until_appear(loc["remove_file"])
        self.wait_until_appear(loc["upload_file_button"], timeout=15)
        # print 2
        # try:
        #     print 3
        #     if self.is_element_clickable(loc["upload_file_button"]) is False:
        #         print 4
        #         print "Either file type is wrong or Upload button is inactive"
        #         self.reload()
        #         self.wait_until_appear(loc["media_upload_file"])
        #         self.press(loc["media_upload_file"])
        #         self.wait_until_appear(loc["upload_area2"])
        #         self.drop_files(loc["upload_area"], data['3_min_video'])
        #         self.wait_until_appear(loc["remove_file"])
        #         self.wait_until_appear(loc["upload_file_button"], timeout=15)
        #         return
        # except NoSuchElementException:
        #     pass

        self.press(loc["upload_file_button"])
        if brand != 'cl':
            self.switch_to_default_content()
        self.wait_until_appear(loc["upload_complete"])
        assert self.is_element_visible(loc["upload_complete"])
        #assert self.is_element_visible(loc["media_upload_file"])
        try:
            # if not self.is_element_visible(loc["file_processing"]):
            #     print "file is processed"
            # else:
            #     self.wait_until_appear(loc["file_processing"])
            while self.is_element_visible(loc["file_processing"], screenshot=False) or \
                    self.is_element_visible(loc["upload_close"], screenshot=False):
                print "File is processing"
                self.wait_in_seconds(5)
                self.reload()
                if self.is_element_visible(
                        loc["file_processing"]) is False or \
                        self.is_element_visible(loc["upload_close"])is False:
                    break
        except NoSuchElementException:
            pass

        self.wait_until_appear(loc["view_media"])
        self.press(loc["save_entry"])
        self.wait_until_appear(loc["campaign_title"].format(
            data['campaign']), timeout=60)
        return True

awards = {
    'brand_experience_activation': {
        'Brand Experience & Aviation': [
            'Fast Moving Consumer Goods',
            'Durable Consumer Goods, including Cars',
            'Travel, Leisure, Retail, Restaurants & Fast Food Chains',
            'Media & Publications',
            'Financial Products & Services, Commercial Public Services, B2B '
            'Products & Services',
            'Corporate Image & Sponsorship',
            'Corporate Social Responsibility',
            'Public Sector',
            'Charities & Non-profit'
        ],
        'Channels': [
            'Use of Ambient Media: Small Scale',
            'Use of Ambient Media: Large Scale',
            'Use of Broadcast',
            'Use of Print or Outdoor'
        ],
        'Use of Brand Experience & Activation': [
            'Guerrilla Marketing & Stunts',
            'Live Shows / Concerts / Festivals',
            'Exhibitions / Installations',
            'Competitions & Promotional Games',
            'Customer Retail / In-Store Experience',
            '360° Customer Journey',
        ],
        'Touchpoints & Technology': [
            'Touchpoint Technology & Tech-led Brand Experience',
            'Use of Mobile & Devices',
            'Use of Social & Digital Platforms',
            'Digital Installations'
        ],
        'Strategy': [
            ' Launch / Re-launch',
            ' Sponsorship &  Brand Partnership'
        ],
        'Campaign': [
            ' Integrated Campaign led by Brand Experience & Activation',
            ' Low Budget / High Impact Campaign'
        ]
    },
    'creative_effectiveness': {
        'Creative Effectiveness': [
            'Creative Effectiveness',
            'Creative Effectiveness for Good',
            'Long-Term Creative Effectiveness'
        ]
    },
    'design': {
        'Design': [

        ]
    },
    'digital': {
        'Digital': {}
    },
    'direct': {

    },
    'entertainment': {

    },
    'film': {

    },
    'film_craft': {

    },
    'glass': {

    },
    'health_care': {

    },
    'innovation': {

    },
    'integrated': {

    },
    'media': {

    },
    'mobile': {

    },
    'outdoor': {

    },
    'pr': {

    },
    'print_outdoor_craft': {

    },
    'print_publishing': {

    },
    'radio_audio': {

    }

}
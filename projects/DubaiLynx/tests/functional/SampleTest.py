from TestDriver import TestDriver
from projects.DubaiLynx.pages.LandingPage import LandingPage
from projects.DubaiLynx.fixtures.TestData import test_data, delegate_data
from projects.DubaiLynx.pages.AccountPage import DLAccountPage
from utils.TestUtils import Expects
from projects.DubaiLynx.pages.CampaignPage import CampaignPage
from projects.DubaiLynx.fixtures.CampaignData import campaign_data


class SmokeTests(TestDriver):

    # def test_s1_check_register_landing_page(self):
    #     obj = DLAccountPage(self.driver)
    #     res = obj.log_into_dl_account(test_data, 'register')
    #     self.assert_and_log('res_1', res)

    # def test_s2_check_awards_landing_page(self):
    #     obj = DLAccountPage(self.driver)
    #     res = obj.log_into_dl_account(test_data, 'awards', account='old')
    #     self.assert_and_log('res_1', res)

    # def test_s3_check_brochure_landing_page(self):
    #     obj = DLAccountPage(self.driver)
    #     res = obj.log_into_dl_account(test_data, 'brochure')
    #     self.assert_and_log

    # def test_s4_create_campaign(self):
    #     obj = CampaignPage(self.driver)
    #     obj.create_campaign(campaign_data)

    def test_s4_create_dl_account(self):
        obj = DLAccountPage(self.driver)
        assert obj.create_dl_account(delegate_data, 'awards')

    def test_s5_create_campaign(self):
        obj = CampaignPage(self.driver)
        (campaign, res) = obj.create_campaign(campaign_data)
        assert res


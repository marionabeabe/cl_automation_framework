from TestDriver import TestDriver
from projects.DubaiLynx.pages.LandingPage import LandingPage
from projects.DubaiLynx.fixtures.TestData import test_data, delegate_data
from projects.DubaiLynx.pages.AccountPage import DLAccountPage
from projects.CannesLions.pages.AccountPage import AccountPage
from utils.TestUtils import Expects
from projects.DubaiLynx.pages.CampaignPage import CampaignPage
from projects.DubaiLynx.fixtures.CampaignData import campaign_data


class SmokeTests(TestDriver):

    # TODO:fix this test
    # def test_s0_check_brochure_landing_page(self):
    #     obj = DLAccountPage(self.driver)
    #     res = obj.log_into_dl_account(test_data, 'brochure')
    #     self.assert_and_log('res_1', res)

    def test_s1_check_register_landing_page(self):
        obj = DLAccountPage(self.driver)
        res = obj.log_into_dl_account(test_data, 'register')
        self.assert_and_log('res_1', res)

    def test_s2_logout(self):
        obj = AccountPage(self.driver)
        res = obj.logout(test_data, site='dubai')
        self.assert_and_log('res_1', res)

    # This is not possible as the user cannot be logged after until 10 mins
    # DS-2534
    # def test_s3_check_awards_landing_page(self):
    #     obj = DLAccountPage(self.driver)
    #     res = obj.log_into_dl_account(test_data, 'awards', account='old')
    #     self.assert_and_log('res_1', res)

    # def test_s4_logout(self):
    #     obj = AccountPage(self.driver)
    #     res = obj.logout(test_data, site='dubai')
    #     self.assert_and_log('res_1', res)
from TestDriver import TestDriver
from projects.DubaiLynx.fixtures.TestData import test_data, delegate_data
from projects.DubaiLynx.pages.AccountPage import DLAccountPage
from utils.TestUtils import Expects
from projects.DubaiLynx.pages.CampaignPage import CampaignPage
from projects.DubaiLynx.pages.CartPage import CartPage
from projects.DubaiLynx.pages.OrderPage import OrderPage
from projects.DubaiLynx.fixtures.CampaignData import campaign_data,\
    categories_data
from projects.DubaiLynx.fixtures.CartData import data, company_data
from projects.DubaiLynx.fixtures.AwardsCategoriesData import all_awards
import platform

linux_path = '/var/lib/jenkins/workspace/files/File2.jpg'
linux_mp3_path = '/var/lib/jenkins/workspace/files/sample_mp3.mp3'
linux_mp4_path = '/var/lib/jenkins/workspace/files/sample_video.mp4'
linux_big_file_path = '/var/lib/jenkins/workspace/files/BigFile.jpg'


class AllAwardsTests(TestDriver):

    def test_aw3_1_create_dl_account(self):
        obj = DLAccountPage(self.driver)
        delegate_data['email'] = "All_Awards_" + delegate_data['email']
        res = obj.create_dl_account(delegate_data, 'awards')
        self.assert_and_log('login', res)

    @Expects(['login'])
    def test_aw3_2create_campaign(self):
        if platform.system() == 'Linux':
            categories_data['media_upload']['file'] = linux_path
            categories_data['media_upload']['mp3'] = linux_mp3_path
            categories_data['media_upload']['video'] = linux_mp4_path
            categories_data['media_upload']['big_file'] = linux_big_file_path
        obj = CampaignPage(self.driver)
        (campaign, res) = obj.create_campaign(campaign_data)
        assert res
        assert obj.create_entry(campaign, all_awards['award'],
                                all_awards['category'], all_awards['entries'])
        res = obj.fill_campaign_details(categories_data, campaign,
                                        all_awards['entries'])
        self.assert_and_log('all', res)

    @Expects(['all'])
    def test_aw3_3_process_order(self):
        obj = CartPage(self.driver)
        res = obj.add_to_cart()
        assert res['res']
        assert obj.process_order(data)
        obj = OrderPage(self.driver)
        entries = obj.process_entries(all_awards['entries'])
        assert obj.process_payment('bank_transfer', company_data, entries)

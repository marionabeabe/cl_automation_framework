from TestDriver import TestDriver
from projects.DubaiLynx.fixtures.TestData import test_data, delegate_data
from projects.DubaiLynx.pages.AccountPage import DLAccountPage
from utils.TestUtils import Expects
from projects.DubaiLynx.pages.CampaignPage import CampaignPage
from projects.DubaiLynx.pages.CartPage import CartPage
from projects.DubaiLynx.pages.OrderPage import OrderPage
from projects.DubaiLynx.fixtures.CampaignData import campaign_data,\
    categories_data
from projects.DubaiLynx.fixtures.AwardsCategoriesData import  \
    two_awards_five_categories
from projects.DubaiLynx.fixtures.CartData import data, company_data
from projects.DubaiLynx.fixtures.PaymentData import cc_data
import platform

linux_path = '/var/lib/jenkins/workspace/files/File2.jpg'


class AwardsTests(TestDriver):

    def test_aw1_1_create_dl_account(self):
        obj = DLAccountPage(self.driver)
        delegate_data['email'] = "Mult_Awards_" + delegate_data['email']
        res = obj.create_dl_account(delegate_data, 'awards')
        self.assert_and_log('login', res)

    @Expects(['login'])
    def test_aw1_2_create_campaign_multiple_entries(self):
        if platform.system() == 'Linux':
            categories_data['media_upload']['file'] = linux_path
        obj = CampaignPage(self.driver)
        (campaign, res) = obj.create_campaign(campaign_data)
        assert res
        assert obj.create_entry(campaign, two_awards_five_categories['award'],
                                two_awards_five_categories['category'])
        res = obj.fill_campaign_details(categories_data, campaign,
                                        two_awards_five_categories['entries'])
        self.assert_and_log('multiple_fill', res)

    @Expects(['multiple_fill'])
    def test_aw1_3_process_order(self):
        obj = CartPage(self.driver)
        res = obj.add_to_cart()
        assert res['res']
        assert obj.process_order(data)
        obj = OrderPage(self.driver)
        entries = obj.process_entries(two_awards_five_categories['entries'])
        # assert obj.process_payment('bank_transfer', company_data)
        assert obj.process_payment('credit_card', company_data, entries,
                                   cc_data, "Visa")

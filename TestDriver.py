from selenium import webdriver
import os
import unittest
import platform
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.firefox.options import Options
from selenium.webdriver.ie.options import Options as IEOptions
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from Exceptions import NoneValueReturned
import pytest
import getpass

browser = os.getenv('browser', 'chrome')
bd = pytest.config.getoption('BROWSER')
browser = bd
lab = pytest.config.getoption('LAB')
user = os.getenv('username')


class TestDriver(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls.data = {}
        print "Browser is: " + browser
        print "OS is: " + platform.system()
        print "Environment is: " + lab
        if browser == 'chrome':
            chrome_options = Options()
            chrome_options.add_argument('--disable-dev-shm-usage')
            chrome_options.add_argument('--headless')
            chrome_options.add_argument('--disable-gpu')

            if platform.system() == 'Linux':
                cls.driver = webdriver.Chrome(
                    executable_path=
                    '/var/lib/jenkins/workspace/drivers/linux/chromedriver',
                    options=chrome_options)
                cls.driver.set_window_size(1366, 768)
            elif platform.system() == 'Darwin':
                username = getpass.getuser()
                cls.driver = webdriver.Chrome(
                    executable_path=
                    '/Users/{}/Desktop/cl_automation_framework/drivers/'
                    'mac/chromedriver'.format(username))
                cls.driver.maximize_window()
            elif platform.system() == 'Windows':
                print "Username: " + user
                # for windows the driver is provided add the path in env vars
                # C:\Users\hemasudheer\automation_framework\drivers\windows
                cls.driver = webdriver.Chrome()
                cls.driver.maximize_window()
                #cls.driver.set_window_size(1366, 768)
        elif browser == 'firefox':
            if platform.system() == 'Linux':
                options = Options()
                options.headless = True
                cls.driver = webdriver.Firefox(
                    options=options,
                    executable_path='/var/lib/jenkins/workspace/drivers/'
                                    'linux/geckodriver')
            elif platform.system() == 'Darwin':
                cls.driver = webdriver.Firefox(
                    executable_path='../drivers/mac/deckodriver')
            elif platform.system() == 'Windows':
                # for windows the driver is provided add the path in env vars
                # C:\Users\hemasudheer\automation_framework\drivers\windows
                cls.driver = webdriver.Firefox()
            cls.driver.maximize_window()
        # other browsers can be done in the same way
        elif browser == 'ie':
            ie_options = IEOptions()
            ie_options.ignore_protected_mode_settings = True
            #ie_options.capabilities.acceptInsecureCerts = False
            ie_options.ignore_zoom_level = True
            ie_options.require_window_focus = True
            cls.driver = webdriver.Ie(options=ie_options)
            cls.driver.maximize_window()
        elif browser == 'edge':
            cls.driver = webdriver.Edge()

    @classmethod
    def tearDownClass(cls):
        cls.driver.quit()

    def assert_and_log(self, key, value):
        if value is None:
            raise NoneValueReturned("Nothing is returned")

        assert value
        self.data[key] = value

    def assert_not_and_log(self, key, value):
        if value is None:
            raise NoneValueReturned("Nothing is returned")

        assert not value
        self.data[key] = value


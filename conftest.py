import pytest


def pytest_addoption(parser):
    parser.addoption(
        "--BROWSER", action="store", default="chrome"
    )
    parser.addoption(
        "--LAB", action="store", default="uat"
    )


@pytest.fixture
def browser(request):
    return request.config.getoption("--BROWSER")


@pytest.fixture
def lab(request):
    return request.config.getoption("--LAB")

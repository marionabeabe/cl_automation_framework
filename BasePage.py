import time
import datetime
from selenium.webdriver.common.keys import Keys
from selenium.common.exceptions import (ElementNotVisibleException,
                                        ElementNotSelectableException,
                                        InvalidSelectorException,
                                        StaleElementReferenceException,
                                        TimeoutException,
                                        NoSuchElementException,
                                        WebDriverException
                                        )
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import Select
from selenium.webdriver import ActionChains
import re
import traceback
import os
from Exceptions import FileNotFoundError, LocatorFoundexception

JS_DROP_FILES = "var c=arguments,b=c[0],k=c[1];c=c[2];for(var d=b.ownerDocument||document,l=0;;){var e=b.getBoundingClientRect(),g=e.left+(k||e.width/2),h=e.top+(c||e.height/2),f=d.elementFromPoint(g,h);if(f&&b.contains(f))break;if(1<++l)throw b=Error('Element not interactable'),b.code=15,b;b.scrollIntoView({behavior:'instant',block:'center',inline:'center'})}var a=d.createElement('INPUT');a.setAttribute('type','file');a.setAttribute('multiple','');a.setAttribute('style','position:fixed;z-index:2147483647;left:0;top:0;');a.onchange=function(b){a.parentElement.removeChild(a);b.stopPropagation();var c={constructor:DataTransfer,effectAllowed:'all',dropEffect:'none',types:['Files'],files:a.files,setData:function(){},getData:function(){},clearData:function(){},setDragImage:function(){}};window.DataTransferItemList&&(c.items=Object.setPrototypeOf(Array.prototype.map.call(a.files,function(a){return{constructor:DataTransferItem,kind:'file',type:a.type,getAsFile:function(){return a},getAsString:function(b){var c=new FileReader;c.onload=function(a){b(a.target.result)};c.readAsText(a)}}}),{constructor:DataTransferItemList,add:function(){},clear:function(){},remove:function(){}}));['dragenter','dragover','drop'].forEach(function(a){var b=d.createEvent('DragEvent');b.initMouseEvent(a,!0,!0,d.defaultView,0,0,0,g,h,!1,!1,!1,!1,0,null);Object.setPrototypeOf(b,null);b.dataTransfer=c;Object.setPrototypeOf(b,DragEvent.prototype);f.dispatchEvent(b)})};d.documentElement.appendChild(a);a.getBoundingClientRect();return a;"


class BasePage(object):

    def __init__(self, driver):
        self.driver = driver

    def reload(self):
        self.driver.refresh()

    def scroll_to_top(self):
        self.driver.find_element_by_tag_name('body').send_keys(
            Keys.CONTROL + Keys.HOME)

    def scroll_to_bottom(self):
        self.driver.find_element_by_tag_name('body').send_keys(
            Keys.CONTROL + Keys.END)

    def clear(self, locator):
        elem_type, elem = locator.split('=', 1)
        self.driver.find_element_by_xpath(elem).send_keys(Keys.CONTROL + "a")
        self.driver.find_element_by_xpath(elem).send_keys(Keys.DELETE)
        #self.driver.find_element_by_xpath(elem).clear()

    def clear_and_type(self, locator, text, enter=False):
        elem_type, elem = locator.split('=', 1)
        if self.check_element_visible(locator) is True:
            self.driver.find_element_by_xpath(elem).clear()
            self.driver.find_element_by_xpath(elem).send_keys(text)
            if enter:
                self.driver.find_element_by_xpath(elem).send_keys(Keys.RETURN)
        else:
            self.screen_shot()
            raise ElementNotVisibleException('Element Not Found')

    def get_web_page(self, address):
        self.driver.get(address)

    def upload_file(self, locator, file_path):
        elem_type, elem = locator.split('=', 1)
        file_xpath = self.driver.find_element_by_xpath(elem)
        file_xpath.send_keys(file_path)

    def current_url(self):
        return self.driver.current_url

    def type_and_press_enter(self, locator, text):
        loc = self.find_by_locator(locator)
        self.wait_in_seconds(0.5)
        loc.type(text)
        loc.type(Keys.RETURN)

    def type(self, locator, text, wait=None):
        if wait is not None:
            self.wait_in_seconds(wait)
        try:
            self.find_by_locator(locator).send_keys(text)
        except StaleElementReferenceException:
            elem = self.find_by_locator(locator)
            elem.send_keys(text)
        except AttributeError:
            print "type"
            self.screen_shot()

    def press(self, locator, wait=None):
        if wait is False:
            pass
        else:
            self.wait_until_appear(locator, 20)
        try:
            self.find_by_locator(locator).click()
        except ElementNotVisibleException:
            print "Element Not Found: " + locator
        except TimeoutException:
            print "Element Not Found: " + locator
        except StaleElementReferenceException:
            elem = self.find_by_locator(locator)
            elem.click()
        except AttributeError:
            print "press"
            self.screen_shot()

    def select(self, locator, data):
        self.wait_until_appear(locator, 20)
        if self.check_element_visible(locator) is True:
            select = Select(self.find_by_locator(locator))
            select.select_by_visible_text(data)
        else:
            print "select"
            self.screen_shot()
            raise ElementNotSelectableException(locator + ' is not selectable')


    def now(self):
        return datetime.datetime.utcnow()

    def hover_then_click(self, element_hover, element_click):
        _, hover_element = element_hover.split('=', 1)
        hover_element = self.driver.find_element_by_xpath(hover_element)
        action_chains = ActionChains(self.driver)
        action_chains.move_to_element(hover_element)
        action_chains.perform()
        self.wait_in_seconds(2)
        self.wait_until_appear(element_click)
        # _, click_element = element_click.split('=', 1)
        # click_element = self.driver.find_element_by_xpath(click_element)
        # action_chains.click(click_element)
        # action_chains.perform()
        self.press(element_click)

    def screen_shot(self):
        path = self.create_png_file_path()
        assert self.driver.get_screenshot_as_file(path)
        print path
        return True

    def create_png_file_path(self):
        ptn = re.compile(r"test(\d{3})")
        traces = traceback.extract_stack()
        for trace in traces:
            res = re.search(ptn, trace[2])
            if res is None:
                continue
            else:
                name = trace[2]
                break
        else:
            today = datetime.datetime.utcnow()
            name = today.strftime('%Y-%b-%d_%H-%M-%S-%f')
        log_dir = self.create_log_dir_path()
        if not os.path.isdir(log_dir):
            os.mkdir(log_dir)
        return log_dir + name + ".png"

    def create_log_dir_path(self):
        current_dir = os.path.abspath(os.path.dirname(__file__))
        return current_dir + "/logs/"

    def wait_until_appear(self, locator, timeout=60, screenshot=False):
        elem_type, elem = locator.split('=', 1)
        try:
            WebDriverWait(self.driver, timeout).until(
                EC.visibility_of_element_located((By.XPATH, elem))
            )
        except TimeoutException:
            if screenshot is False:
                return False
            else:
                print "wait until appear"
                self.screen_shot()

    def wait_until_disappear(self, locator, timeout=60):
        elem_type, elem = locator.split('=', 1)
        try:
            WebDriverWait(self.driver, timeout).until(
                EC.invisibility_of_element((By.XPATH, elem))
            )
        except TimeoutException:
            self.screen_shot()

    def wait_in_seconds(self, seconds):
        time.sleep(seconds)

    def implicitly_wait(self, seconds):
        self.driver.implicitly_wait(seconds)

    def check_element_visible(self, locator):
        elem_type, elem = locator.split('=', 1)
        if elem_type == 'xpath' and \
                self.driver.find_element_by_xpath(
                    elem).is_displayed():
            return True
        elif elem_type == 'css' and \
                self.driver.find_element_by_css_locator(
                    elem).is_displayed():
            return True
        elif elem_type == 'value' and \
                self.driver.find_element_by_partial_text(
                    elem).is_displayed():
            return True
        else:
            return False

    def find_by_locator(self, locator, timeout=20, silent=False):
        try:
            self.wait_until_appear(locator, timeout)
            if "=" not in locator:
                print "Please check the locator you are using"
                raise InvalidSelectorException("Check locator")

            elem_type, element = locator.split('=', 1)
            if elem_type == 'css':
                return self.driver.find_element_by_css(element)
            elif elem_type == 'id':
                return self.driver.find_element_by_id(element)
            elif elem_type == 'name':
                return self.driver.find_element_by_name(element)
            elif elem_type == 'text':
                return self.driver.find_element_by_text(element)
            elif elem_type == 'tag':
                return self.driver.find_element_by_tag(element)
            elif elem_type == 'xpath':
                return self.driver.find_element_by_xpath(element)
        except StaleElementReferenceException:
            _, element = locator.split('=', 1)
            return self.driver.find_element_by_xpath(element)
        except WebDriverException:
            print "find by locator"
            self.screen_shot()

    def is_element_visible(self, locator, screenshot=None):
        try:
            if self.check_element_visible(locator):
                return True
            else:
                print "Element is not visible Check Data or env"
                return False
        except StaleElementReferenceException:
            _, element = locator.split('=', 1)
            elem = self.driver.find_element_by_xpath(element)
            return elem.is_displayed()

        except AssertionError:
            self.screen_shot()

        except NoSuchElementException:
            if screenshot is False:
                return False
            else:
                self.screen_shot()
                return False

    def is_element_clickable(self, locator, timeout=10):
        elem_type, elem = locator.split('=', 1)
        try:
            WebDriverWait(self.driver, timeout).until(
                EC.element_to_be_clickable((By.XPATH, elem))
            )
            return True
        except NoSuchElementException:
            self.screen_shot()
            return False

    def switch_to_iframe(self, locator):
        frame = self.driver.find_element_by_xpath(locator)
        self.driver.switch_to.frame(frame)

    def switch_to_default_content(self):
        self.driver.switch_to.default_content()

    def drop_files(self, element, files, offsetX=0, offsetY=0):
        element = self.driver.find_element_by_xpath(element)
        self.driver = element.parent
        isLocal = not self.driver._is_remote or '127.0.0.1' in \
                  self.driver.command_executor._url
        paths = []

        for file in (files if isinstance(files, list) else [files]):
            if not os.path.isfile(file):
                raise FileNotFoundError(file)
            paths.append(file if isLocal else element._upload(file))

        value = '\n'.join(paths)
        elm_input = self.driver.execute_script(JS_DROP_FILES, element, offsetX,
                                          offsetY)
        elm_input._execute('sendKeysToElement',
                           {'value': [value], 'text': value})

    def debug(self):
        import pdb;pdb.set_trace()

    def convert(self, name):
        s1 = re.sub('(.)([A-Z][a-z]+)', r'\1_\2', name)
        return re.sub('([a-z0-9])([A-Z])', r'\1_\2', s1).lower()






